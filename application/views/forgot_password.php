<html lang="en">
<head>
    <title>Password Change | Booth</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #17a2b8;
            height: 100vh;
        }

        #login .container #login-row #login-column #login-box {
            margin-top: 120px;
            max-width: 600px;
            height: 320px;
            border: 1px solid #9C9C9C;
            background-color: #EAEAEA;
        }

        #login .container #login-row #login-column #login-box #login-form {
            padding: 20px;
        }

        #login .container #login-row #login-column #login-box #login-form #register-link {
            margin-top: -85px;
        }
    </style>
</head>
<body>
<div id="login">
    <h3 class="text-center text-white pt-5">Change Password</h3>
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form" action="<?php echo base_url('index/changePassword'); ?>"
                          method="post" onsubmit="return false;">
                        <h3 class="text-center text-info">Change Password</h3>
                        <div class="form-group">
                            <label for="new_password" class="text-info">New Password:</label><br>
                            <input type="password" name="new_password" id="new_password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="confirm_password" class="text-info">Confirm New Password:</label><br>
                            <input type="password" name="confirm_password" id="confirm_password" class="form-control"
                                   required>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="UserID" value="<?php echo base64_encode($UserID); ?>">
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#login-form").submit(function (e) {
        e.preventDefault();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                alert(result.message);
                if (result.status == true) {
                    $form[0].reset();
                }
            }
        });
    });
</script>

</body>
</html>
