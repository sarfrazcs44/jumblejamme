<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="row">
                                <div class="col-sm-4">
                                    Question Details
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Name</label>
                                        <h5>
                                            <a href="<?php echo base_url('cms/user/view/' . base64_encode($question->UserID)); ?>"
                                               target="_blank"><?php echo $question->FullName; ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Username</label>
                                        <h5><?php echo $question->UserName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">City</label>
                                        <h5><?php echo $question->UserCityName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Category</label>
                                        <h5>
                                            <?php echo $question->CategoryName; ?>
                                            / <?php echo $question->SubCategoryName; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Question Description</label>
                                        <h5><?php echo $question->QuestionDescription; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Question Asked At</label>
                                        <h5><?php echo getFormattedDateTime($question->QuestionAskedAt, 'd-m-Y h:i:s A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            Question Images
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <?php
                            foreach ($question->QuestionImages as $image) { ?>
                                <div class="col-sm-4" style="width: 200px;height: 200px;">
                                    <a data-fancybox="gallery"
                                       href="<?php echo base_url($image->Image); ?>">
                                        <img src="<?php echo base_url($image->CompressedImage); ?>">
                                    </a>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Question Comments (<?php echo $question->CommentCount; ?>)</h5>
                    </div>
                    <div class="card-content">
                        <?php
                        if (!empty($question->QuestionComments)) {
                            foreach ($question->QuestionComments as $questionComment) { ?>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <a data-fancybox="gallery"
                                           data-caption="<?php echo $questionComment->FullName; ?>: <?php echo $questionComment->Comment; ?>"
                                           href="<?php echo base_url($questionComment->CompressedImage); ?>">
                                            <img src="<?php echo base_url($questionComment->CompressedImage); ?>"
                                                 style="width: 40px;height: 40px;">
                                        </a>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="row">
                                            <div class="col-sm-12"><strong><?php echo $questionComment->FullName; ?> (@<?php echo $questionComment->UserName; ?>)</strong></div>
                                            <div class="col-sm-12"><?php echo $questionComment->Comment; ?></div>
                                            <div class="col-sm-12"><?php echo getFormattedDateTime($questionComment->CreatedAt, 'd-m-Y h:i A'); ?></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            <?php }
                        } else { ?>
                            <div class="row">
                                <div class="col-sm-12">No Comments Found</div>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Question Reports (<?php echo $question->ReportCount; ?>)</h5>
                    </div>
                    <div class="card-content">
                        <?php
                        if (!empty($question->QuestionReports)) {
                            foreach ($question->QuestionReports as $questionReport) { ?>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <a data-fancybox="gallery"
                                           data-caption="<?php echo $questionReport->FullName; ?>: <?php echo $questionReport->ReportReason; ?>"
                                           href="<?php echo base_url($questionReport->CompressedImage); ?>">
                                            <img src="<?php echo base_url($questionReport->CompressedImage); ?>"
                                                 style="width: 40px;height: 40px;">
                                        </a>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="row">
                                            <div class="col-sm-12"><strong><?php echo $questionReport->FullName; ?> (@<?php echo $questionReport->UserName; ?>)</strong></div>
                                            <div class="col-sm-12"><?php echo $questionReport->ReportReason; ?></div>
                                            <div class="col-sm-12"><?php echo getFormattedDateTime($questionReport->ReportedAt, 'd-m-Y h:i A'); ?></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            <?php }
                        } else { ?>
                            <div class="row">
                                <div class="col-sm-12">No Reports Found</div>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.fancybox').fancybox({
            beforeShow: function () {
                this.title = $(this.element).data("caption");
            }
        });
    }); // ready
</script>