<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        $common_fields5 = '';
        $common_fields6 = '';
        $category_dropdown = '';
        $sub_category_dropdown = '';
        $nutrition_dropdown = '';
        $boxes_dropdown = '';
        $variant_fields = '';
        $demand_bidding = '';

        if ($key == 0) {
            $category_dropdown = getCategoryTreeForProduct(0, '','EN',$result[$key]->CategoryID);
            


            $demand_bidding = '<div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="Demand">
                                                <input name="IsDemand" value="1" type="radio" id="Demand" ' . ((isset($result[$key]->IsDemand) && $result[$key]->IsDemand == 1) ? 'checked' : '') . '/> Demand &nbsp;&nbsp;&nbsp;
                                                <input name="IsDemand" value="0" type="radio" id="Product" ' . ((isset($result[$key]->IsDemand) && $result[$key]->IsDemand == 0) ? 'checked' : '') . '/> Product
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row product_fields" style="' . ((isset($result[$key]->IsDemand) && $result[$key]->IsDemand == 1) ? 'display: none;' : '') . '">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsFixedPrice">
                                                <input name="IsFixedPrice" value="1" type="radio" id="FixedPrice" ' . ((isset($result[$key]->IsFixedPrice) && $result[$key]->IsFixedPrice == 1) ? 'checked' : '') . '/> Fixed Price &nbsp;&nbsp;&nbsp;
                                                <input name="IsFixedPrice" value="0" type="radio" id="Bidding" ' . ((isset($result[$key]->IsFixedPrice) && $result[$key]->IsFixedPrice == 0) ? 'checked' : '') . ' /> Bidding
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>';


            $common_fields4 = '<div class="row">';
            $product_images = get_images($result[$key]->ProductID, 'ProductImage');
            // print_rm($product_images);
            if ($product_images) {
                foreach ($product_images as $product_image){
                    $common_fields4 .= '<div class="col-md-3 col-sm-3 col-xs-3" id="img-' . $product_image->SiteImageID . '"><i class="fa fa-trash delete_image" data-image-id="' . $product_image->SiteImageID . '" data-image-path="' . $product_image->ImageName . '" aria-hidden="true"></i><img src="' . base_url() . $product_image->ImageName . '" style="height:200px;width:200px;"></div>';
                }
            }
            $common_fields4 .= '<div class="col-md-12">
                                    <div class="form-group m-b-20">
                                        <label>'.lang('Image').'s Upload upto 10 images</label><br>
                                        <input type="file" name="Image[]" id="filer_input1" required>
                                    </div>
                                </div></div>';
            $variant_fields .= '<div class="row product_fields FixedPrice" style="' . ((isset($result[$key]->IsFixedPrice) && $result[$key]->IsFixedPrice == 0) || (isset($result[$key]->IsDemand) && $result[$key]->IsDemand == 1) ? 'display: none;' : '') . '"> <div class="col-md-12 col-sm-12">
                                <div class="multi-field-wrapper">
                                   <div class="multi-fields">';

                    if($variants){
                        foreach ($variants as  $variant) {
                           $variant_fields .= '<div class="row multi-field">
                                                     <div class="col-md-3 col-sm-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label"> Add Variation Min Value</label>
                                                            <input type="text" name="MinRang[]" class="form-control" value="'.$variant->MinRang.'">

                                                        </div>
                                                     </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label"> Add Variation Max Value</label>
                                                            <input type="text" name="MaxRang[]" class="form-control" value="'.$variant->MaxRang.'">

                                                        </div>
                                                    </div>
                                                     <div class="col-md-3 col-sm-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label"> Add Price</label>
                                                            <input type="text" name="Price[]" class="form-control" value="'.$variant->Price.'">
                                                        </div>
                                                     </div>
                                                     <div class="col-md-2 col-sm-2 remove-field">
                                                        <button type="button" class="btn btn-danger">X</button>
                                                     </div>
                                                  </div>';
                        }
                    }               


                                      
            $variant_fields .=' <div class="row multi-field">
                                                     <div class="col-md-3 col-sm-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label"> Add Variation Min Value</label>
                                                            <input type="text" name="MinRang[]" class="form-control" value="">

                                                        </div>
                                                     </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label"> Add Variation Max Value</label>
                                                            <input type="text" name="MaxRang[]" class="form-control" value="">

                                                        </div>
                                                    </div>
                                                     <div class="col-md-3 col-sm-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label"> Add Price</label>
                                                            <input type="text" name="Price[]" class="form-control" value="">
                                                        </div>
                                                     </div>
                                                     <div class="col-md-2 col-sm-2 remove-field">
                                                        <button type="button" class="btn btn-danger">X</button>
                                                     </div>
                                                  </div></div>
                                   <button type="button" class="add-field  btn btn-success">Add more</button>
                                </div>
                            </div></div>
                            <div class="row demand" style="' . ((isset($result[$key]->IsDemand) && $result[$key]->IsDemand == 0) ? 'display: none;' : '') . '">
                                
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Location">Location</label>
                                        <textarea class="form-control" name="Location" id="Location">' . ((isset($result[$key]->Location)) ? $result[$key]->Location : '') . '</textarea>
                                    </div>
                                </div>
                                
                            </div>';                           


            $common_fields = '<div class="col-md-3">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="ProductPrice"><span class="Bidding" style="' . ((isset($result[$key]->IsFixedPrice) && $result[$key]->IsFixedPrice == 1) ? 'display: none;' : '') . '">Starting </span>' . lang('price') . '</label>
                                     <input type="text" name="ProductPrice" parsley-trigger="change" required  class="form-control number-with-decimals" id="ProductPrice" value="' . ((isset($result[$key]->ProductPrice)) ? $result[$key]->ProductPrice : '') . '">
                                                               
                                </div>
                          </div>
                          <div class="col-md-3">
                                    <div class="form-group label-floating FixedPrice" style="' . (((isset($result[$key]->IsFixedPrice) && $result[$key]->IsFixedPrice == 0) || (isset($result[$key]->IsDemand) && $result[$key]->IsDemand == 1)) ? 'display: none;' : '') . '">
                                        <label class="control-label" for="DiscountedPrice">Discounted Price</label>
                                        <input type="text" name="DiscountedPrice"   class="form-control" id="DiscountedPrice" value="' . ((isset($result[$key]->DiscountedPrice)) ? $result[$key]->DiscountedPrice : '') . '">
                                    </div>
                                </div>
                            <div class="col-md-3 Bidding" style="' . ((isset($result[$key]->IsFixedPrice) && $result[$key]->IsFixedPrice == 1) ? 'display: none;' : '') . '">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="EndBidDateTime">Bidding End Date Time</label>
                                        <input type="text" name="EndBidDateTime"   class="form-control datetimepicker" id="EndBidDateTime" value="' . ((isset($result[$key]->EndBidDateTime)) ? date('d-m-Y g:i A',strtotime($result[0]->EndBidDateTime)) : '') . '">
                                    </div>
                                </div>';
            $common_fields2 = '<div class="row"><div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4 checkbox-radios product_fields FixedPrice" style="' . ((isset($result[$key]->IsFixedPrice) && $result[$key]->IsFixedPrice == 0) || (isset($result[$key]->IsDemand) && $result[$key]->IsDemand == 1) ? 'display: none;' : '') . '">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="OutOfStock">
                                                <input name="OutOfStock" value="1" type="checkbox" id="OutOfStock" ' . ((isset($result[$key]->OutOfStock) && $result[$key]->OutOfStock == 1) ? 'checked' : '') . '/> Out of stock
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                </div>';

            $common_fields3 = '<div class="row">
                                    
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="CategoryID" class="form-control" id="CategoryID" required>
                                            ' . $category_dropdown . '
                                        </select>
                                    </div>
                                </div>
                                
                                </div>
                                <div class="row" style="' . ((isset($result[$key]->IsDemand) && $result[$key]->IsDemand == 1) ? 'display: none;' : '') . '">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Quantity">Stock (Quantity)</label>
                                        <input type="text" name="Quantity" required  class="form-control" id="Quantity" value="' . ((isset($result[$key]->Quantity)) ? $result[$key]->Quantity : '') . '">
                                    </div>
                                </div>
                                <div class="col-md-3 delivery_charges" style = "' . ((isset($result[$key]->IsDeliveryChargesInclude) && $result[$key]->IsDeliveryChargesInclude == 1) ? 'display:none;' : '') . '">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DeliveryCharges">Shipping Price</label>
                                        <input type="text" name="DeliveryCharges" required  class="form-control" id="DeliveryCharges" value="' . ((isset($result[$key]->DeliveryCharges)) ? $result[$key]->DeliveryCharges : '') . '">
                                    </div>
                                </div>
                                <div class="col-md-3 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsDeliveryChargesInclude">
                                                <input name="IsDeliveryChargesInclude" value="1" type="checkbox" id="IsDeliveryChargesInclude" ' . ((isset($result[$key]->IsDeliveryChargesInclude) && $result[$key]->IsDeliveryChargesInclude == 1) ? 'checked' : '') . ' /> Included Shipping Price
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>';


        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="'.$language->SystemLanguageTitle.'">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                   '.$demand_bidding.'
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                        ' . $common_fields . '
                                                         
                                                    </div>
                                                    ' . $common_fields3 . '
                                                    '.$variant_fields.'


                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description">' . lang('description') . '</label>
                                                                <textarea class="form-control" name="Description" id="Description" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    



                                                    
                                                    ' . $common_fields4 . '
                                                   
                                                    ' . $common_fields2 . '
                                                    
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
    $('#IsDeliveryChargesInclude').on('click',function(){
            if($(this).prop("checked") == true){

                $('.delivery_charges').hide();
            }else{
                $('.delivery_charges').show();
            }
        });


        $('#Demand').on('click',function(){
            $('.demand').show();
            $('.Bidding').hide();
            $('.product_fields').hide();
        });
        $('#Product').on('click',function(){
            
            $('#FixedPrice').click();
            $('.demand').hide();
            $('.product_fields').show();
        });

        $('#FixedPrice').on('click',function(){
            $('.FixedPrice').show();
            $('.Bidding').hide();
        });
        $('#Bidding').on('click',function(){
            $('.FixedPrice').hide();
            $('.Bidding').show();
        });

    $('.multi-field-wrapper').each(function() {
        var $wrapper = $('.multi-fields', this);
        $(".add-field", $(this)).click(function(e) {
            $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
        });
    //  $('.remove-field').click(function() {
        
    //         $(this).parent('.multi-field').remove();
    // });
        $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
        });
        });

    $(".delete_image").on('click', function () {
            var id = $(this).attr('data-image-id');
            var img_path = $(this).attr('data-image-path');

            url = "cms/product/deleteImage";
            //reload = "<?php echo base_url();?>cms/product/edit/<?php echo $result[0]->ProductID; ?>";
            deleteImage(id, url, img_path);

        });
     });
</script>