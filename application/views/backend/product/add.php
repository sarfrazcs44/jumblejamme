<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="Demand">
                                                <input name="IsDemand" value="1" type="radio" id="Demand"/> Demand &nbsp;&nbsp;&nbsp;
                                                <input name="IsDemand" value="0" type="radio" id="Product" checked/> Product
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row product_fields">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsFixedPrice">
                                                <input name="IsFixedPrice" value="1" type="radio" id="FixedPrice" checked/> Fixed Price &nbsp;&nbsp;&nbsp;
                                                <input name="IsFixedPrice" value="0" type="radio" id="Bidding"/> Bidding
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ProductPrice"><span class="Bidding" style="display: none;">Starting </span>Price</label>
                                        <input type="text" name="ProductPrice" required  class="form-control" id="ProductPrice">
                                    </div>
                                </div>
                                <div class="col-md-3 product_fields FixedPrice">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DiscountedPrice">Discounted Price</label>
                                        <input type="text" name="DiscountedPrice" required  class="form-control" id="DiscountedPrice">
                                    </div>
                                </div>
                                <div class="col-md-3 Bidding" style="display: none;">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="EndBidDateTime">Bidding End Date Time</label>
                                        <input type="text" name="EndBidDateTime"   class="form-control datetimepicker" id="EndBidDateTime">
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Select Category</label>
                                        <select name="CategoryID" class="form-control" id="CategoryID" required>
                                            <?php echo getCategoryTreeForProduct(0, '',$language);?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row product_fields">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Quantity">Stock (Quantity)</label>
                                        <input type="text" name="Quantity" required  class="form-control" id="Quantity">
                                    </div>
                                </div>
                                <div class="col-md-3 delivery_charges">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DeliveryCharges">Shipping Price</label>
                                        <input type="text" name="DeliveryCharges" required  class="form-control" id="DeliveryCharges">
                                    </div>
                                </div>
                                <div class="col-md-3 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsDeliveryChargesInclude">
                                                <input name="IsDeliveryChargesInclude" value="1" type="checkbox" id="IsDeliveryChargesInclude" /> Included Shipping Price
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row product_fields FixedPrice">
                            <div class="col-md-12 col-sm-12">
                                <div class="multi-field-wrapper">
                                   <div class="multi-fields">
                                      <div class="row multi-field">
                                         <div class="col-md-3 col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label"> Add Variation Min Value</label>
                                                <input type="text" name="MinRang[]" class="form-control" placeholder="">

                                            </div>
                                         </div>
                                        <div class="col-md-3 col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label"> Add Variation Max Value</label>
                                                <input type="text" name="MaxRang[]" class="form-control" placeholder="">

                                            </div>
                                        </div>
                                         <div class="col-md-3 col-sm-3">
                                            <div class="form-group label-floating">
                                                <label class="control-label"> Add Price</label>
                                                <input type="text" name="Price[]" class="form-control">
                                            </div>
                                         </div>
                                         <div class="col-md-2 col-sm-2 remove-field">
                                            <button type="button" class="btn btn-danger">X</button>
                                         </div>
                                      </div>
                                   </div>
                                   <button type="button" class="add-field  btn btn-success">Add more</button>
                                </div>
                            </div>
                            </div>
                            <div class="row demand" style="display: none;">
                                
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Location">Location</label>
                                        <textarea class="form-control" name="Location" id="Location"></textarea>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description">Descritpion</label>
                                        <textarea class="form-control textarea" name="Description" id="Description" required></textarea>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group m-b-20">
                                        <label><?php echo lang('Image').'s'; ?> Upload upto 10 images</label><br>
                                        <input type="file" name="Image[]" id="filer_input1" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4 checkbox-radios product_fields FixedPrice">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="OutOfStock">
                                                <input name="OutOfStock" value="1" type="checkbox" id="OutOfStock"/> Out Of stock
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $(document).ready(function() {
    $('#IsDeliveryChargesInclude').on('click',function(){
            if($(this).prop("checked") == true){

                $('.delivery_charges').hide();
            }else{
                $('.delivery_charges').show();
            }
        });
        $('#Demand').on('click',function(){
            $('.demand').show();
            $('.Bidding').hide();
            $('.product_fields').hide();
        });
        $('#Product').on('click',function(){
            
            $('#FixedPrice').click();
            $('.demand').hide();
            $('.product_fields').show();
        });

        $('#FixedPrice').on('click',function(){
            $('.FixedPrice').show();
            $('.Bidding').hide();
        });
        $('#Bidding').on('click',function(){
            $('.FixedPrice').hide();
            $('.Bidding').show();
        });

    $('.multi-field-wrapper').each(function() {
        var $wrapper = $('.multi-fields', this);
        $(".add-field", $(this)).click(function(e) {
            $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
        });
    //  $('.remove-field').click(function() {
        
    //         $(this).parent('.multi-field').remove();
    // });
        $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
        });
});
     });
</script>
