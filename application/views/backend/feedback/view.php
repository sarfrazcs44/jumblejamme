<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <?php echo ($feedback->OrderNumber != '' ? 'Complain' : 'Feedback'); ?>
                            
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <?php if($feedback->OrderNumber != ''){ ?>
                                <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Order Number</label>
                                        <h5><?php echo $feedback->OrderNumber; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Full Name</label>
                                        <h5><?php echo $feedback->FullName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Email</label>
                                        <h5><?php echo $feedback->Email; ?></h5>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Mobile No</label>
                                        <h5><?php echo $feedback->MobileNo; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Created At</label>
                                        <h5><?php echo getFormattedDateTime($feedback->CreatedAt, 'd-m-Y h:i A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Message</label>
                                        <h5><?php echo $feedback->Message; ?></h5>
                                    </div>
                                </div>
                            </div>

                             
                            
                            

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        
    
    </div>
</div>
