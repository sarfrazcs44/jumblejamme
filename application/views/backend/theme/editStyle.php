<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Edit Style</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/updateStyle" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="ThemeStyleID" value="<?php echo $style_id; ?>">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="StyleTitleEn">Style Title En</label>
                                        <input type="text" name="StyleTitleEn" required  class="form-control" id="StyleTitleEn" value="<?php echo $result->StyleTitleEn; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="StyleTitleAr">Style Title Ar</label>
                                        <input type="text" name="StyleTitleAr" required  class="form-control" id="StyleTitleAr" value="<?php echo $result->StyleTitleAr; ?>" style="direction: rtl;">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group"><label>Style Image</label>
                                        <input type="file" name="StyleImage"  required accept="image/*">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" <?php echo ($result->IsActive == 1 ? 'checked' : ''); ?> /> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <button type="button" class="btn btn-default waves-effect m-l-5" onclick="window.history.back();">
                                    <?php echo lang('back');?>
                                </button>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>