<div class="main-wrapper">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="search-filter">
               <form action="">
                  <div class="col-md-3">
                     <select class="form-control" name="CategoryID" id="CategoryID">
                              <?php
                              $CategoryID  = 0;
                              if(isset($_GET['CategoryID'])){
                                 $CategoryID = $_GET['CategoryID'];
                              } 

                              echo getCategoryTreeForProduct(0, '','EN',$CategoryID);?>
                           </select>
                  </div>
              
                  <div class="col-md-5">
                     <input class="form-control" type="search" name="key" placeholder="Search By Keyword" value="<?php echo (isset($_GET['key']) ? $_GET['key'] : ''); ?>">
                  </div>
                  <div class="col-md-3 no-padding">
                     <div class="col-md-6 col-sm-6">
                        <input class="form-control" type="search" name="minprice" placeholder="Min-price" value="<?php echo (isset($_GET['minprice']) ? $_GET['minprice'] : ''); ?>">
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <input class="form-control" type="search" name="maxprice" placeholder="Max-price" value="<?php echo (isset($_GET['maxprice']) ? $_GET['maxprice'] : ''); ?>">
                     </div>
                  </div>
                  <div class="col-md-1">
                     <button type="submit" class="btn btn-default btn-search">Search</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
<?php if(!empty($bidding_products)){ ?>
      <div class="row">
<?php  foreach ($bidding_products as $key => $b_product) { ?>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="<?php echo base_url(get_images($b_product->ProductID,'ProductImage',FALSE)); ?>"> </div>
               <h4 class="title"><?php echo lang($b_product->Title);?></h4>
               <p id="counter<?php echo $key; ?>" class="counter-clr"></p>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$<?php echo $b_product->ProductPrice; ?></span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
      <?php } ?>
      </div>
   <?php } ?>
   <?php if($ShowPaggination){ ?>
      <div class="row">
         <div class="col-md-12">
            <div class="pagi001">
               <ul class="pagination">
                  <?php echo $links; ?>
               </ul>
            </div>
         </div>
      </div>
   <?php } ?>
   </div>
</div>
<script>
   // Set the date we're counting down to
   var countDownDate = new Date("Jan, 2020 12:37:25").getTime();
   
   // Update the count down every 1 second
   var x = setInterval(function() {
   
     // Get today's date and time
     var now = new Date().getTime();
       
     // Find the distance between now and the count down date
     var distance = countDownDate - now;
       
     // Time calculations for days, hours, minutes and seconds
     var days = Math.floor(distance / (1000 * 60 * 60 * 24));
     var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
     var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
     var seconds = Math.floor((distance % (1000 * 60)) / 1000);
     var milliseconds = Math.floor(((distance % (16.5 * 60))));
       
     // Output the result in an element with id="demo"
     document.getElementById("counter").innerHTML = hours + "h "
     + minutes + "m " + seconds + "s ";
   
       document.getElementById("counter1").innerHTML = hours + "h "
     + minutes + "m " + seconds + "s ";
   
       document.getElementById("counter2").innerHTML = hours + "h "
     + minutes + "m " + seconds + "s ";
   
       document.getElementById("counter3").innerHTML = hours + "h "
     + minutes + "m " + seconds + "s ";
       
     // If the count down is over, write some text 
     if (distance < 0) {
       clearInterval(x);
       document.getElementById("counter").innerHTML = "EXPIRED";
     }
   }, 1);
</script>