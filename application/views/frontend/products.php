<div class="main-wrapper">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="search-filter">
               <form action="">
                  <div class="col-md-3">
                     <select class="form-control" name="CategoryID" id="CategoryID">
                              <?php 
                              $CategoryID  = 0;
                              if(isset($_GET['CategoryID'])){
                                 $CategoryID = $_GET['CategoryID'];
                              }
                              echo getCategoryTreeForProduct(0, '','EN',$CategoryID);?>
                           </select>
                  </div>
                  <div class="col-md-5">
                     <input class="form-control" type="search" name="key" placeholder="Search By Keyword" value="<?php echo (isset($_GET['key']) ? $_GET['key'] : ''); ?>">
                  </div>
                  <div class="col-md-3">
                     <label class="check1">1
                     <input type="checkbox" checked="checked">
                     <span class="checkmark"></span>
                     </label>
                     or
                     <label class="check1">More
                     <input type="checkbox">
                     <span class="checkmark"></span>
                     </label>         
                  </div>
                  <div class="col-md-1">
                     <button type="submit" class="btn btn-default btn-search">Search</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <?php if(!empty($products)){ ?>
      <div class="row">
         <?php  foreach ($products as $key => $product) { ?>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="<?php echo base_url(get_images($product->ProductID,'ProductImage',FALSE)); ?>"> </div>
               <h4 class="title"><a href="<?php echo base_url('products/detail/'.$product->ProductID);?>"><?php echo lang($product->Title);?></a></h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                    <?php if($product->DiscountedPrice > 0 ){ ?>
                        <span class="price-new">$<?php echo $product->DiscountedPrice; ?></span> <del class="price-old">$<?php echo $product->ProductPrice; ?></del>

                    <?php }else{ ?>
                        <span class="price-new">$<?php echo $product->ProductPrice; ?></span>

                    <?php } ?>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="<?php echo base_url('products/detail/'.$product->ProductID);?>" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <?php } ?>
         
         
      </div>
      <?php } ?>
      <?php if($ShowPaggination){ ?>
      <div class="row">
         <div class="col-md-12">
            <div class="pagi001">
               <ul class="pagination">
                  <?php echo $links; ?>
               </ul>
            </div>
         </div>
      </div>
   <?php } ?>
   </div>
</div>