<div class="wrapper">
<div class='container'>
   <div class='row'>
      <div class='col-md-12'>
         <div id='mainContentWrapper'>
            <div class="col-md-12">
               <h3 class="main-heading005">
                  <?php echo lang('Review Your Order & Complete Checkout');?>
               </h3>
               <div class="shopping_cart">
                 <form action="<?php echo base_url('checkout/place_order');?>" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                     <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <h4 class="panel-title">
                                 <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> <?php echo lang('Review
                                 Your Order');?></a>
                              </h4>
                           </div>
                           <div id="collapseOne" class="panel-collapse collapse in">
                              <div class="panel-body">
                                 <?php 
                                 $total = 0;
                                 $DeliveryCharges = 0;
                                 foreach ($products as $key => $value) { 
                                    $image = get_images($value->ProductID,"ProductImage",false);
                                    

                                    if($value->IsDeliveryChargesInclude == 0){
                                       $DeliveryCharges += rateAccordingToSetCurrency($value->DeliveryCharges,$currency_rate);
                                    }

                                    ?> 

                                    <div class="cart-listing">
                                    <div class="row">
                                       <div class="col-md-1 col-sm-1"><img src="<?php echo base_url($image);?>" class="cart-list-img" /> </div>
                                       <div class="col-md-4 col-sm-4 cart005"><?php echo $value->ProductTitle; ?></div>
                                       <div class="col-md-2 col-sm-2 cart005"><?php echo ($value->IsActive ==  1 ? lang('In stock') : lang('Out of stock')); ?></div>
                                       <div class="col-md-2 col-sm-2"><input type="number" class="form1 qty change_quantity" name="Quantity[]" data-temp-id = "<?php echo $value->TempOrderID; ?>" value="<?php echo $value->ProductQuantity; ?>"></div>
                                       <div class="col-md-2 col-sm-2 cart005"><?php 
                                       if($value->ProductVariantID > 0){
                                          $price = rateAccordingToSetCurrency($value->VariantPrice,$currency_rate) * $value->ProductQuantity;
                                       }else{
                                          $price = rateAccordingToSetCurrency($value->ProductPrice,$currency_rate) * $value->ProductQuantity;
                                       }
                                       echo $price;
                                       $total += $price;

                                       ?> €</div>
                                       <div class="col-md-1 col-sm-1 cart005"><button class="btn btn-sm btn-danger delete" data-tem-id="<?php echo $value->TempOrderID;?>"><i class="fa fa-trash"></i> </button> </div>
                                    </div>
                                 </div>
                                   
                                <?php } ?>
                                 
                                 
                                 <div class="cart-list-payment">
                                    <div class="row">
                                       <div class="col-md-10">
                                          <div class="pull-right"> <?php echo lang('Subtotal'); ?>:</div>
                                       </div>
                                       <div class="col-md-2">
                                          <div class="pull-right">$<?php echo $total; ?></div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-10">
                                          <div class="pull-right"> <?php echo lang('Shipping Charges'); ?>:</div>
                                       </div>
                                       <div class="col-md-2">
                                          <div class="pull-right">$<?php echo $DeliveryCharges; ?></div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-10">
                                          <div class="pull-right total-prc"> <?php echo lang('Total'); ?>:</div>
                                       </div>
                                       <div class="col-md-2">
                                          <div class="pull-right total-prc">$<?php echo $total + $DeliveryCharges; ?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="shippingbtn"><a style="width:auto;" data-toggle="collapse"
                        data-parent="#accordion" href="#collapseTwo" class=" btn btn-success" onclick="$(this).fadeOut(); $('#payInfo').fadeIn();"><?php echo lang('Continue
                        to Billing Information'); ?>»</a>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><?php echo lang('Contact
                              and Billing Information'); ?></a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                           <div class="panel-body">
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label><?php echo lang('First Name'); ?></label>
                                       <input type="text" name="FirstName" class="form-control">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label><?php echo lang('Last Name'); ?></label>
                                       <input type="text" name="LastName" class="form-control">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label><?php echo lang('Phone no'); ?></label>
                                       <input type="text" name="Phone" class="form-control">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label><?php echo lang('Country'); ?></label>
                                       <select class="form-group" name="CountryID" id="country">
                                          <option value=""><?php echo lang('Select Country'); ?></option>
                                         <?php foreach ($countries as $key => $value) { ?>
                                                   <option value="<?php echo $value->CountryID; ?>"><?php echo $value->Title.' ('.$value->CountryCode.')'; ?></option>
                                          <?php }?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label><?php echo lang('State'); ?></label>
                                       <select  class="form-control selectpicker" id="states" name="StateID">
                                          
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label><?php echo lang('City'); ?></label>
                                       <select class="form-control selectpicker"  name="CityID" id="city">
                                          
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-9">
                                    <div class="form-group">
                                       <div class="form-group">
                                          <label><?php echo lang('Shipping Address'); ?></label>
                                          <input type="text" name="ShippingAddress" class="form-control">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                       <div class="form-group">
                                          <label><?php echo lang('Zip Code'); ?></label>
                                          <input type="text" name="ShippingZipCode" class="form-control">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div style="text-align: right; margin-bottom:20px;"><a data-toggle="collapse"
                        data-parent="#accordion"
                        href="#collapseThree"
                        class=" btn   btn-success" id="payInfo"
                        style="width:auto;display: none;" onclick="$(this).fadeOut();  
                        document.getElementById('collapseThree').scrollIntoView()"><?php echo lang('Enter Payment Information'); ?> »</a>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                              <b><?php echo lang('Payment Information'); ?></b>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                           <div class="panel-body">
                              <span class='payment-errors'></span>
                              <div class="col-md-8 col-sm-6">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label><?php echo lang('Name on Card'); ?></label>
                                       <input type="text" class="form-control" stripe-data="name"
                                          id="name-on-card" placeholder="Card Holder's Name" name="NameOnCard">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label><?php echo lang('Card Number'); ?></label>
                                       <input type="text" class="form-control" stripe-data="name"
                                          id="name-number" placeholder="Debit/Credit card number" name="CardNumber">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label><?php echo lang('Expiration Date'); ?></label>
                                       <input type="text" class="form-control" stripe-data="name"
                                          id="name-expiry" placeholder="mm/yyyy" name="CardExpiry">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label><?php echo lang('Card CVC'); ?></label>
                                       <input type="text" class="form-control" stripe-data="cvc"
                                          id="card-cvc" placeholder="Security Code" name="CardCVC">
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <img class=" payment-images" src="<?php echo base_url();?>assets/frontend/images/cc.png">
                                 </div>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <div class="save-card"><input type="checkbox" name="SaveCardDetails" id="savecard"> <label for="savecard"> <?php echo lang('Save Card Details'); ?></label> </div>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <button type="submit" class="btn btn-success btn-lg" style="width:auto;"><?php echo lang('Pay Now'); ?>
                                    
                                    </button>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <p style="margin-top:20px;"><?php echo lang('By submiting this order you are agreeing to our'); ?>
                                     <a href="#"><?php echo lang('universal
                                    billing agreement'); ?></a>, <?php echo lang('and'); ?> <a href="#"><?php echo lang('terms of service'); ?></a>.
                                    <?php echo lang('If you have any questions about our products or services please contact us
                                    before placing this order.'); ?>
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
</div>

<script>
   $(document).ready(function () {
      $('#country').on('change',function(){

               var CountryID = $(this).val();

               if(CountryID == '')
               {
                   $('#states').html('<option value=""><?php echo lang("choose_state");?></option>');
               }
               else
               {
                   $.blockUI({
                       css: {
                           border: 'none',
                           padding: '15px',
                           backgroundColor: '#000',
                           '-webkit-border-radius': '10px',
                           '-moz-border-radius': '10px',
                           opacity: .5,
                           color: '#fff'
                       }
                   });


                   $.ajax({
                       type: "POST",
                       url: base_url + 'index/getCountryStates',
                       data: {
                           'CountryID': CountryID
                       },
                       dataType: "json",
                       cache: false,
                       //async:false,
                       success: function(result) {

                           $('#states').html(result.html);
                           if(result.array)
                           {
                               $(".selectpicker").selectpicker('refresh');
                           }
                       },
                       complete: function() {
                           $.unblockUI();
                       }
                   });
               }

          });
          $('#states').on('change',function(){

               var StateID = $(this).val();

               if(StateID == '')
               {
                   $('#city').html('<option value=""><?php echo lang("choose_city");?></option>');
               }
               else
               {
                   $.blockUI({
                       css: {
                           border: 'none',
                           padding: '15px',
                           backgroundColor: '#000',
                           '-webkit-border-radius': '10px',
                           '-moz-border-radius': '10px',
                           opacity: .5,
                           color: '#fff'
                       }
                   });


                   $.ajax({
                       type: "POST",
                       url: base_url + 'index/getStateCities',
                       data: {
                           'StateID': StateID
                       },
                       dataType: "json",
                       cache: false,
                       //async:false,
                       success: function(result) {

                           $('#city').html(result.html);
                           if(result.array)
                           {
                               $(".selectpicker").selectpicker('refresh');
                           }

                       },
                       complete: function() {
                           $.unblockUI();
                       }
                   });
               }

       });
   });
</script>