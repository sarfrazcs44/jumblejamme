<div class="wrapper">
   <div class="container-fluid">
      <section class="main02">
         <div class="col-md-12">
            <div class="col-md-6">
               <div class="side-heading">
                  <h2>Welcome to Jumblejam</h2>
                  <p>You are 30 seconds away from earning your own money!</p>
               </div>
            </div>
            <div class="col-md-6">
               <h2 class="create-account">Create a Account</h2>
               <form action="<?php echo base_url('account/action');?>" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>First Name</label>     
                           <input type="text" name="FirstName" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Last Name</label>     
                           <input type="text" name="LastName" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <label>Gender</label>     
                        <div class="form-group">    
                           <input type="radio" name="Gender" value="Male" checked> <span>Male</span>
                           <input type="radio" name="Gender" value="Female"> <span>Female</span>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Date of birth</label>     
                           <input type="date" name="Dob" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Country</label>     
                           <select class="form1" name="CountryID" id="country">
                              <option value="" class="animated_item">Select Country</option>
                             <?php foreach ($countries as $key => $value) { ?>
                                       <option value="<?php echo $value->CountryID; ?>" class="animated_item"><?php echo $value->Title.' ('.$value->CountryCode.')'; ?></option>
                              <?php }?>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>State</label>     
                           <select class="form1" name="StateID" id="states">
                             
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>City</label>     
                           <select class="form1" name="CityID" id="city">
                              
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Zip Code</label>     
                           <input type="text" name="ZipCode" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Phone Number</label>     
                           <input type="text" name="Mobile" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Email Address</label>     
                           <input type="text" name="Email" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Password</label>     
                           <input type="password" name="Password" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Confirm Password(Min 8 characters)</label>     
                           <input type="password" name="ConfirmPassword" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-12">
                        By creating an account, you agree to Jublejam's Conditions of Use and Privacy Notice.
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <input type="submit" name="" required="" value="Register" class="regis-btn">
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
   </div>
   </section>
</div>
<script>
   $(document).ready(function () {
      $('#country').on('change',function(){

               var CountryID = $(this).val();

               if(CountryID == '')
               {
                   $('#StateID').html('<option value=""><?php echo lang("choose_state");?></option>');
               }
               else
               {
                   $.blockUI({
                       css: {
                           border: 'none',
                           padding: '15px',
                           backgroundColor: '#000',
                           '-webkit-border-radius': '10px',
                           '-moz-border-radius': '10px',
                           opacity: .5,
                           color: '#fff'
                       }
                   });


                   $.ajax({
                       type: "POST",
                       url: base_url + 'index/getCountryStates',
                       data: {
                           'CountryID': CountryID
                       },
                       dataType: "json",
                       cache: false,
                       //async:false,
                       success: function(result) {

                           $('#states').html(result.html);
                           if(result.array)
                           {
                               $(".selectpicker").selectpicker('refresh');
                           }
                       },
                       complete: function() {
                           $.unblockUI();
                       }
                   });
               }

          });
          $('#states').on('change',function(){

               var StateID = $(this).val();

               if(StateID == '')
               {
                   $('#CityID').html('<option value=""><?php echo lang("choose_city");?></option>');
               }
               else
               {
                   $.blockUI({
                       css: {
                           border: 'none',
                           padding: '15px',
                           backgroundColor: '#000',
                           '-webkit-border-radius': '10px',
                           '-moz-border-radius': '10px',
                           opacity: .5,
                           color: '#fff'
                       }
                   });


                   $.ajax({
                       type: "POST",
                       url: base_url + 'index/getStateCities',
                       data: {
                           'StateID': StateID
                       },
                       dataType: "json",
                       cache: false,
                       //async:false,
                       success: function(result) {

                           $('#city').html(result.html);
                           if(result.array)
                           {
                               $(".selectpicker").selectpicker('refresh');
                           }

                       },
                       complete: function() {
                           $.unblockUI();
                       }
                   });
               }

       });
   });
</script>