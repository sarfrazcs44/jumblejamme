<div class="main-wrapper">
   <div class="container">
      <div class="row">
         <div class="preview01 col-md-6">
            <div id="carousel" class="carousel slide" data-ride="carousel">
               <?php 
               
               if($images){ 
                  $small_images = '<div class="item active">';

                  ?> 

               <div class="carousel-inner">

                  <?php foreach ($images as $key => $value) {
                        if($key % 4 == 0 && $key != 0){
                           echo $small_images .= '</div>';
                           $small_images .= '<div class="item active">';
                        }
                        $small_images .= '<div data-target="#carousel" data-slide-to="'.$key.'" class="thumb"><img src="'.base_url($value->ImageName).'"></div>';

                   ?>

                     <div class="item <?php echo ($key == 0 ? 'active' : ''); ?> " data-thumb="<?php echo $key; ?>">
                        <img src="<?php echo base_url($value->ImageName); ?>">        
                     </div>
                    
                    <?php
                  }

                  $small_images .= '</div>';

               } 

               ?>
                  
               
               </div>
            </div>
            <div class="clearfix">
               <div id="thumbcarousel" class="carousel slide" data-interval="false">
                  <?php if($images){ ?>
                  <div class="carousel-inner">
                     <?php echo $small_images; ?>
                     <!-- /item -->
                  </div>
               <?php } ?>
                  <!-- /carousel-inner -->
                  <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  </a>
                  <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  </a>
               </div>
               <!-- /thumbcarousel -->
            </div>
            <!-- /clearfix -->
         </div>
         <div class="details col-md-6">
            <h3 class="product-title01"><?php echo lang($product->Title);?></h3>
            <div class="rating">
               <div class="stars">
                  <?php $avg_rating  = productAverageRating($product->ProductID);
                        for($i=0;$i<5;$i++){ 
                           $checked = '';
                              if($i < $avg_rating){
                                 $checked = 'checked';
                              }
                           ?>
                              <span class="fa fa-star <?php echo $checked; ?>"></span>
                        <?php }

                  ?> 
                  
               </div>
               <?php $rating = productRatings($product->ProductID); ?>
               <span class="review-no"> <?php echo $rating['total_ratings_count'].' '.lang('reviews');?></span>
            </div>
            <p class="product-description01"><?php echo lang($product->Description);?></p>
            <h4 class="price"> price: <span>$ <?php echo ($product->DiscountedPrice > 0 ? rateAccordingToSetCurrency($product->DiscountedPrice,$currency_rate) : rateAccordingToSetCurrency($product->ProductPrice,$currency_rate)); ?></span></h4>
            <!--<p class="vote"><strong>91%</strong> of buyers enjoyed this product!</p>-->
            <div class="quantity" style="padding-bottom:20px;">
               <h4> Quantity </h4>
               <div>
                  <div class="btn-minus"><span class="glyphicon glyphicon-minus"></span></div>
                  <input value="1"  id="quantity_field"/>
                  <div class="btn-plus"><span class="glyphicon glyphicon-plus"></span></div>
               </div>
            </div>
            <?php if($variants){ ?>
            <div class="variation">
               <select class="form1" id="product_variant">
                  <option value=""><?php echo lang('Select Variant');?></option>
                  <?php foreach ($variants as $value) { ?>
                     <option value="<?php echo $value->ProductPriceVariationID; ?>"><?php echo $value->MinRang.' - '.$value->MaxRang.' for '.rateAccordingToSetCurrency($value->Price,$currency_rate); ?></option>
                     <?php
                  }
                  ?>
                  
               </select>
            </div>
         <?php } ?>
            <div class="action">
               <button class="add-to-cart01 btn btn-default add_to_cart"  data-pro-id="<?php echo $product->ProductID; ?>" type="button">add to cart</button>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="desc005">
               <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#home"><?php echo lang('Product Description');?></a></li>
                  <li><a data-toggle="tab" href="#menu1"><?php echo lang('Specification');?></a></li>
               </ul>
               <div class="tab-content">
                  <div id="home" class="tab-pane fade in active">
                     <h3><?php echo lang('Description');?></h3>
                     <p><?php echo lang($product->Description);?></p>
                  </div>
                  <div id="menu1" class="tab-pane fade">
                     <h3>Specification</h3>
                     <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <h2 class="review-heading">Reviews & Rating</h2>
         </div>
         <!--          <div class="col-md-8">
            -->         <!--    <div class="form-group" id="rating-ability-wrapper">
            <form>
               <label class="control-label" for="rating">
               <span class="field-label-header">How would you rate Our Product*</span><br>
               <span class="field-label-info"></span>
               <input type="hidden" id="selected_rating" name="selected_rating" value="" required="required">
               </label>
               <div class="bold rating-header" style="">
                  <span class="selected-rating">0</span><small> / 5</small>
               </div>
               <span class="btnrating btn btn-default btn-md" data-attr="1" id="rating-star-1">
               <i class="fa fa-star" aria-hidden="true"></i>
               </span>
               <span class="btnrating btn btn-default btn-md" data-attr="2" id="rating-star-2">
               <i class="fa fa-star" aria-hidden="true"></i>
               </span>
               <span class="btnrating btn btn-default btn-md" data-attr="3" id="rating-star-3">
               <i class="fa fa-star" aria-hidden="true"></i>
               </span>
               <span class="btnrating btn btn-default btn-md" data-attr="4" id="rating-star-4">
               <i class="fa fa-star" aria-hidden="true"></i>
               </span>
               <span class="btnrating btn btn-default btn-md" data-attr="5" id="rating-star-5">
               <i class="fa fa-star" aria-hidden="true"></i>
               </span>
            </div>
            <div class="comment-box">
            <b>Ask Question/Comment</b>
            <textarea size="3" class="form1"></textarea>
            <input type="submit" name="" class="btn btn-default comment-btn" value="Post">
            </div>
            </form>
            </div> -->
         <!-- </div> -->
         <div class="col-md-8">
            <?php if($reviews){
                foreach ($reviews as $value) { ?>
                   
               <div class="comment-1">
               <div class="comment-user-img">
                  <div class="comment-img"><img src="<?php echo base_url($value->Image); ?>" class="comment-user-img"></div>
                  <div class="name-date">
                     <div class="comment-name"><?php echo $value->FullName; ?></div>
                     <div class="comment-date">Date: <span><?php echo date("j M G",strtotime($value->CreatedAt)); ?></span></div>
                     <div class="rating">
                        <div class="stars">
                           <?php for($i=0;$i<5;$i++){
                              $checked = '';
                              if($i < $value->Rating){
                                 $checked = 'checked';
                              }

                            ?>
                           <span class="fa fa-star <?php echo $checked; ?>"></span>
                        <?php } ?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="message">
                  <p><?php echo $value->Review; ?></p>
               </div>
            </div>

               <?php }

            }
            ?>
            
            
         </div>
         <div class="col-xs-12 col-md-4">
            <div class="well well-sm">
               <div class="row">
                  <div class="col-xs-12 col-md-6 text-center">
                     <h1 class="rating-num">
                        <?php echo $avg_rating; ?>
                     </h1>
                     <div class="rating">
                        <span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star">
                        </span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star">
                        </span><span class="glyphicon glyphicon-star-empty"></span>
                     </div>
                     <div>
                        <span class="glyphicon glyphicon-user"></span><?php echo $rating['total_ratings_count']; ?> <?php echo lang('total');?>
                     </div>
                  </div>
                  <div class="col-xs-12 col-md-6">
                     <div class="row rating-desc">
                        <div class="col-xs-3 col-md-3 text-right">
                           <span class="glyphicon glyphicon-star"></span>5
                        </div>
                        <div class="col-xs-8 col-md-9">
                           <div class="progress progress-striped">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20"
                                 aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($rating['total_ratings_count'] != 0 ? (($rating['rating_5'] / $rating['total_ratings_count']) * 100) : 0); ?>%">
                                 <span class="sr-only"><?php echo ($rating['total_ratings_count'] != 0 ? (($rating['rating_5'] / $rating['total_ratings_count']) * 100) : 0); ?>%</span>
                              </div>
                           </div>
                        </div>
                        <!-- end 5 -->
                        <div class="col-xs-3 col-md-3 text-right">
                           <span class="glyphicon glyphicon-star"></span>4
                        </div>
                        <div class="col-xs-8 col-md-9">
                           <div class="progress">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20"
                                 aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($rating['total_ratings_count'] != 0 ? (($rating['rating_4'] / $rating['total_ratings_count']) * 100) : 0); ?>%">
                                 <span class="sr-only"><?php echo ($rating['total_ratings_count'] != 0 ? (($rating['rating_4'] / $rating['total_ratings_count']) * 100) : 0); ?>%</span>
                              </div>
                           </div>
                        </div>
                        <!-- end 4 -->
                        <div class="col-xs-3 col-md-3 text-right">
                           <span class="glyphicon glyphicon-star"></span>3
                        </div>
                        <div class="col-xs-8 col-md-9">
                           <div class="progress">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20"
                                 aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($rating['total_ratings_count'] != 0 ? (($rating['rating_3'] / $rating['total_ratings_count']) * 100) : 0); ?>%">
                                 <span class="sr-only"><?php echo ($rating['total_ratings_count'] != 0 ? (($rating['rating_3'] / $rating['total_ratings_count']) * 100) : 0); ?>%</span>
                              </div>
                           </div>
                        </div>
                        <!-- end 3 -->
                        <div class="col-xs-3 col-md-3 text-right">
                           <span class="glyphicon glyphicon-star"></span>2
                        </div>
                        <div class="col-xs-8 col-md-9">
                           <div class="progress">
                              <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20"
                                 aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($rating['total_ratings_count'] != 0 ? (($rating['rating_2'] / $rating['total_ratings_count']) * 100) : 0); ?>%">
                                 <span class="sr-only"><?php echo ($rating['total_ratings_count'] != 0 ? (($rating['rating_2'] / $rating['total_ratings_count']) * 100) : 0); ?>%</span>
                              </div>
                           </div>
                        </div>
                        <!-- end 2 -->
                        <div class="col-xs-3 col-md-3 text-right">
                           <span class="glyphicon glyphicon-star"></span>1
                        </div>
                        <div class="col-xs-8 col-md-9">
                           <div class="progress">
                              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80"
                                 aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($rating['total_ratings_count'] != 0 ? (($rating['rating_1'] / $rating['total_ratings_count']) * 100) : 0); ?>%">
                                 <span class="sr-only"><?php echo ($rating['total_ratings_count'] != 0 ? (($rating['rating_1'] / $rating['total_ratings_count']) * 100) : 0); ?>%</span>
                              </div>
                           </div>
                        </div>
                        <!-- end 1 -->
                     </div>
                     <!-- end row -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function(){
           //-- Click on detail
       
   
           //-- Click on QUANTITY
           $(".btn-minus").on("click",function(){
               var now = $(".quantity > div > input").val();
               if ($.isNumeric(now)){
                   if (parseInt(now) -1 > 0){ now--;}
                   $(".quantity > div > input").val(now);
               }else{
                   $(".quantity > div > input").val("1");
               }
           })            
           $(".btn-plus").on("click",function(){
               var now = $(".quantity > div > input").val();
               if ($.isNumeric(now)){
                   $(".quantity > div > input").val(parseInt(now)+1);
               }else{
                   $(".quantity > div > input").val("1");
               }
           })                        
       }) 
</script>