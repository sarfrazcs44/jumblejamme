<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Jumble Jam</title>
      <!-- Bootstrap core CSS -->
      <link href="<?php echo base_url(); ?>assets/frontend/css/bootstrap.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/frontend/css/font-awesome.css" rel="stylesheet">
      <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/themify-icons.css">-->
      <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/frontend/img/favicon.png">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/owl.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/chat.css">

      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/style.css">


      <script src="<?php echo base_url(); ?>assets/frontend/js/jquery-3.2.1.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/frontend/js/bootstrap.js"></script>
   </head>
   <body>
      <header>
         <!--end shopping-cart -->
         <!-- Static navbar -->
         <div class="container-fluid">
            <div class="shopping-cart">
               <!-- <div class="shopping-cart-header">
                  <div class="shopping-cart-total">
                    <span class="lighter-text">Total:</span>
                    <span class="main-color-text total">$461.15</span>
                  </div>
                  </div>  --><!--end shopping-cart-header -->

                  <?php 
                  $cart_items = array();
                  $user_id_to_get_cart_item = '';
                  $total_cart_item = '';
                  if($this->session->userdata('user')) {

                      $user_id_to_get_cart_item = $this->session->userdata['user']['UserID'];

                     } else {
                      $user_id_to_get_cart_item = get_cookie('temp_order_key');
                     }

                     if($user_id_to_get_cart_item != ''){

                        $cart_items = getCartItems($user_id_to_get_cart_item);
                        $total_cart_item = getTotalProduct($user_id_to_get_cart_item);
                     }

           if(!empty($cart_items)){
//print_rm($cart_items);
                  ?>
               <ul class="shopping-cart-items">
                  <?php foreach ($cart_items as $cart) { 

                     $price = $cart->DiscountedPrice > 0 ? $cart->DiscountedPrice : $cart->ProductPrice;

                     $image_header_cart = get_images($cart->ProductID,'ProductImage',false);
                     ?>
                   
                 
                  <li class="clearfix">
                     <img src="<?php echo base_url($image_header_cart); ?>" alt="item1" />
                     <span class="item-name"><?php echo $cart->ProductTitle; ?></span>
                     <?php if($cart->ProductVariantID > 0){
                        $price = $cart->VariantPrice;
                      ?>
                     <span class="item-detail"><?php echo lang('Pack').' '.$cart->MaxRang; ?></span>
                  <?php } ?>
                     <span class="item-price">$<?php echo rateAccordingToSetCurrency(($price * $cart->ProductQuantity),$currency_rate); ?></span>
                     <span class="item-quantity"><?php echo lang('Quantity');?>: <?php echo $cart->ProductQuantity; ?></span>
                  </li>
               <?php } ?>
                  
               </ul>
               <a href="<?php echo ($this->session->userdata('user') ? base_url('checkout/order_detail') : 'javascript:void(0);');?>" <?php echo ($this->session->userdata('user') ?  '' : 'data-toggle="modal" data-target="#myModal"')?> class="checkout-btn"><?php echo lang('Checkout');?> <i class="fa fa-chevron-right"></i></a>
            <?php }   ?>
            </div>
            <div class="top-header">
               <div class="row">
                  <div class="col-md-2 col-sm-2 col-xs-12">
                     <a href="<?php echo base_url();?>"><img src="<?php echo base_url(); ?>assets/frontend/images/logo.png" class="img-responsive logo"></a>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                     <div class="input-group filter001">
                        <div class="input-group-btn search-panel">
                           <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                           <span id="search_concept"><?php echo lang('Filter by');?></span> <span class="caret"></span>
                           </button>
                           <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Mobile</a></li>
                              <li><a href="#">Furniture</a></li>
                              <li><a href="#">Electronics</a></li>
                           </ul>
                        </div>
                        <input type="hidden" name="search_param" value="all" id="search_param">         
                        <input type="text" class="form-control" name="x" placeholder="Search Anything Here">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-4  col-xs-12 cart-icon001">
                     <ul>
                        <li class="support">
                           <div class="form-group">
                              <select class="form-control" onchange="changeLanguage(this.value);">
                                 <?php
                                 if($getSystemLanguages)
                                 {
                                    foreach ($getSystemLanguages as $key => $lang) 
                                    {
                                 ?>
                                       <option value="<?php echo $lang->ShortCode;?>" <?php echo ($language_code == $lang->ShortCode ? 'selected="selected"' : '');?>><?php echo $lang->ShortCode;?></option>
                                 <?php
                                    }
                                 }
                                 ?>
                              </select>
                           </div>
                        </li>
                        <li class="support">
                           <div class="form-group">
                              <select class="form-control" onchange="changeCurrency(this.value);">
                                 <?php
                                 if($getCurrencies)
                                 {
                                    foreach ($getCurrencies as $key => $value) 
                                    {
                                 ?>
                                       <option value="<?php echo $value->CurrencyCode;?>" <?php echo ($currency_code == $value->CurrencyCode ? 'selected="selected"' : '');?>><?php echo $value->CurrencyCode;?></option>
                                 <?php
                                    }
                                 }
                                 ?>
                              </select>
                           </div>
                        </li>
                        <?php if($this->session->userdata('user')){ ?>
                        <li class="support">
                           <a href="#" class="btn btn-default add-new" data-toggle="modal" data-target="#addpro"><?php echo lang('Add New Post');?></a>
                        </li>
                        
                     <?php
                      } ?>
                     <li>  <a href="javascript:void(0);" <?php echo ($total_cart_item>0 ? 'id="cart"' : '');?>><i class="fa fa-shopping-cart"></i> <?php echo ($total_cart_item>0 ? '<span class="badge">'.$total_cart_item.'</span>' : '');?></a></li>
                        
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="menu001">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                     <a class="navbar-brand" href="#"></a>
                  </div>
                  <div id="navbar" class="navbar-collapse collapse">
                     <ul class="nav navbar-nav">
                        <li class="active"></li>
                        <li></li>
                        <li></li>
                        <li class="dropdown">
                           <ul class="dropdown-menu">
                              <li></li>
                              <li></li>
                              <li><a href="#">Something else here</a></li>
                              <li></li>
                              <li></li>
                           </ul>
                        </li>
                     </ul>
                     <ul class="nav navbar-nav navbar-left">
                        <ul class="nav navbar-nav">
                           <li class="<?php echo ($url_string == '' || $url_string == 'index' ? 'active' : '');?>"><a href="<?php echo base_url();?>"><?php echo lang('Home');?></a></li>
                           <li class="<?php echo ($url_string == 'demands' ? 'active' : '');?>"><a href="<?php echo base_url('demands');?>">Demands</a></li>
                           <li class="<?php echo ($url_string == 'bidding' ? 'active' : '');?>"><a href="<?php echo base_url('bidding');?>">Bidding</a></li>
                           <li class="<?php echo ($url_string == 'products' ? 'active' : '');?>"><a href="<?php echo base_url('products');?>">Products</a></li>
                           </li>
                        </ul>
                     </ul>
                     <ul class=" nav navbar-nav navbar-right">
                        <?php if(!$this->session->userdata('user')){ ?>
                        <li><a href="#" data-toggle="modal" data-target="#myModal">Login</a></li>
                        <li><a href="<?php echo base_url('account/registration');?>">Register</a></li>
                     <?php }else{ ?>
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><b>Hello</b> <span><?php echo $this->session->userdata['user']['FullName']; ?>!</span> <span class="caret"></span></a>
                           <ul class="dropdown-menu">
                              <li><a href="profile.php">My Profile</a></li>
                              <li><a href="<?php echo base_url('customer/bidding');?>">My Biddings</a></li>
                              <li><a href="#">My Ads</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="<?php echo base_url('account/logout');?>">Log Out</a></li>
                           </ul>
                        </li>
                     <?php } ?>
                     </ul>
                  </div>
                  <!--/.nav-collapse -->
               </div>
               <!--/.container-fluid -->
            </nav>
         </div>
      </header>
      <script>
          var base_url = '<?php echo base_url(); ?>';
          var delete_msg = '<?php echo lang('are_you_sure');?>';
      </script>