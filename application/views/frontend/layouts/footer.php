<footer>
   <div class="container-fluid">
   <div class="row footer-bar">
      <div class="col-md-3 col-sm-6 col-xs-12">
         <h4 class="useful-link">ABOUT US</h4>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the dus PageMaker including versions of Lorem I</p>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <h4 class="useful-link">USEFUL LINKS</h4>
         <h5>Events</h5>
         <div class="line-bar"></div>
         <h5>Support</h5>
         <div class="line-bar"></div>
         <h5>Hosting</h5>
         <div class="line-bar"></div>
         <h5>Career</h5>
         <div class="line-bar"></div>
         <h5>Blog</h5>
         <div class="line-bar"></div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <h4 class="useful-link">FOLLOW US</h4>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
         <a class="social-icon" href="#"><i class="fa fa-facebook-square fa-3x social"></i></a>
         <a class="social-icon" href="#"><i class="fa fa-twitter-square fa-3x social"></i></a>
         <a class="social-icon" href="#"><i class="fa fa-google-plus-square fa-3x social"></i></a>
         <a class="social-icon" href="#"><i class="fa fa-envelope-square fa-3x social"></i></a>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <h4 class="useful-link">NEWSLETTER</h4>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
         <input type="text" name="News" placeholder="News" class="new-bar">
         <input type="button" name="" value="Subcribe" class="Subcribe-bar">
      </div>
   </div>
   <div class="row copy-bar">
   <div class="col-md-12 col-sm-6 col-xs-12">
      <h5>Copyright 2019-20 Jumble jam All right Reserved </h5>
   </div>
</footer>
<!-- 
   <script src="js/owl.js"></script> -->
<script type="text/javascript">
   if ($(window).width() > 560) {     
   document.write('\x3Cscript type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/owl.js">\x3C/script>');
   }
</script>

<script type="text/javascript">
   $('#show').click(function(){
   $('#showcomment').css('display', 'inline-block');
   })
</script>
<script type="text/javascript">
   $(document).ready(function(){
     
     $("#showPanel").click(function(){
         $("#chat-box").show();
     });
     
     $("#hidePanel").click(function(){
         $("#chat-box").hide();
     });
   
   });
   
</script>

<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-notify.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/all-custom.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/script.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.datatables.js"></script>
</body>
</html>
<!-- login signup popup -->
<div class="modal fade" id="myModal" role="dialog">
   <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
            <a href="#" class="cross" data-dismiss="modal"><i class="fa fa-remove"></i></a>
            <ul class="nav nav-tabs">
               <li class="active"><a data-toggle="tab" href="#home"><i class="fa fa-sign-in"></i> Login</a></li>
               <li><a href="<?php echo base_url('account/registration');?>"> <i class="fa fa-user-plus"></i> Registration</a></li>
            </ul>
            <div class="tab-content">
               <div id="home" class="tab-pane fade in active">
                  <div class="login-1">
                    <form action="<?php echo base_url('account/checkLogin');?>" method="post" onsubmit="return false;"  class="loginForm form_data">
                        <div class="form-group">
                           <label>Email</label>
                           <input type="text" name="Email">
                        </div>
                        <div class="form-group">
                           <label>Password</label>
                           <input type="password" name="Password">
                        </div>
                        <div class="form-group">         
                           <input type="Submit" name="" value="Login">
                        </div>
                     </form>
                     <div class="right"><a href="#" class="forget">Forget Password?</a></div>
                  </div>
               </div>
               <div id="menu2" class="tab-pane fade">
                  <div class="login-1">
                     <form action="profile.php">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>Your Name</label>
                                 <input type="text" name="" >
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>Email</label>
                                 <input type="text" name="" >
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>Password</label>
                                 <input type="password" name="" placeholder="At least 6 characters">
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>Re-enter Password </label>
                                 <input type="password" name="" placeholder="Re-enter Password">
                              </div>
                           </div>
                           <div class="col-md-12">
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">         
                                 <input type="Submit" name="" value="Register">
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
         </div>
      </div>
   </div>
</div>
<!-- End login signup popup -->
<!--   add post -->
<!-- Modal -->
<div id="addpro" class="modal fade add-products" role="dialog">
   <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?php echo lang('Add New Post / Products'); ?></h4>
         </div>
         <div class="modal-body">
            <h3><?php echo lang('Is it a Demand or a product?'); ?></h3>
            <a href="<?php echo base_url('product/add/1');?>"><label class="option001"><?php echo lang('Demand'); ?>
            <span class="checkmark"></span>
            </label></a>
            <a href="#" data-toggle="modal" data-target="#addpro1">
            <label class="option001"><?php echo lang('Products'); ?>
            <span class="checkmark"></span></a>
            </label>
            <p><?php echo lang('Select one option to continue'); ?></p>
         </div>
      </div>
   </div>
</div>
<!--   add  post -->
<!-- fixed pro -->
<div id="addpro1" class="modal fade add-products" role="dialog">
   <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?php echo lang('Add New Post / Products'); ?></h4>
         </div>
         <div class="modal-body">
            <h3><?php echo lang('Is it a fixed product or a bidding?'); ?></h3>
            <a href="<?php echo base_url('product/add/0/fixed');?>"><label class="option001"><?php echo lang('Fixed'); ?>
            <span class="checkmark"></span>
            </label></a>
            <a href="<?php echo base_url('product/add/0/bidding');?>"><label class="option001"><?php echo lang('Bidding'); ?>
            <span class="checkmark"></span>
            </label></a>
            <p><?php echo lang('Select one option to continue'); ?></p>
         </div>
      </div>
   </div>
</div>
<!-- fixed pro end-->
<!-- bidding check out -->
<div id="biddingcheck1" class="modal fade quickbid" role="dialog">
   <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Checkout</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12">
                  <h3 class="main-heading005">
                     Payment Details
                  </h3>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Name on Card</label>
                           <input type="text" class="form-control" stripe-data="name"
                              id="name-on-card" placeholder="Card Holder's Name">
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Card Number</label>
                           <input type="text" class="form-control" stripe-data="name"
                              id="name-on-card" placeholder="Debit/Credit card number">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Expiration Date</label>
                           <input type="text" class="form-control" stripe-data="name"
                              id="name-on-card" placeholder="mm/yyyy">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Card CVC</label>
                           <input type="text" class="form-control" stripe-data="cvc"
                              id="card-cvc" placeholder="Security Code">
                        </div>
                     </div>
                     <div class="col-md-12">
                        <img class=" payment-images" src="<?php echo base_url(); ?>assets/frontend/images/cc.png">
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <div class="save-card"><input type="checkbox" name="" id="savecard"> <label for="savecard"> Save Card Details</label> </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <button type="submit" class="btn btn-success btn-lg" style="width:auto;">Pay
                        Now
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



   <div id="chat-box">
      <div id="frame" class="chat-frame">
         <span value='Hide' id="hidePanel" class="hidechat"><i class="fa fa-times"></i></span>
         <div class="content mainchat ">
            <div class="contact-profile">
               <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
               <p>Harvey Specter</p>
            </div>
            <div class="messages">
               <ul>
                  <li class="sent">
                     <img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
                     <p>lorem ipsum es to dolor lorem ipsum es to dolorlorem ipsum es to dolor</p>
                  </li>
                  <li class="replies">
                     <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                     <p>lorem ipsum es to dolor</p>
                  </li>
                  <li class="replies">
                     <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                     <p>lorem ipsum es to dolor lorem ipsum es to dolor</p>
                  </li>
                  <li class="sent">
                     <img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
                     <p>lorem ipsum es to dolor</p>
                  </li>
                  <li class="replies">
                     <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                     <p>lorem ipsum</p>
                  </li>
               </ul>
            </div>
            <div class="message-input">
               <div class="wrap">
                  <input type="text" placeholder="Write your message..." />
                  <button class="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
               </div>
            </div>
         </div>
      </div>
   </div>
<!-- bidding check out-->