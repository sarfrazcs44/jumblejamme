<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/chat.css">

<div class="wrapper">

<section class="mobile-view">
<div class="container-fluid">
   <div class="row">
      <div class="col-md-12">
         <a href="demands.php"><img src="<?php echo base_url(); ?>assets/frontend/images/home3.jpg" class="img-responsive"></a>
      </div>
       <div class="col-md-12">
         <a href="product-Grid.php"><img src="<?php echo base_url(); ?>assets/frontend/images/home1.jpg" class="img-responsive"></a>
      </div>
       <div class="col-md-12">
         <a href="bidding.php"><img src="<?php echo base_url(); ?>assets/frontend/images/home2.jpg" class="img-responsive"></a>
      </div>
   </div>
</div>
   
</section>


   <section class="main-home">
      <div class="container-fluid">
         <div class="col-md-4 col-sm-4">
            <div class="slide-box003">
               <div class="row">
                  <div class="col-md-12">
                     <div class="slider-heading">
                        Demands
                     </div>
                  </div>
               </div>
               <div class="ads-box">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="topbar001">
                           <div class="col-md-2">
                              <div class="ad-img">
                                 <img src="<?php echo base_url(); ?>assets/frontend/images/p1.jpg">
                              </div>
                           </div>
                           <div class="col-md-10">
                              <h3 class="title01">
                              Seller name
                              </h3>
                           </div>
                        </div>
                        <div class="pro001">
                           <div class="col-md-4">
                              <img src="<?php echo base_url(); ?>assets/frontend/images/p1.jpg">
                           </div>
                           <div class="col-md-8">
                              <p class="desc01">
                                 lorem ipsum es to dolor porque lorem ipsum es to dolor lorem ipsum es to dolor
                              </p>
                              <div class="location001">
                                 <i class="fa fa-map-marker"></i> DHA, Lahore
                              </div>
                           </div>
                        </div>
                        <div class="sharing-icons">
                            <div class="row">
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-commenting-o"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-share-alt"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#" value='Show' id="showPanel"><i class="fa fa-comments-o"></i></a></div></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="ads-box">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="topbar001">
                           <div class="col-md-2">
                              <div class="ad-img">
                                 <img src="<?php echo base_url(); ?>assets/frontend/images/p1.jpg">
                              </div>
                           </div>
                           <div class="col-md-10">
                              <h3 class="title01">
                              Seller name
                              </h3>
                           </div>
                        </div>
                        <div class="pro001">
                           <div class="col-md-4">
                              <img src="<?php echo base_url(); ?>assets/frontend/images/p1.jpg">
                           </div>
                           <div class="col-md-8">
                              <p class="desc01">
                                 lorem ipsum es to dolor porque lorem ipsum es to dolor lorem ipsum es to dolor
                              </p>
                              <div class="location001">
                                 <i class="fa fa-map-marker"></i> DHA, Lahore
                              </div>
                           </div>
                        </div>
                         <div class="sharing-icons">
                            <div class="row">
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-commenting-o"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-share-alt"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#" value='Show' id="showPanel"><i class="fa fa-comments-o"></i></a></div></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="ads-box">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="topbar001">
                           <div class="col-md-2">
                              <div class="ad-img">
                                 <img src="<?php echo base_url(); ?>assets/frontend/images/p1.jpg">
                              </div>
                           </div>
                           <div class="col-md-10">
                              <h3 class="title01">
                              Seller name
                              </h3>
                           </div>
                        </div>
                        <div class="pro001">
                           <div class="col-md-4">
                              <img src="<?php echo base_url(); ?>assets/frontend/images/p1.jpg">
                           </div>
                           <div class="col-md-8">
                              <p class="desc01">
                                 lorem ipsum es to dolor porque lorem ipsum es to dolor lorem ipsum es to dolor
                              </p>
                              <div class="location001">
                                 <i class="fa fa-map-marker"></i> DHA, Lahore
                              </div>
                           </div>
                        </div>
                         <div class="sharing-icons">
                            <div class="row">
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-commenting-o"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-share-alt"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#" value='Show' id="showPanel"><i class="fa fa-comments-o"></i></a></div></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4">
            <div class="slide-box001">
               <div class="row">
                  <div class="col-md-12">
                     <div class="slider-heading">
                        Latest Products
                     </div>
                  </div>
                  <?php if(!empty($fixed_products)){
                              foreach ($fixed_products as  $value) { ?>
                                 <div class="col-md-6 col-sm-12">
                                    <figure class="card card-product">
                                       <div class="img-wrap"><img src="<?php echo base_url(get_images($value->ProductID,'ProductImage',FALSE)); ?>"> </div>
                                       <h4 class="title"><?php echo lang($value->Title);?></h4>
                                       <div class="bottom-wrap">
                                          <div class="price-wrap h5">
                                             <?php if($value->DiscountedPrice > 0 ){ ?>
                                                <span class="price-new">$<?php echo $value->DiscountedPrice; ?></span> <del class="price-old">$<?php echo $value->ProductPrice; ?></del>

                                            <?php }else{ ?>
                                             <span class="price-new">$<?php echo $value->ProductPrice; ?></span>

                                            <?php } ?>
                                             
                                          </div>
                                          <a href="<?php echo base_url('products/detail/'.$value->ProductID);?>" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                                          <!-- price-wrap.// -->
                                       </div>
                                       <!-- bottom-wrap.// -->
                                    </figure>
                                 </div>
                              

                              <?php
                              }
                           }
                           ?>
                  
                 
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4">
            <div class="slide-box002">
               <div class="row">
                  <div class="col-md-12">
                     <div class="slider-heading">
                        Bidding
                     </div>
                  </div>
                  <?php if(!empty($bidding_products)){
                              foreach ($bidding_products as  $b_product) { ?>
                  <div class="col-md-6 col-sm-12">
                     <figure class="card card-product">
                        <div class="img-wrap"><img src="<?php echo base_url(get_images($b_product->ProductID,'ProductImage',FALSE)); ?>"> </div>
                        <h4 class="title"><?php echo lang($b_product->Title);?></h4>
                        <div class="bottom-wrap">
                           <div class="price-wrap h5">
                              <span class="price-new">$<?php echo $b_product->ProductPrice; ?></span>
                           </div>
                           <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                           <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                     </figure>
                  </div>
                   <?php
                              }
                           }
                           ?>
                  
               </div>
            </div>
         </div>
      </div>
</div>
</section>
</div>