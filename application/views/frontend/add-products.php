<div class="wrapper">
   <div class="container">
      <section class="">
         <div class="col-md-12 col-sm-12">
            <div class="col-md-6 col-sm-12">
               <h2 class="create-account"><?php echo lang('Add New Product'); ?></h2>
               <form action="<?php echo base_url('product/action');?>" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">
                            <input type="hidden" name="IsDemand" value="<?php echo $IsDemand;?>">
                            <input type="hidden" name="IsFixedPrice" value="<?php echo $IsFixedPrice;?>">
                  <div class="row">
                     <div class="col-md-12 col-sm-6">
                        <div class="form-group">    
                           <label><?php echo lang('Title'); ?></label>     
                           <input type="text" name="Title" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                           <label><?php echo lang('Select Category'); ?></label>     
                           <select class="form1" name="CategoryID" id="CategoryID">
                              <?php echo getCategoryTreeForProduct(0, '','EN');?>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                           <label><?php echo lang('Price'); ?></label>     
                           <input type="text" name="ProductPrice" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                           <label><?php echo lang('Discount Price (optional)'); ?></label>     
                           <input type="text" name="DiscountedPrice" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                           <label><?php echo lang('Stock (Quantity)'); ?></label>     
                           <input type="text" name="Quantity" required="" class="form1">
                        </div>
                     </div>
                      <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                           <label><?php echo lang('Add Shipping Price'); ?></label>     
                           <input type="text" name="DeliveryCharges" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                            
                            <label class="top001"><input type="checkbox" name="IsDeliveryChargesInclude" value="1" required=""> <?php echo lang('Included Shipping Price'); ?></label>   
                        </div>
                     </div>
                     <div class="col-md-12 col-sm-12">
                        <div class="multi-field-wrapper">
                           <div class="multi-fields">
                              <div class="row multi-field">
                                 <div class="col-md-3 col-sm-3">
                                    <label> <?php echo lang('Add Variation'); ?></label>
                                    <input type="text" name="MinRang[]" class="form1" placeholder="eg:1 to 10 (Min val)">
                                 </div>
                                 <div class="col-md-3 col-sm-3">
                                    <label> <?php echo lang('Add Variation'); ?></label>
                                    <input type="text" name="MaxRang[]" class="form1" placeholder="eg:1 to 10 (Max val)">
                                 </div>
                                 <div class="col-md-4 col-sm-4">
                                    <label> <?php echo lang('Add Price'); ?></label>
                                    <input type="text" name="Price[]" class="form1" placeholder="eg:$100 ">
                                 </div>
                                 <div class="col-md-2 col-sm-2 remove-field">
                                    <button type="button" class="btn btn-danger">X</button>
                                 </div>
                              </div>
                           </div>
                           <button type="button" class="add-field  btn btn-success"><?php echo lang('Add more'); ?></button>
                        </div>
                     </div>
                     <div class="col-md-12 col-sm-12">
                        <label><?php echo lang('Description'); ?></label>     
                        <textarea class="form1 pro-desc" name="Description"></textarea>			
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                           <input type="submit" name="" required="" value="Update" class="regis-btn">
                        </div>
                     </div>
                  </div>
            </div>
            <div class="col-md-6 col-sm-12">
            <div class="field" align="left">
            <h3><?php echo lang('Upload your Product images'); ?></h3>
            <label class="upload-pro" for="files"><?php echo lang('Click here to upload images'); ?><br>(<?php echo lang('Max no of image is 10'); ?>)</label>
            <input type="file" id="files" name="Image[]" multiple/>
            </div>
            </form>
            <div class="row" id="image_preview"></div>
            </div>
         </div>
   </div>
   </section>
   <!-- <script type="text/javascript">
      function preview_images() 
      {
       var total_file=document.getElementById("images").files.length;
       for(var i=0;i<total_file;i++)
       {
        $('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
       }
      }
      
      </script> -->
</div>
<script type="text/javascript"></script>