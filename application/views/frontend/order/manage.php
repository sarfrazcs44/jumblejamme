<div class="main-wrapper">
   <div class="container">
      <div class="col-md-12">
         <h3 class="main-heading005">
            Orders <?php echo $this->uri->segment(3) != '' ? '- ' . ucwords(str_replace('_', ' ', ucwords($this->uri->segment(3)))) : ''; ?>
         </h3>
      </div>
      <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Customer Name</th>
                                    <th>Dealer Name</th>
                                    <th>Order Tracking #</th>
                                    <?php if (isset($all)) { ?>
                                        <th>Order Status</th>
                                    <?php } ?>
                                    <th>Order Grand Total</th>
                                    <th>Order Received At</th>
                                    <?php if (checkRightAccess(46, $this->session->userdata['user']['RoleID'], 'CanEdit') || checkRightAccess(46, $this->session->userdata['user']['RoleID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($results) {
                                    $i = 1;
                                    foreach ($results as $value) { ?>
                                        <tr id="<?php echo $value['OrderRequestID']; ?>">
                                            <td><?php echo $i; ?></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['UserID']); ?>"
                                                   target="_blank"><?php echo $value['FullName']; ?></a></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                                   target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                            <td><?php echo $value['OrderTrackID']; ?></td>
                                            <?php if (isset($all)) {
                                                $status = $value['OrderStatusID'];
                                                if ($status == 1) {
                                                    $class = "btn btn-sm";
                                                } else if ($status == 2) {
                                                    $class = "btn btn-warning btn-sm";
                                                } else if ($status == 3 || $status == 7) {
                                                    $class = "btn btn-info btn-sm";
                                                } else if ($status == 4) {
                                                    $class = "btn btn-success btn-sm";
                                                } else if ($status == 5 || $status == 6 || $status == 8) {
                                                    $class = "btn btn-danger btn-sm";
                                                }
                                                ?>
                                                <td>
                                                    <button class="<?php echo $class; ?>"><?php echo $value['OrderStatus']; ?>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                </td>
                                            <?php } ?>
                                            <td><?php echo $value['GrandTotal']; ?> <?php echo $value['Currency']; ?></td>
                                            <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                            <?php if (checkRightAccess(46, $this->session->userdata['user']['RoleID'], 'CanEdit') || checkRightAccess(46, $this->session->userdata['user']['RoleID'], 'CanDelete')) { ?>
                                                <td>
                                                    <?php if (checkRightAccess(46, $this->session->userdata['user']['RoleID'], 'CanEdit')) { ?>
                                                        
                                                    <?php } ?>

                                                    <?php if (checkRightAccess(46, $this->session->userdata['user']['RoleID'], 'CanEdit')) { ?>
                                                        <a href="<?php echo base_url('customer/' . $ControllerName . '/view/' . $value['OrderRequestID']); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons"
                                                                    title="Click to view details">View</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>

                                                    
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                        $i++;
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
   </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<!--<script>
    $(document).ready(function () {
        $.ajax({
            url: base_url + 'cms/booking/markAsRead',
            type: 'GET',
            success: function (data) {
                console.log(data);
            }

        });
    });
</script>-->