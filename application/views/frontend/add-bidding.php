<div class="wrapper">
   <div class="container">
      <section class="">
         <div class="col-md-12">
            <div class="col-md-6">
               <h2 class="create-account">Add New Bid</h2>
               <form action="<?php echo base_url('product/action');?>" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">
                            <input type="hidden" name="IsDemand" value="<?php echo $IsDemand;?>">
                            <input type="hidden" name="IsFixedPrice" value="<?php echo $IsFixedPrice;?>">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">    
                           <label><?php echo lang('Title'); ?></label>     
                           <input type="text" name="Title" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label><?php echo lang('Select Category'); ?></label>     
                           <select class="form1" name="CategoryID" id="CategoryID">
                              <?php echo getCategoryTreeForProduct(0, '','EN');?>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label><?php echo lang('Quantity'); ?></label>     
                           <input type="text" name="Quantity" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">    
                           <label><?php echo lang('Start Bid Price'); ?></label>     
                           <input type="text" name="ProductPrice" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">    
                           <label><?php echo lang('End Bid Day'); ?></label>     
                           <input type="date" name="EndBidDate" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">    
                           <label><?php echo lang('End Bid Time'); ?></label>     
                           <input type="time" name="EndBidTime" required="" class="form1">
                        </div>
                     </div>
                       <div class="col-md-6 ">
                        <div class="form-group">    
                           <label><?php echo lang('Add Shipping Price'); ?></label>     
                           <input type="text" name="DeliveryCharges" required="" class="form1">
                        </div>
                     </div>
                  
                     <div class="col-md-6 ">
                        <div class="form-group">    
                            
<label class="top001"><input type="checkbox" name="IsDeliveryChargesInclude" required=""> <?php echo lang('Included Shipping Price'); ?></label>                           </div>
                     </div>
                     <div class="col-md-12">
                        <label><?php echo lang('Description'); ?></label>     
                        <textarea class="form1 pro-desc" name="Description"></textarea>			
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <input type="submit" name="" required="" value="Update" class="regis-btn">
                        </div>
                     </div>
                  </div>
            </div>
            <div class="col-md-6">
            <div class="field" align="left">
            <h3><?php echo lang('Upload your Product images'); ?></h3>
            <label class="upload-pro" for="files"><?php echo lang('Click here to upload images'); ?><br>(<?php echo lang('Max no of image is 10'); ?>)</label>
            <input type="file" id="files" name="files[]" multiple />
            </div>
            </form>
            <div class="row" id="image_preview"></div>
            </div>
         </div>
   </div>
   </section>
   <!-- <script type="text/javascript">
      function preview_images() 
      {
       var total_file=document.getElementById("images").files.length;
       for(var i=0;i<total_file;i++)
       {
        $('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
       }
      }
      
      </script> -->
</div>
<script type="text/javascript"></script>