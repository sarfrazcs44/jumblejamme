<?php include 'header.php'; ?>
<div class="main-wrapper">
   <div class="container">
      <div class="row">
         <div class="preview01 col-md-6">
            <div id="carousel" class="carousel slide" data-ride="carousel">
               <div class="carousel-inner">
                  <div class="item active" data-thumb="0">
                     <img src="images/p3.jpg">        
                  </div>
                  <div class="item" data-thumb="0">
                     <img src="images/p4.jpg">
                  </div>
                  <div class="item" data-thumb="0">
                     <img src="images/p5.jpg">
                  </div>
                  <div class="item" data-thumb="0">
                     <img src="images/p3.jpg">
                  </div>
                  <div class="item" data-thumb="1">
                     <img src="images/p4.jpg">
                  </div>
                  <div class="item" data-thumb="1">
                     <img src="images/p5.jpg">
                  </div>
                  <div class="item" data-thumb="1">
                     <img src="images/p3.jpg">
                  </div>
               </div>
            </div>
            <div class="clearfix">
               <div id="thumbcarousel" class="carousel slide" data-interval="false">
                  <div class="carousel-inner">
                     <div class="item active">
                        <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="images/p3.jpg"></div>
                        <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="images/p4.jpg"></div>
                        <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="images/p5.jpg"></div>
                        <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="images/p3.jpg"></div>
                     </div>
                     <!-- /item -->
                     <div class="item">
                        <div data-target="#carousel" data-slide-to="4" class="thumb"><img src="images/p3.jpg"></div>
                        <div data-target="#carousel" data-slide-to="5" class="thumb"><img src="images/p4.jpg"></div>
                        <div data-target="#carousel" data-slide-to="6" class="thumb"><img src="images/p5.jpg"></div>
                        <div data-target="#carousel" data-slide-to="7" class="thumb"><img src="images/p3.jpg"></div>
                     </div>
                     <!-- /item -->
                  </div>
                  <!-- /carousel-inner -->
                  <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  </a>
                  <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  </a>
               </div>
               <!-- /thumbcarousel -->
            </div>
            <!-- /clearfix -->
         </div>
         <div class="details col-md-6">
            <h3 class="product-title01">Product Title Here</h3>
            <p class="product-description01">Suspendisse quos? Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere.</p>
            <b>Time Left</b> 
            <p id="counter" class="counter-clr single-page-counter"></p>
            <h4 class="bid-price"> Current Bid: <span>$180</span> <span>(USD)</span> <span class="saving">(a 30% Saving)</span></h4>
            <h4 class="bid-price"> Minimum Bid: <span>$200</span></h4>
            <div class="input-group bid001">
               <input type="text" class="form-control" name="x" placeholder="Enter Your Minimum Bid">
               <span class="input-group-btn">
               <button class="btn btn-default bid-btn" type="button" data-toggle="modal" data-target="#biddingcheck1">Place bid</button>
               </span>
            </div>
            <div class="place-bid"><b>Total Placed bids:</b> <span>44</span></div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="desc005">
               <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#home">Product Description</a></li>
                  <li><a data-toggle="tab" href="#menu1">Specification</a></li>
               </ul>
               <div class="tab-content">
                  <div id="home" class="tab-pane fade in active">
                     <h3>ADVANTAGES</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                     <h3>HOW TO USE?</h3>
                     <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                  </div>
                  <div id="menu1" class="tab-pane fade">
                     <h3>Specification</h3>
                     <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
           //-- Click on detail
       
   
           //-- Click on QUANTITY
           $(".btn-minus").on("click",function(){
               var now = $(".quantity > div > input").val();
               if ($.isNumeric(now)){
                   if (parseInt(now) -1 > 0){ now--;}
                   $(".quantity > div > input").val(now);
               }else{
                   $(".quantity > div > input").val("1");
               }
           })            
           $(".btn-plus").on("click",function(){
               var now = $(".quantity > div > input").val();
               if ($.isNumeric(now)){
                   $(".quantity > div > input").val(parseInt(now)+1);
               }else{
                   $(".quantity > div > input").val("1");
               }
           })                        
       }) 
</script>
<script>
   // Set the date we're counting down to
   var countDownDate = new Date("Nov 11, 2019 12:37:25").getTime();
   
   // Update the count down every 1 second
   var x = setInterval(function() {
   
     // Get today's date and time
     var now = new Date().getTime();
       
     // Find the distance between now and the count down date
     var distance = countDownDate - now;
       
     // Time calculations for days, hours, minutes and seconds
     var days = Math.floor(distance / (1000 * 60 * 60 * 24));
     var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
     var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
     var seconds = Math.floor((distance % (1000 * 60)) / 1000);
     var milliseconds = Math.floor(((distance % (16.5 * 60))));
       
     // Output the result in an element with id="demo"
     document.getElementById("counter").innerHTML = hours + "h "
     + minutes + "m " + seconds + "s ";
       
     // If the count down is over, write some text 
     if (distance < 0) {
       clearInterval(x);
       document.getElementById("counter").innerHTML = "EXPIRED";
     }
   }, 1);
</script>
<?php include 'footer.php'; ?>