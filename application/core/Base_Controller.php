<?php

class Base_Controller extends CI_Controller {

    protected $language;

    public function __construct() {

        parent::__construct();
        $this->load->helper('cookie');
        if ($this->session->userdata('lang')) {
            $this->language = $this->session->userdata('lang');
            // $this->language = $this->session->userdata('languageID');
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language = $result->ShortCode;
            } else {
                $this->language = 'EN';
            }
        }

        if ($this->session->userdata('currency_code') && $this->session->userdata('currency_rate')) {

            $this->currency_code = $this->session->userdata('currency_code');
            $this->currency_rate = $this->session->userdata('currency_rate');

        } else {

            $currency = getDefaultCurrency();
            if ($currency) {
                $this->currency_code = $currency->CurrencyCode;
                $this->currency_rate = $this->getCurrency($currency->CurrencyCode);
            } else {
                $this->currency_code = 'USD';
                $this->currency_rate = 1;
            }
        }

        if ($this->session->userdata('lang_code')) {
            $this->language_code = $this->session->userdata('lang_code');
            // $this->language = $this->session->userdata('languageID');
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language_code = $result->ShortCode;
            } else {
                $this->language_code = 'EN';
            }
        }
        //echo $this->currency_rate;exit;
        $this->data['site_setting'] = $this->getSiteSetting();
        $this->data['url_string'] = $this->uri->segment(1);
        $this->data['getCurrencies'] = $this->getAllCurrencies();
        $this->data['currency_code'] = $this->currency_code;
        $this->data['currency_rate'] = $this->currency_rate;
        $this->data['getSystemLanguages'] = getSystemLanguages();
        $this->data['language_code'] = $this->language_code;
        //print_rm($this->data['getSystemLanguages']);
        //$this->changeCurrency();
        //echo 'hre';exit;
    }

    public function changeLanguage($language) {
        $this->load->Model('Model_system_language');
        $fetch_by['GlobalCode'] = $language;
        $result = $this->Model_system_language->getWithMultipleFields($fetch_by);
        $languageID = $result->SysLID;
        if (!$result) {

            $default_lang = getDefaultLanguage();
            $language = $default_lang->ShortCode;
            $languageID = $default_lang->SystemLanguageID;
        }
        $this->session->set_userdata('lang', $language);
        //$this->session->set_userdata('languageID',$languageID);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function switchLanguage() {

        $LanguageCode = $this->input->post('LanguageCode');
        
        $this->session->set_userdata('lang_code', $LanguageCode);

        $success['error'] = false;

        echo json_encode($success);
        exit;
    }


    public function changeCurrency()
    {
        $req_url = 'https://api.exchangerate-api.com/v4/latest/USD';
        $response_json = file_get_contents($req_url);

        $CurrencyCode = $this->input->post('CurrencyCode');

        // Continuing if we got a result
        if(false !== $response_json) {

          // Try/catch for json_decode operation
            try 
            {
                // Decoding
                $response_object = json_decode($response_json);
                
                $this->session->set_userdata('currency_code', $CurrencyCode);
                $this->session->set_userdata('currency_rate', round($response_object->rates->$CurrencyCode, 2));

                $success['error'] = false;

                echo json_encode($success);
                exit;

            }
            catch(Exception $e) 
            {
                // Handle JSON parse error...
            }

        }
    }


    public function getCurrency($CurrencyCode = 'USD')
    {
        $req_url = 'https://api.exchangerate-api.com/v4/latest/USD';
        $response_json = file_get_contents($req_url);

        // Continuing if we got a result
        if(false !== $response_json) {

          // Try/catch for json_decode operation
            try 
            {
                // Decoding
                $response_object = json_decode($response_json);
                
                return round($response_object->rates->$CurrencyCode, 2);

            }
            catch(Exception $e) 
            {
                // Handle JSON parse error...
            }

        }
    }

    public function getSiteSetting() {

        $this->load->model('Site_setting_model');
        return $this->Site_setting_model->get(1, false, 'SiteSettingID');
    }

    public function getAllCurrencies() {

        $this->load->model('Currency_model');
        return $this->Currency_model->getAllJoinedData(false, 'CurrencyID', $this->language, array('currencies.IsActive'=>1));
    }

    public function uploadImage($file_key, $path, $id = false, $type = false, $multiple = false) {
        $data = array();
        $extension = array("jpeg", "jpg", "png", "gif");
        foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
            $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
            $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (in_array($ext, $extension)) {

                move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);
                if (!$multiple) {
                    return $path . $file_name;
                } else {
                    $this->load->model('Site_images_model');
                    $data['FileID'] = $id;
                    $data['ImageType'] = $type;
                    $data['ImageName'] = $path . $file_name;
                    $this->Site_images_model->save($data);
                }
                /* $data['DestinationID'] = $id; 
                  $data['ImagePath'] = $path.$file_name;
                  $this->Site_images_model->save($data); */
            }
        }
        return true;
    }
    
    public function DeleteImage() {
        $deleted_by = array();
        $ImagePath = $this->input->post('image_path');
        $deleted_by['SiteImageID'] = $this->input->post('image_id');
        if (file_exists($ImagePath)) {
            unlink($ImagePath);
        }
        $this->Site_images_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


    public function createLinks($total_page,$page = 0,$url = ''){

        $post_data = $this->input->get();

        $search_url = '';
        if(!empty($post_data)){
            $search_url .= '?';
            foreach ($post_data as $key => $value) {
                if($value != ''){
                    $search_url .= $key.'='.$value.'&';
                }
                
            }
        }

        $links = '';
        if($page > 0){
             $links .= '<li class="page-item"><a class="page-link" href="'.base_url($url).($page - 1).'.'.$search_url.'">Previous</a></li>';
        }
       
        for($i=0;$i<$total_page;$i++){
            $links .= '<li class="page-item '.($i == $page ? 'active' : '').'"><a class="page-link" href="'.base_url($url).$i.''.$search_url.'">'.($i + 1).'</a></li>';
        }

        if($total_page != $page){
            $links .= '<li class="page-item"><a class="page-link" href="'.base_url($url).($page + 1).''.$search_url.'">Next</a></li>';
        }

        return $links;

    }
    
}
