<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Product_model');
        $this->load->model('User_text_model');
        $this->load->model('State_model');
        $this->load->model('City_model');
    }

    

    public function index()
    {

        $this->data['view'] = 'frontend/index';
        $this->data['fixed_products'] = $this->Product_model->getProducts('products.IsActive = 1 AND products.IsFixedPrice = 1 AND products.IsDemand = 0','EN',4,0,'products.ProductID','DESC');
        $this->data['bidding_products'] = $this->Product_model->getProducts('products.IsActive = 1 AND products.IsFixedPrice = 0 AND products.IsDemand = 0','EN',4,0,'products.ProductID','DESC');
        $this->data['demands'] = $this->Product_model->getProducts('products.IsActive = 1 AND products.IsDemand = 1','EN',4,0,'products.ProductID','DESC');
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function verifyEmail()
    {
        if (isset($_GET['verification_token']) && $_GET['verification_token'] != '') {
            $UserID = base64_decode($_GET['verification_token']);
            $user_info = $this->User_model->getJoinedData(true, 'UserID', 'users.UserID =' . $UserID);
            if ($user_info) {
                $this->User_model->update(array('IsEmailVerified' => 1), array('UserID' => $UserID));
                echo "Your email is verified successfully. You can get back to the app to continue using it.";
                exit();
            } else {
                echo "Sorry! We couldn't verify you as your verification process failed.";
                exit();
            }
        } else {
            echo "Sorry! We couldn't verify you as your verification process failed.";
            exit();
        }
    }

    public function forgotPassword()
    {
        if (isset($_GET['verification_token']) && $_GET['verification_token'] != '') {
            $UserID = base64_decode($_GET['verification_token']);
            $user_info = $this->User_model->getJoinedData(true, 'UserID', 'users.UserID =' . $UserID);
            if ($user_info) {
                $data['UserID'] = $UserID;
                $this->load->view('forgot_password', $data);
            } else {
                echo "Sorry! We couldn't verify you as your verification process failed.";
                exit();
            }
        } else {
            echo "Sorry! We couldn't verify you as your verification process failed.";
            exit();
        }
    }

    public function changePassword()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $password = str_replace(' ', '', $post_data['new_password']);
            $password_length = strlen($password);
            if ($post_data['new_password'] !== $post_data['confirm_password']) {
                $response['status'] = false;
                $response['message'] = "Password and confirm password doesn't match!";
                echo json_encode($response);
                exit();
            } elseif ($password_length < 6) {
                $response['status'] = false;
                $response['message'] = "Password field must have atleast 6 characters!";
                echo json_encode($response);
                exit();
            } elseif ($post_data['new_password'] == $post_data['confirm_password'] && $password_length >= 6) {
                $UserID = base64_decode($post_data['UserID']);
                $user = $this->User_model->get($UserID, true, 'UserID');
                if ($user) {
                    $update['Password'] = md5($post_data['new_password']);
                    $update['OnlineStatus'] = 'Offline';
                    $update_by['UserID'] = $UserID;
                    $this->User_model->update($update, $update_by);
                    $response['status'] = true;
                    $response['message'] = "Your password is updated successfully. You can use this password for login.";
                    echo json_encode($response);
                    exit();
                } else {
                    $response['status'] = false;
                    $response['message'] = "Something went wrong. Please try again later.";
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response['status'] = false;
                $response['message'] = "Something went wrong. Please try again later.";
                echo json_encode($response);
                exit();
            }
        } else {
            $response['status'] = false;
            $response['message'] = "Something went wrong. Please try again later.";
            echo json_encode($response);
            exit();
        }
    }

    public function test()
    {
        $where = "users.UserID = 2";
        $user_info = $this->User_model->getUserInfo($where);
        $this->sendVerificationForEmail($user_info);
    }

    private function sendVerificationForEmail($user_info)
    {
        if ($user_info['Email'] !== '') {
            $verify_link = base_url() . 'index/verifyEmail?verification_token=' . base64_encode($user_info['UserID']) . '&lang=' . $this->language;
            $email_template = get_email_template(7, $this->language);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $user_info['FullName'], $message);
            $message = str_replace("{{link}}", $verify_link, $message);
            $data['to'] = $user_info['Email'];
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            $email_sent = sendEmail($data);
            if ($email_sent) {
                echo "Email sent";
            } else {
                echo "Email not sent";
            }
        }
    }

    public function getCountryStates()
    {
        $CountryID = $this->input->post('CountryID');

        // To make selected state if user has selected state used in edit forms
        $selectedStateID = $this->input->post('SelectedStateID');

        $parent                             = 'State_model';
        
        $response = array();
        $options = '<option value="">'.lang("choose_state").'</option>';

        if(is_array($CountryID))
        {
            
            foreach ($CountryID as $key => $country) {
                $getCountryDetail = $this->Country_model->getJoinedData(false,'CountryID','countries.CountryID ='.$country,'DESC','');
                $options .= '<optgroup label="'.$getCountryDetail[0]->Title.'">';
                $States = $this->$parent->getAllJoinedData(false,'StateID',$this->language,'states.IsActive = 1 AND states.CountryID = '.$country,'ASC','states_text.Title',true);

                foreach ($States as $key => $value) {
                    if(!empty($selectedStateID)) {
                        $selected = ($selectedStateID == $value->StateID) ? "selected": "";
                        $options .= '<option value="'.$value->StateID.'" '.$selected.'>'.$value->Title.'</option>';
                    } else {
                        $options .= '<option value="'.$country.'-'.$value->StateID.'">'.$value->Title.'</option>';
                    }
                }
                $options .= '</optgroup>';
            }
            $response['array'] = true;
        }
        else
        {

            $States = $this->$parent->getAllJoinedData(false,'StateID',$this->language,'states.IsActive = 1 AND states.CountryID = '.$CountryID,'ASC','states_text.Title',true);

            foreach ($States as $key => $value) {
                if(!empty($selectedStateID)) {
                    $selected = ($selectedStateID == $value->StateID) ? "selected": "";
                    $options .= '<option value="'.$value->StateID.'" '.$selected.'>'.$value->Title.'</option>';
                }else {
                    $options .= '<option value="'.$value->StateID.'">'.$value->Title.'</option>';
                }

            }
            $response['array'] = false;
        }

        $response['success'] = true;
        $response['html'] = $options;

        echo json_encode($response);
        exit();
    }
    

     public function getStateCities()
    {
        $StateID = $this->input->post('StateID');

        // To make selected city if user has selected city used in edit forms
        $SelectedCityID = $this->input->post('SelectedCityID');

        $parent                             = 'City_model';

        $response = array();
        $options = '<option value="">'.lang("choose_city").'</option>';
        
        if(is_array($StateID))
        {
            
            foreach ($StateID as $key => $state) {
                $statearr = explode('-', $state);

                $getStateDetail = $this->State_model->getJoinedData(false,'StateID','states.StateID ='.$statearr[1],'DESC','');

                $options .= '<optgroup label="'.$getStateDetail[0]->Title.'">';
                $Cities = $this->$parent->getAllJoinedData(false,'CityID',$this->language,'cities.IsActive = 1 AND cities.StateID = '.$statearr[1],'ASC','cities_text.Title',true);

                foreach ($Cities as $key => $value) {
                    if(!empty($SelectedCityID)) {
                        $selected = ($SelectedCityID == $value->CityID) ? "selected" : "";
                        $options .= '<option value="'.$value->CityID.'" '.$selected.' >'.$value->Title.'</option>';
                    }else {
                        $options .= '<option value="' . $state . '-' . $value->CityID . '">' . $value->Title . '</option>';
                    }
                }
                $options .= '</optgroup>';
            }
            $response['array'] = true;
        }
        else
        {
            $Cities = $this->$parent->getAllJoinedData(false,'CityID',$this->language,'cities.IsActive = 1 AND cities.StateID = '.$StateID,'ASC','cities_text.Title',true);

            foreach ($Cities as $key => $value) {
                if(!empty($SelectedCityID)) {
                    $selected = ($SelectedCityID == $value->CityID) ? "selected" : "";
                    $options .= '<option value="'.$value->CityID.'" '.$selected.' >'.$value->Title.'</option>';
                }else {
                    $options .= '<option value="'.$value->CityID.'">'.$value->Title.'</option>';
                }

            }
            $response['array'] = false;
        }

        $response['success'] = true;
        $response['html'] = $options;

        echo json_encode($response);
        exit();
    }
}