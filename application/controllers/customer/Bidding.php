<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidding extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        checkUserSession();
        $this->load->model('Product_model');
        $this->load->model('Category_model');
    }
    public function index()
    {
        $this->data['view'] = 'frontend/users_bids';
        $this->load->view('frontend/layouts/default', $this->data);
    }
}