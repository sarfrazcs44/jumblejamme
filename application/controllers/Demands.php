<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demands extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('User_text_model');
    }
    public function index()
    {
        $this->data['view'] = 'frontend/demands';


        $this->load->view('frontend/layouts/default', $this->data);
    }
}