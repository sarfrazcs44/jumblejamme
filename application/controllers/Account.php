<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Product_model');
        $this->load->model('User_text_model');
        $this->load->model('Module_model');
        $this->load->model('Country_model');
        $this->load->model('Modules_users_rights_model');
        $this->load->model('Temp_order_model');
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['site_setting'] = $this->getSiteSetting();
    }



    public function registration()
    {
        //$ip = $this->getModifiedIP();
        //$location_data = $this->getLocationByIp($ip);
        //$this->data['country'] = strtolower(( $location_data['CountryTitle'] ? $location_data['CountryTitle'] : '' ));
        $this->data['countries'] = $this->Country_model->getAllJoinedData(false,'CountryID',$this->language,'countries.IsActive = 1','ASC','countries_text.Title',true);
        $this->data['view'] = 'frontend/signup';
		$this->load->view('frontend/layouts/default',$this->data);
    }

    public function login()
    {
       
        $this->data['view'] = 'frontend/'.$this->data['ControllerName'].'/login';
        $this->load->view('frontend/layouts/default',$this->data);
    }

    public function forgotPassword()
    {
        $this->data['view'] = 'frontend/'.$this->data['ControllerName'].'/forgot_password';
        $this->load->view('frontend/layouts/default',$this->data);
    }


     public function checkLogin()
    {
        $data = array();
        $post_data = $this->input->post();
        $this->loginValidation();
        $checkUser = $this->checkUser($post_data);
        if ($checkUser != true) {
            $data = array();
            $data['success'] = 'false';
            $data['error'] = 'Email or password incorrect.';
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data['success'] = 'Login Successfully';
            $data['error'] = false;
            $data['reload'] = true;
            //$data['url'] = 'cms/dashboard';
            echo json_encode($data);
            exit();
        }
    }

    private function loginValidation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Email', 'Email', 'required');
        $this->form_validation->set_rules('Password', 'Password', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    private function checkUser($post_data)
    {
        $post_data['Password'] = md5($post_data['Password']);
        $user = $this->User_model->getUserData($post_data, $this->language);

        if (!empty($user)) {

            /*if($user->RoleID != '1')
            {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = 'You can\'t be login from here';
                echo json_encode($data);
                exit();

            }
            $user = (array)$user;*/
            
            $this->session->set_userdata('user', $user);
            if (get_cookie('temp_order_key')) {
               // echo 'here';exit;
                $temp_user_id = get_cookie('temp_order_key');
                $update = array();
                $update['UserID'] = $this->session->userdata['user']['UserID'];
                $update_by['UserID'] = $temp_user_id;
                $this->Temp_order_model->update($update, $update_by);
                delete_cookie('temp_order_key');

            }
            //$this->updateUserLoginStatus();
            return true;
        } else {
            return false;
        }

    }


    public function resetPassword()
    {
        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/reset_password';
        $this->load->view('front/layouts/default',$this->data);
    }
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->registrationValidation();
                $this->save();
            break; 
            case 'forgot_password':
            $this->SendCodeForPasswordChange();
            break;
            case 'reset_password':
            $this->SavePasswordChange();
            break;
            case 'update':
            //$this->paymentValidation();
           // $this->updatePaymentInfo();
            break;
           
                 
        }
    }

    private function SendCodeForPasswordChange(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email');
        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else{

            $post_data = $this->input->post();

            $user_data = $this->User_model->getJoinedData(true,'UserID','users.Email = "'.$post_data['Email'].'"');
            if($user_data){
                $user_data = $user_data[0];
                $code =RandomString().$user_data['UserID'];

                $this->User_model->update(array('ForgotPasswordString' => $code),array('UserID' => $user_data['UserID']));
                $message = 'Hi '.$user_data['FullName'].'<br>';
                $message .= 'Your forgot password code is '.$code;
                $message = emailTemplate($message);

                $email_data = array();
                $email_data['to'] = $user_data['Email'];
                $email_data['subject'] = 'Forgot Password';
                $email_data['body'] = $message;
                 

                if(sendEmail($email_data)){
                    $success['error'] = false;
                    $success['success'] = 'Forgot Password code is sent to your email and mobile number. Use that code to reset your password';
                    $success['redirect'] = true;
                    $success['url'] = 'account/resetPassword';
                    echo json_encode($success);
                    exit;


                }else{
                    $errors['error'] = 'Something went wrong please try later';
                    $errors['success'] = 'false';
                    echo json_encode($errors);
                    exit;

                }
                


            }else{
                $errors['error'] = 'Email does not exist';
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;

            }

        }



    }


    private function SavePasswordChange(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('Code', 'Code', 'required');
        
        $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[ConfirmPassword]');
        $this->form_validation->set_rules('ConfirmPassword', lang('confirm_password'), 'required');
        

        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {
            $post_data = $this->input->post();

            $user_data = $this->User_model->get($post_data['Code'],true,'ForgotPasswordString');
            if($user_data){
                
               

                $this->User_model->update(array('ForgotPasswordString' => '','Password' => md5($post_data['Password'])),array('UserID' => $user_data['UserID']));
                


                $success['error'] = false;
                $success['success'] = 'Password updated successfully';
                $success['redirect'] = true;
                $success['url'] = 'account/login';
                echo json_encode($success);
                exit;
                


            }else{
                $errors['error'] = 'Code does not exist';
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;

            }
               
        }
    }

   
    
    private function registrationValidation()
    {

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        // $this->form_validation->set_rules('RoleID', lang('role'), 'required');
        // $this->form_validation->set_rules('Type', 'Registration Type', 'required');
        $this->form_validation->set_rules('FirstName', lang('first_name'), 'required');
        $this->form_validation->set_rules('LastName', lang('last_name'), 'required');
        $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email|is_unique[users.Email]');
        
        $this->form_validation->set_rules('Mobile', lang('phone_no'), 'required');
        $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[ConfirmPassword]');
        $this->form_validation->set_rules('ConfirmPassword', lang('confirm_password'), 'required');
        
        $this->form_validation->set_rules('CountryID', 'Country', 'required');
        $this->form_validation->set_rules('StateID', 'State', 'required');
        $this->form_validation->set_rules('CityID', 'City', 'required');
       
        

        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {
                return true;
        }

    }
    
    public function save()
    {
        $post_data                          = $this->input->post();
        
        $parent                             = 'User_model';
        $child                              = 'User_text_model';
        $save_parent_data                   = array();
        $save_child_data                    = array();

        


        
        $getSortValue = $this->$parent->getLastRow('UserID');
        $sort = 0;
        if(!empty($getSortValue))
        {
           $sort = $getSortValue['SortOrder'] + 1;
        }
       
        $save_parent_data['SortOrder']      = $sort;
        
        $save_parent_data['IsActive']       = 1;
        $save_parent_data['RoleID']         = 2;// customer
        $save_parent_data['Password']       = md5($post_data['Password']);
        $save_parent_data['Email']          = $post_data['Email'];
       
        
        $save_parent_data['Mobile']          = $post_data['Mobile'];
        $save_parent_data['IsMobileVerified']          = 1;
        $save_parent_data['IsEmailVerified']          = 1;
       
        $save_parent_data['Gender']         = $post_data['Gender'];
        $save_parent_data['CountryID']      = $post_data['CountryID'];
        $save_parent_data['StateID']        = $post_data['StateID'];  
        $save_parent_data['CityID']         = $post_data['CityID']; 
       
        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
        {


           
            
            $default_lang = getDefaultLanguage(); 
            $save_child_data['FullName']                        = $post_data['FirstName'].' '.$post_data['LastName'];
            
            $save_child_data['UserID']                          = $insert_id;
            $save_child_data['SystemLanguageID']                = 1;
            $this->$child->save($save_child_data);
            
               

            $success['error']   = false;
            $success['reload']   = false;
            // $success['success'] = 'Your account is created successfully please wait admin to approved your account';
            $success['success'] = 'Your account is created successfully';
            $success['reload']   = true;
            
            
            echo json_encode($success);
            exit;


        }else
        {
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }


    public function updateAccountInfo($UserID = '',$PaymentID = 0){
        if($UserID != ''){
            $this->data['UserID'] = $UserID;
            $this->data['PaymentID'] = $PaymentID;
            $this->data['view'] = 'front/'.$this->data['ControllerName'].'/account_info';
            $this->load->view('front/layouts/default',$this->data);
        }else{
            redirect(base_url());
        }
    }

    //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $user = $this->session->userdata('user');
        $this->session->unset_userdata('user');
        redirect($this->config->item('base_url'));
    }
}