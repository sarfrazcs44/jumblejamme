<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Product_model');
        $this->load->model('Product_variation_model');
        $this->load->model('Product_rating_model');
    }
    public function index($page = 0)
    {
        $this->data['view'] = 'frontend/products';

        $limit = 12;
        $post_data = $this->input->get();

        $where = '';
        if(isset($post_data['CategoryID']) && $post_data['CategoryID'] > 0)
        {
            $where .= " AND products.CategoryID = ".$post_data['CategoryID'];
        }
        if(isset($post_data['key']) && $post_data['key'] != '')
        {
            $where .= " AND products_text.Title LIKE '%".$post_data['key']."%'";
        }
        if(isset($post_data['minprice']) && $post_data['minprice'] > 0)
        {
            $where .= " AND products.ProductPrice >= ".$post_data['minprice'];
        }
        if(isset($post_data['maxprice']) && $post_data['maxprice'] > 0)
        {
            $where .= " AND products.ProductPrice <= ".$post_data['maxprice'];
        }




        $this->data['products'] = $this->Product_model->getProducts('products.IsActive = 1 AND products.IsFixedPrice = 1 AND products.IsDemand = 0 '.$where,'EN',$limit,$limit * $page,'products.ProductID','DESC');


        $this->data['product_count'] = $this->Product_model->getCountOfProducts('products.IsActive = 1 AND products.IsFixedPrice = 1 AND products.IsDemand = 0 '.$where,'EN');
        $this->data['Page'] = $page;

        //echo $this->db->last_query();exit;

       // echo $this->data['product_count'];exit;

        $url = 'products/index/';
        $links = $this->createLinks($this->data['product_count']/$limit,$page,$url);
        $this->data['links'] = $links;


        $this->data['ShowPaggination'] = true;
        if($this->data['product_count'] < $limit){
            $this->data['ShowPaggination'] = false;
        }


        $this->load->view('frontend/layouts/default', $this->data);
    }
    public function detail($id)
    {

        //$parent = $this->data['Parent_model'];
        $this->data['product'] = $this->Product_model->getJoinedData(false, 'ProductID', 'products.ProductID ='.$id, 'DESC', '');


        if (!$this->data['product']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['product'] = $this->data['product'][0];
        $this->data['view'] = 'frontend/product-detail';
        $this->data['variants'] = $this->Product_variation_model->getMultipleRows(array('ProductID' => $id));
        $this->data['images'] = get_images($id);
        $this->data['reviews']  = $this->Product_rating_model->getProductRatings("product_ratings.ProductID = " . $id);
       // print_rm($this->data['reviews']);


        $this->load->view('frontend/layouts/default', $this->data);
    }
}