<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technician extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('User_model');
        $this->load->Model('User_text_model');
        $this->load->Model('City_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';
    }

    public function index()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['results'] = $this->User_model->getUsers('users.RoleID = 2'); // only technician users
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $language = 'EN';
        // $this->data['cities'] = $this->City_model->getAllCities($this->language, false);
        $where = 'cities.IsActive = 1 AND system_languages.ShortCode = "' . $language . '"';
        $this->data['cities'] = $this->City_model->getJoinedData(false, 'CityID', $where);
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['result'] = $this->User_model->getUsers("users.UserID = $id");
        // dump($this->data['result']);

        if (!$this->data['result']) {
            $this->session->set_flashdata('message', lang('some_thing_went_wrong'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $language = 'EN';
        // $this->data['cities'] = $this->City_model->getAllCities($this->language, false);
        $where = 'cities.IsActive = 1 AND system_languages.ShortCode = "' . $language . '"';
        $this->data['cities'] = $this->City_model->getJoinedData(false, 'CityID', $where);
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data['UserID'] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;
            case 'getCities':
                $this->getCities();
                break;

        }
    }

    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('FullName', 'Full Name', 'required');
        $this->form_validation->set_rules('Email', 'Email', 'required|valid_email|is_unique[users.Email]');
        $this->form_validation->set_rules('Password', 'Password', 'required|min_length[8]');
        $this->form_validation->set_rules('Mobile', 'Mobile', 'required|is_unique[users.Mobile]');
        $this->form_validation->set_rules('CityID', 'City', 'required');
        $this->form_validation->set_rules('CompanyName', 'Company Name', 'required');

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $post_data = $this->input->post();
        $post_data['RoleID'] = 2;
        $post_data['Password'] = md5($post_data['Password']);
        $post_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
        $post_data['CreatedAt'] = $post_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $post_data['CreatedBy'] = $post_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
        if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {
            $post_data['Image'] = $this->uploadImage("Image", "uploads/users/");
        }

        $full_name = $post_data['FullName'];
        unset($post_data['FullName']);
        unset($post_data['form_type']);
        $insert_id = $this->User_model->save($post_data);
        if ($insert_id > 0) {
            $this->User_text_model->save(array('FullName' => $full_name, 'UserID' => $insert_id, 'SystemLanguageID' => 1));
            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $post_data = $this->input->post();
        if (isset($post_data['UserID'])) {
            if ($post_data['Password'] != '')
            {
                $post_data['Password'] = md5($post_data['Password']);
            }
            $post_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
            $post_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $post_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
            if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {
                $post_data['Image'] = $this->uploadImage("Image", "uploads/users/");
            }
            $UserID = $post_data['UserID'];
            $full_name = $post_data['FullName'];
            unset($post_data['FullName']);
            unset($post_data['form_type']);
            unset($post_data['UserID']);
            $this->User_model->update($post_data, array('UserID' => $UserID));
            $this->User_text_model->update(array('FullName' => $full_name), array('UserID' => $UserID));
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $UserID;
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $deleted_by = array();
        $deleted_by['UserID'] = $this->input->post('id');
        $this->User_model->delete($deleted_by);
        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');
        echo json_encode($success);
        exit;
    }


}