<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['TableKey'] = 'FeedbackID';
        $this->data['Table'] = 'feedbacks';


    }


    public function index()
    {
        $parent = $this->data['Parent_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        $this->data['results'] = $this->$parent->getAll(false);

        $this->load->view('backend/layouts/default', $this->data);
    }
    public function view($id = '')
    {
        if (!checkUserRightAccess(63, $this->session->userdata['admin']['UserID'], 'CanView')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $id = base64_decode($id);
        
        $parent = $this->data['Parent_model'];
        $this->data['feedback'] = $this->$parent->get($id,false,'FeedbackID');
        if (empty($this->data['feedback'])) {
            $this->session->set_flashdata('message', lang('some_thing_went_wrong'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/view';
        

        $this->load->view('backend/layouts/default', $this->data);

    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'delete':
                $this->delete();
                break;

        }
    }

    private function delete()
    {

        if (!checkUserRightAccess(63, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


}