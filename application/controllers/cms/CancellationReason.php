<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CancellationReason extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();
        $this->load->model('Order_cancellation_reason_model');
        $this->data['language'] = $this->language;
    }

    public function index()
    {
        $this->data['view'] = 'backend/cancellationReason/manage';
        $this->data['results'] = $this->Order_cancellation_reason_model->getAll();
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['view'] = 'backend/cancellationReason/add';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['result'] = $this->Order_cancellation_reason_model->get($id, false, 'OrderCancellationReasonID');
        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['view'] = 'backend/cancellationReason/edit';
        $this->load->view('backend/layouts/default', $this->data);

    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }

    private function save()
    {
        if (!checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        unset($post_data['form_type']);
        $post_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
        $insert_id = $this->Order_cancellation_reason_model->save($post_data);
        if ($insert_id > 0) {
            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            // $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            $success['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        if (!checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        unset($post_data['form_type']);
        if (isset($post_data['OrderCancellationReasonID']) && $post_data['OrderCancellationReasonID'] > 0) {
            $post_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
            $update_by['OrderCancellationReasonID'] = $post_data['OrderCancellationReasonID'];
            $this->Order_cancellation_reason_model->update($post_data, $update_by);
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function delete()
    {

        if (!checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

}