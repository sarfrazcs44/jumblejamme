<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidding extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Product_model');
        $this->load->model('Category_model');
    }
    public function index($page = 0)
    {
        
        

        $this->data['view'] = 'frontend/bidding';

        $limit = 12;
        $post_data = $this->input->get();

        $where = '';
        if(isset($post_data['CategoryID']) && $post_data['CategoryID'] > 0)
        {
            $where .= " AND products.CategoryID = ".$post_data['CategoryID'];
        }
        if(isset($post_data['key']) && $post_data['key'] != '')
        {
            $where .= " AND products_text.Title LIKE '%".$post_data['key']."%'";
        }
        if(isset($post_data['minprice']) && $post_data['minprice'] > 0)
        {
            $where .= " AND products.ProductPrice >= ".$post_data['minprice'];
        }
        if(isset($post_data['maxprice']) && $post_data['maxprice'] > 0)
        {
            $where .= " AND products.ProductPrice <= ".$post_data['maxprice'];
        }




        $this->data['bidding_products'] = $this->Product_model->getProducts('products.IsActive = 1 AND products.IsFixedPrice = 0 AND products.IsDemand = 0 '.$where,'EN',$limit,$limit * $page,'products.ProductID','DESC');


        $this->data['product_count'] = $this->Product_model->getCountOfProducts('products.IsActive = 1 AND products.IsFixedPrice = 0 AND products.IsDemand = 0 '.$where,'EN');
        $this->data['Page'] = $page;

        //echo $this->db->last_query();exit;

       // echo $this->data['product_count'];exit;

        $url = 'bidding/index/';
        $links = $this->createLinks($this->data['product_count']/$limit,$page,$url);
        $this->data['links'] = $links;


        $this->data['ShowPaggination'] = true;
        if($this->data['product_count'] < $limit){
            $this->data['ShowPaggination'] = false;
        }
       // echo $this->data['product_count'];exit;


        $this->load->view('frontend/layouts/default', $this->data);
    }
}