<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends Base_Controller
{
    public $data = array();


    public function __construct()
    {
        parent::__construct();
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->load->Model([
            'Product_model',
            'Site_images_model',
            'Category_model',
            'Order_model',
            'Temp_order_model'

        ]);

        $this->load->helper('text');
    }


    public function index()
    {

        $this->data['view'] = 'front/' . $this->data['ControllerName'] . '/cart';
        $this->load->view('front/layouts/default', $this->data);
    }


    public function add_to_cart()
    {


        if ($this->session->userdata('user')) {

            $user_id = $this->session->userdata['user']['UserID'];

        } else {

            if (!get_cookie('temp_order_key')) {
                $user_id = date('YmdHis') . RandomString();
                $cookie = array(
                    'name' => 'temp_order_key',
                    'value' => $user_id,
                    // 'expire' => time() + 86500,
                    'expire' => 86500,

                );
                set_cookie($cookie);
            } else {
                $user_id = get_cookie('temp_order_key');
            }
        }


        


       
        


        $posted_product_quantity = $this->input->post('product_quantity');
        $product_variant = $this->input->post('product_variant');
        

        $already_added = $this->Temp_order_model->alreadyAddToCart($user_id, $this->input->post('product_id'));
//echo $this->db->last_query();exit;

        if ($already_added) {

            $update = array();
            $update_by = array();

            $update['ProductQuantity'] = $already_added['ProductQuantity'] + $posted_product_quantity;
            $update_by['TempOrderID'] = $already_added['TempOrderID'];
            if($product_variant > 0){
                 $save_data['ProductVariantID'] = $this->input->post('product_variant');
            }

            $this->Temp_order_model->update($update, $update_by);


            $success['error'] = false;
            $success['reload'] = True;
            $success['success'] = lang('Added Successfully');
            echo json_encode($success);
            exit;

        } else {
            $save_data = array();
            $save_data['UserID'] = $user_id;//$this->session->userdata['user']['user_id'];
            $save_data['ProductQuantity'] = $posted_product_quantity;
            $save_data['ProductID'] = $this->input->post('product_id');
            if($product_variant > 0){
                 $save_data['ProductVariantID'] = $this->input->post('product_variant');
            }

            $this->Temp_order_model->save($save_data);

            $success['error'] = false;
            $success['reload'] = True;
            $success['success'] = lang('Added Successfully');
            echo json_encode($success);
            exit;
        }


    }


    public function delete_cart_item()
    {
        $temp_order_id = $this->input->post('temp_order_id');

        $deleted_by['TempOrderID'] = $temp_order_id;
        $this->Temp_order_model->delete($deleted_by);
        $success['error'] = false;
        $success['reload'] = True;
        $success['success'] = 'Deleted Successfully';
        echo json_encode($success);
        exit;

    }


    public function update_cart()
    {

        $this->Temp_order_model->update(array('ProductQuantity' => $this->input->post('value')), array('TempOrderID' => $this->input->post('id')));
        $success['error'] = false;
        $success['reload'] = True;
        $success['success'] = 'Update Successfully';
        echo json_encode($success);
        exit;

    }


}
