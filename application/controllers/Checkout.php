<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends Base_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Order_model');
        $this->load->model('Order_item_model');
        $this->load->model('Temp_order_model');
        $this->load->model('Order_request_model');
        $this->load->model('User_model');
        $this->load->model('Country_model');
        //$this->load->model('Payment_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();

    }


    public function index()
    {
        $this->order_detail();
    }

    public function order_detail()
    {
        if (!$this->session->userdata('user')) {
            $this->session->set_userdata('url', 'checkout');
            redirect('account/login');
        } else {


            $user_id = $this->session->userdata['user']['UserID'];
            $this->data['products'] = $this->Temp_order_model->getCartItems('temp_orders.UserID = '.$user_id);
            //echo '<pre>';print_r($this->data['products']);exit;

            if (empty($this->data['products'])) {
                $this->session->set_flashdata('message', "There is no item in the cart.");
                redirect(base_url());
            }

            $this->data['total_product'] = getTotalProduct($user_id);

            $this->data['userinfo'] = $this->User_model->getJoinedData(true, 'UserID', 'users.UserID =' . $this->session->userdata['user']['UserID']);

            //$this->data['step'] = $step;
            $this->data['countries'] = $this->Country_model->getAllJoinedData(false,'CountryID',$this->language,'countries.IsActive = 1','ASC','countries_text.Title',true);

            $this->data['view'] = 'frontend/checkout';
            $this->load->view('frontend/layouts/default', $this->data);


        }
    }

    public function place_order()
    {

        if ($this->input->post() != NULL) {

            $post_data = $this->input->post();

            $user_id = $this->session->userdata['user']['UserID'];
            $products  = $this->Temp_order_model->getCartItems('temp_orders.UserID = '.$user_id);
            $product_categories = array();


            $this->form_validation->set_rules('FirstName', lang('FirstName'), 'trim|required');
            $this->form_validation->set_rules('LastName', lang('LastName'), 'trim|required');
            $this->form_validation->set_rules('Phone', lang('Phone no'), 'trim|required');
            
            $this->form_validation->set_rules('ShippingAddress', lang('Address'), 'trim|required');
            $this->form_validation->set_rules('ShippingZipCode', lang('Zip code'), 'trim|required');
           
           
            $this->form_validation->set_rules('CountryID', lang('Country'), 'trim|required');
            $this->form_validation->set_rules('StateID', lang('State'), 'trim|required');
            $this->form_validation->set_rules('CityID', lang('City'), 'trim|required');


            $this->form_validation->set_rules('NameOnCard', lang('Name On Card'), 'trim|required');
            $this->form_validation->set_rules('CardNumber', lang('Card Number'), 'trim|required');
            $this->form_validation->set_rules('CardExpiry', lang('Expiration Date'), 'trim|required');
            $this->form_validation->set_rules('CardCVC', lang('CardCVC'), 'trim|required');





            

           // $product_categories = array_column($products, 'CategoryID');

            

            // $this->form_validation->set_rules('Cnic', 'CNIC', 'trim|required');
            //$this->form_validation->set_rules('AmountDeposit', 'Amount', 'trim|required');

            //$this->form_validation->set_rules('TransactionID', 'TransactionID', 'trim|required');

            //$this->form_validation->set_rules('DepositDate', 'Deposit Date', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
            }

            

            $AmountDeposit = 0;
            $vendors_array = array();
            foreach ($products as $product) {

                if (!in_array($product->DealerID, $vendors_array)) {
                    $vendors_array[] = $product->DealerID;

                }
            }

            $save_data = array();
           // $save_data['OrderTrackID'] = RandomString();
            $save_data['UserID'] = $user_id;
            $save_data['FirstName'] = $post_data['FirstName'];
            $save_data['LastName'] = $post_data['LastName'];
            $save_data['Phone'] = $post_data['Phone'];
          
            $save_data['ShippingAddress'] = $post_data['ShippingAddress'];
            $save_data['CountryID'] = $post_data['CountryID'];
            $save_data['StateID'] = $post_data['StateID'];
            $save_data['CityID'] = $post_data['CityID'];
            $save_data['Currency'] = $this->currency_code;
            $save_data['CurrencyRate'] = $this->currency_rate;

            if(isset($post_data['SaveCardDetails'])){
                $save_data['NameOnCard'] = $post_data['NameOnCard'];
                $save_data['CardNumber'] = $post_data['CardNumber'];
                $save_data['CardExpiry'] = $post_data['CardExpiry'];
                $save_data['CardCVC']    = $post_data['CardCVC'];

            }
            
            

            $save_data['CreatedAt'] = $save_data['UpdatedAt'] = time();


            $insert_id = $this->Order_model->save($save_data);

            if ($insert_id > 0){
                
                
                    $product_ids_for_activity = array();
                    foreach ($vendors_array as $key => $value) {
                        $OrderTrackID = str_pad($insert_id, 5, '0', STR_PAD_LEFT);
                        $order_request_data = [
                            'OrderID' => $insert_id,
                            'OrderStatusID' => 3,// this is for payment done can be change later
                            'OrderTrackID' => $OrderTrackID,
                            'DealerID' => $value
                        ];
                        
                        $TotalAmount = 0;
                        $ActualDeliveryCharges = 0;
                        $order_request_id = $this->Order_request_model->save($order_request_data);
                        $update_order['OrderTrackID'] = str_pad($order_request_id, 5, '0', STR_PAD_LEFT);
                        $update_order['OrderLastStatusID'] = 1;
                        $update_order_by['OrderRequestID'] = $order_request_id;
                        $this->Order_request_model->update($update_order, $update_order_by);
                        $order_item_data = array();
                        foreach ($products as $product) {
                            if ($product->DealerID == $value) {
                                    if($product->ProductVariantID > 0){
                                          $price = rateAccordingToSetCurrency($product->VariantPrice,$this->currency_rate) * $product->ProductQuantity;
                                          $order_item_data['ProductVariantID'] = $product->ProductVariantID;

                                       }else{
                                          $price = rateAccordingToSetCurrency($product->ProductPrice,$this->currency_rate) * $product->ProductQuantity;
                                       }
                                
                                    $TotalAmount += $price;
                                    $DeliveryCharges = 0;
                                    if($product->IsDeliveryChargesInclude == 0){
                                       $DeliveryCharges = rateAccordingToSetCurrency($product->DeliveryCharges,$this->currency_rate);
                                    }
                                    $ActualDeliveryCharges += $DeliveryCharges;
                                    $order_item_data['OrderRequestID'] = $order_request_id;
                                    $order_item_data['ProductID'] = $product->ProductID;
                                    $order_item_data['Quantity'] = $product->ProductQuantity;
                                    $order_item_data['Price'] = $product->ProductPrice;
                                    $this->Order_item_model->save($order_item_data);
                                    $product_ids_for_activity[] = $product->ProductID;
                                
                            }
                        }

                        // update order request invoice details
                        $update_order_request_data['TotalAmount'] = $TotalAmount;
                        $update_order_request_data['ActualDeliveryCharges'] = $ActualDeliveryCharges;
                        //$update_order_request_data['VatPercentageApplied'] = $user_customization->VatPercentage;
                       // $update_order_request_data['VatAmountApplied'] = ($update_order_request_data['TotalAmount'] * $update_order_request_data['VatPercentageApplied']) / 100;
                        $update_order_request_data['GrandTotal'] = $update_order_request_data['TotalAmount'] + $update_order_request_data['ActualDeliveryCharges'];
                        $update_order_request_data_by['OrderRequestID'] = $order_request_id;
                        $this->Order_request_model->update($update_order_request_data, $update_order_request_data_by);
                    }
                    $deleted_by['UserID'] = $this->session->userdata['user']['UserID'];
                    $this->Temp_order_model->delete($deleted_by);
                    $return_array = $this->Order_model->get($insert_id, true, 'OrderID');
                    $order_request = $this->Order_request_model->getMultipleRows(array('OrderID' => $insert_id), true);
                    $i = 0;
                    foreach ($order_request as $key => $value) {
                        $return_array['order_requests'][$i] = $value;
                        $i++;
                    }

                

                    

                    $success['error'] = false;
                    $success['success'] = lang('Order place successfully.');
                    $success['redirect'] = true;
                    $success['url'] = '';

                    echo json_encode($success);
                    exit;

            } else {
                $errors['error'] = lang('something went wrong.');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }


        }

    }

    public function sendNotificationEmailToVendor($order_id)
    {
        

        $OrderDetail               = $this->Order_model->get($order_id, true, 'OrderID');
        $order_request = $this->Order_request_model->getMultipleRows(array('OrderID' => $order_id), true);
                   
        foreach ($order_request as $key => $value) {
                $this->data['order_items'] = $order_details = $this->Order_model->getOrdersDetailWithCustomerDetail($order_id,false,'orders_requests.OrderRequestID = '.$value['OrderRequestID']);
                $vendor_data = getUserData($value['VendorID']);

                $message = $this->load->view('front/emails/order_confirmation_vendor',$this->data, true);
                
                
                $email_data['to'] = $vendor_data[0]['Email'];
                $email_data['subject'] = 'Order received at brandsvalley : Order # ' . $order_details[0]['OrderTrackID'];
                $email_data['from'] = 'noreply@brandsvalley.net';
                $email_data['body'] = $message;
               
                sendEmail($email_data);
                sms(array('receiver' => $vendor_data[0]['Phone'],'msgdata' => 'You has been receiver an order with track id '.$order_details[0]['OrderTrackID'].''));
          
        }
      
        
         
        
        return true;
    }  


   /* public function sendThankyouEmailToCustomer($order_id)
    {
        $data = array();
        $requests = array();
        //$data['order_items'] =  $order_details  =  $this->Order_model->getOrdersDetailWithCustomerDetail($order_id);
      
        //print_rm($order_details);

        $order_details  =  $this->Order_model->getOrdersRequest('orders.OrderID = '.$order_id);

        foreach ($order_details as $key => $value) {
              $requests[$key] = $value;

              $requests[$key]['items'] = $this->Order_model->getOrderItems($value['OrderRequestID']);
              //echo $this->db->last_query();exit;
        }

       
        $data['requests'] = $requests;
        

        $email_template = get_email_template(15, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $order_details[0]['FullName'], $message);
        
        $data['message'] = $message;

        $message = $this->load->view('emails/order_confirmation_email',$data, true);
        
        //echo email_format($message);exit;

       
        $data['to'] = $order_details[0]['UserEmail'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message,false);
        sendEmail($data);
    }*/


    


    public function sendThankyouEmailToCustomer($order_id)
    {
        $this->data['order_items'] = $order_details = $this->Order_model->getOrdersDetailWithCustomerDetail($order_id);
        $this->data['delivery_charges'] = $this->Order_model->getTotalOrdersDeliveryCharges($order_id);

        $message = $this->load->view('front/emails/order_confirmation', $this->data, true);
        $email_data['to'] = $order_details[0]['UserEmail'] . ',' . $order_details[0]['Email'];
        $email_data['subject'] = 'Order placed at brandsvalley : Order # ' . $order_details[0]['OrderTrackID'];
        $email_data['from'] = 'noreply@brandsvalley.net';
        $email_data['body'] = $message;
        //print_rm($email_data);
        sendEmail($email_data);
        sms(array('receiver' => $order_details[0]['Telephone'], 'msgdata' => 'Your customer order has been placed successfully with track id: ' . $order_details[0]['OrderTrackID'] . ' and price ' . number_format($order_details[0]['AmountDeposit']) . ' Your product will be delivered soon. Thanks'));


        return true;
    }


}
