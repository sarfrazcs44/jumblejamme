<?php

Class Temp_order_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("temp_orders");

    }

    public function getTotalProduct($user_id)
    {
        $this->db->select('COUNT(ProductID) as CartProductsCount, SUM(ProductQuantity) as CartQuantityCount');
        $this->db->from('temp_orders');
        $this->db->where('UserID', $user_id);
        return $this->db->get()->result();
    }

    public function getCartItemsBooths($where = false, $system_language_code = 'EN')
    {
        $this->db->select("temp_orders.*, users.UserID, users_text.BoothName, countries.Currency, countries.CurrencySymbol, user_customization.VatPercentage");
        $this->db->from('temp_orders');
        $this->db->join('users', 'temp_orders.BoothID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('user_customization', 'users.UserID = user_customization.UserID');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('countries', 'cities.CountryID = countries.CountryID', 'LEFT');
        $this->db->join('system_languages', 'users_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        if ($where) {
            $this->db->where($where);
        }
        $this->db->group_by('temp_orders.BoothID');
        $this->db->order_by('temp_orders.BoothID', 'ASC');
        $result = $this->db->get();
        return $result->result();
    }

    public function getCartItems($where = false, $system_language_code = 'EN')
    {
        $this->db->select("temp_orders.*,product_price_variations.MinRang,product_price_variations.MaxRang,product_price_variations.Price as VariantPrice ,products_text.Title as ProductTitle, products.*,products.UserID as DealerID");
        $this->db->from('temp_orders');
        $this->db->join('products', 'temp_orders.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('product_price_variations', 'products.ProductID = product_price_variations.ProductID', 'LEFT');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        if ($where) {
            $this->db->where($where);
        }
        $this->db->group_by('temp_orders.TempOrderID');
        $result = $this->db->get();
        //echo $this->db->last_query();exit;
        return $result->result();
    }

    public function alreadyAddToCart($user_id='',$product_id = ''){
            
            $query = "SELECT * from temp_orders WHERE UserID=$user_id AND ProductID = $product_id";
            $result = $this->db->query($query);
            return $result->row_array();
    }

    public function getAbandonedCart()
    {
        $this->db->select("users.UserID,users.OnlineStatus,users.Mobile,users.Email,users_text.FullName,COUNT(temp_orders.ProductID) as CartProductsCount, SUM(temp_orders.ProductQuantity) as CartQuantityCount");
        $this->db->from('temp_orders');
        $this->db->join('users', 'temp_orders.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->group_by('temp_orders.UserID');
        $this->db->order_by('CartQuantityCount', 'DESC');
        $result = $this->db->get();
        return $result->result();
    }

    public function getAbandonedCartAnonymous()
    {
        $sql = "SELECT temp_orders.UserID, COUNT(temp_orders.ProductID) as CartProductsCount, SUM(temp_orders.ProductQuantity) as CartQuantityCount FROM temp_orders WHERE UserID NOT IN (SELECT UserID FROM users) HAVING CartProductsCount > 0 AND CartQuantityCount > 0";
        $query = $this->db->query($sql);
        return $query->result();
    }

}
