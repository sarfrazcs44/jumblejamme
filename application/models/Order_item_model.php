<?php

Class Order_item_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("order_items");

    }

    public function getOrderItems($where = false, $system_language_code = 'EN')
    {
        $this->db->select('order_items.*,orders.Currency,orders.CurrencyRate,order_items.Quantity as OrderQuantity, products_text.Title as ProductTitle, products.*,product_price_variations.MaxRang');
        $this->db->from('order_items');
        $this->db->join('products', 'products.ProductID = order_items.ProductID');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('orders_requests', 'orders_requests.OrderRequestID = order_items.OrderRequestID');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->join('product_price_variations', 'order_items.ProductVariantID = product_price_variations.ProductPriceVariationID','LEFT');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = products_text.SystemLanguageID', 'Left');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        if ($where)
        {
            $this->db->where($where);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

}
    
    
    