<?php
Class Chat_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("chats");
    }

    public function getChatRooms($user_id, $start, $limit,$type)
    {

       
        /*$sql = "SELECT cm.*, c.* FROM chat_messages cm 
                JOIN chats c ON cm.ChatID = c.ChatID 
                WHERE ((cm.SenderID = $user_id OR cm.ReceiverID = $user_id) AND c.Type = '".$type."' ) OR ((cm.SenderID = $user_id OR cm.ReceiverID = $user_id) AND c.ReceiverType = '".$type."')
                GROUP BY cm.ChatID 
                ORDER BY c.LastActivityAt DESC LIMIT $start, $limit";*/

         $sql = "SELECT chat_messages.*, chats.*,su.UserID as ConversationSenderID,ru.UserID as ConversationReceiverID,sut.FullName as ConversationSenderName,sut.BoothName as ConversationSenderBoothName,rut.FullName as ConversationReceiverName,rut.BoothName as ConversationReceiverBoothName,su.UserName as ConversationSenderUserName,su.BoothUserName as ConversationSenderBoothUserName,ru.UserName as ConversationReceiverUserName,ru.BoothUserName as ConversationReceiverBoothUserName,su.CompressedImage as ConversationSenderImage,ru.CompressedImage as ConversationReceiverImage,su.CompressedBoothImage as ConversationSenderBoothImage,ru.CompressedBoothImage as ConversationReceiverBoothImage
                    FROM chats 
                    LEFT JOIN chat_messages ON chat_messages.ChatID = chats.ChatID 
                    LEFT JOIN users su ON chats.SenderID = su.UserID 
                    LEFT JOIN users ru ON chats.ReceiverID = ru.UserID
                    LEFT JOIN users_text sut ON su.UserID = sut.UserID AND sut.SystemLanguageID = 1
                    LEFT JOIN users_text rut ON ru.UserID = rut.UserID AND rut.SystemLanguageID = 1
                    WHERE (chats.SenderID = $user_id AND chats.Type = '".$type."' ) OR (chats.ReceiverID = $user_id AND chats.ReceiverType = '".$type."') OR (chats.ReceiverID = $user_id AND chats.ReceiverType = '".$type."' )  
                    GROUP BY chats.ChatID 
                    ORDER BY chats.LastActivityAt DESC LIMIT $start, $limit";       
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
}
