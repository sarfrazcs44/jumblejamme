<?php

Class Order_request_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("orders_requests");

    }

    public function getOrdersRequests($where = false, $system_language_code = 'EN', $limit = false, $start = 0, $sort_by = 'orders_requests.OrderRequestID', $sort_as = 'DESC')
    {
        $this->db->select("orders_requests.*,
                            bu.UserID as BoothID, 
                            bu.CompressedBoothImage as BoothImage, 
                            bu.UserName as BoothUserName, 
                            bu.Email as BoothEmail, 
                            bu.Mobile as BoothMobile, 
                            but.FullName as BoothName, 
                            u.UserID as UserID, 
                            u.CompressedImage as UserImage, 
                            u.UserName as UserName, 
                            u.Email as UserEmail, 
                            u.Mobile as UserMobile,
                            u.Gender as UserGender,
                            u.IsEmailVerified,
                            u.IsMobileVerified,
                            u.OnlineStatus,
                            ut.FullName, 
                            bu.BoothType, 
                            IF('$system_language_code' = 'AR', order_statuses.OrderStatusAr , order_statuses.OrderStatusEn) as OrderStatus,
                            IF('$system_language_code' = 'AR', order_cancellation_reasons.CancellationReasonAr , order_cancellation_reasons.CancellationReasonEn) as OrderCancellationReason,
                            cities_text.Title as UserCityTitle,
                            states_text.Title as UserStateTitle,
                            orders.CreatedAt as OrderReceivedAt,
                            orders.Currency,
                            orders.CurrencyRate,
                            orders.FirstName,
                            orders.LastName,
                            orders.Phone,
                            orders.ShippingAddress,
                            orders.ShippingZipCode,
                            countries_text.Title as UserCountryTitle");
        $this->db->from('orders_requests');
        $this->db->join('order_statuses', 'orders_requests.OrderStatusID = order_statuses.OrderStatusID');
        $this->db->join('order_cancellation_reasons', 'orders_requests.OrderCancellationReasonID = order_cancellation_reasons.OrderCancellationReasonID', 'LEFT');
        $this->db->join('users bu', 'orders_requests.DealerID = bu.UserID');
        $this->db->join('users_text but', 'bu.UserID = but.UserID AND but.SystemLanguageID = 1');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        $this->db->join('users u', 'orders.UserID = u.UserID');
        $this->db->join('users_text ut', 'u.UserID = ut.UserID AND ut.SystemLanguageID = 1');
       // $this->db->join('user_addresses', 'orders.AddressID = user_addresses.AddressID', 'LEFT');
       // $this->db->join('user_customization', 'bu.UserID = user_customization.UserID');
        
        $this->db->join('cities', 'orders.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1');

        $this->db->join('states', 'orders.StateID = states.StateID', 'LEFT');
        $this->db->join('states_text', 'states.StateID = states_text.StateID AND states_text.SystemLanguageID = 1');

        $this->db->join('countries', 'orders.CountryID = countries.CountryID', 'LEFT');
        $this->db->join('countries_text', 'countries.CountryID = countries_text.CountryID AND countries_text.SystemLanguageID = 1');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->group_by('orders_requests.OrderRequestID');
        $this->db->order_by($sort_by, $sort_as);

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getOrderRequestsForCronjob($where = false)
    {
        $this->db->select("orders_requests.*, orders.*");
        $this->db->from('orders_requests');
        $this->db->join('orders', 'orders_requests.OrderID = orders.OrderID');
        if ($where)
        {
            $this->db->where($where);
        }
        return $this->db->get()->result();
    }

}
    
    
    