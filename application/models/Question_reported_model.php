<?php

Class Question_reported_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("question_reported");

    }

    public function getCountReports($QuestionID)
    {
        $this->db->select('COUNT(QuestionID) as Total');
        $this->db->from('question_reported');
        $this->db->where('QuestionID', $QuestionID);

        $result = $this->db->get();
        return $result->row();
    }

    public function getReports($QuestionID)
    {
        $this->db->select('question_reported.*, users.UserID, users.CompressedImage, users_text.FullName, users_text.BoothName, users.UserName, users.BoothUserName');
        $this->db->from('question_reported');
        $this->db->join('users', 'question_reported.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->where('question_reported.QuestionID', $QuestionID);

        $result = $this->db->get();
        return $result->result();
    }


}