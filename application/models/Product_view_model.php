<?php
Class Product_view_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("product_views");
    }

    public function getProductsViewsCount($ProductID)
    {
        $this->db->select('COUNT(ProductID) as Total');
        $this->db->from('product_views');
        $this->db->where('ProductID', $ProductID);
        $result = $this->db->get();
        return $result->row();
    }

    public function getProductsViews($system_language_code = 'EN', $sort_as = 'DESC', $limit = false, $start = 0)
    {
        $this->db->select('products.*, products_text.*, COUNT(product_views.ProductID) as ViewCount');
        $this->db->from('product_views');
        $this->db->join('products', 'product_views.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->having('ViewCount >', 0);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->group_by('product_views.ProductID');
        $this->db->order_by('ViewCount', $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return array();
        }
    }

}