<?php

Class Comment_reported_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("comment_reported");

    }

    public function getCountReports($CommentID)
    {
        $this->db->select('COUNT(CommentID) as Total');
        $this->db->from('comment_reported');
        $this->db->where('CommentID', $CommentID);

        $result = $this->db->get();
        return $result->row();
    }

    public function getReports($CommentID)
    {
        $this->db->select('comment_reported.*, users.UserID, users.CompressedImage, users_text.FullName, users_text.BoothName, users.UserName, users.BoothUserName');
        $this->db->from('comment_reported');
        $this->db->join('users', 'comment_reported.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->where('comment_reported.CommentID', $CommentID);

        $result = $this->db->get();
        return $result->result();
    }


}