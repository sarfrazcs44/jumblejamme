<?php
Class Dashboard_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getMostOrdersByTechnician()
    {
        $sql = "SELECT cities_text.Title, users.Image, users_text.UserID, users_text.FullName, COUNT(bookings.BookingID) AS bookings_count 
                  FROM bookings
                  LEFT JOIN users ON bookings.TechnicianID = users.UserID 
                  JOIN users_text ON users.UserID = users_text.UserID 
                  LEFT JOIN cities ON users.CityID = cities.CityID 
                  JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
                  WHERE bookings.Status = 5 AND bookings.TechnicianID > 0
                  GROUP BY bookings.TechnicianID 
                  ORDER BY bookings_count DESC LIMIT 10";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getMostOrdersByCategory()
    {
        $sql = "SELECT categories.Image, categories.CategoryID, categories_text.Title, COUNT(bookings.BookingID) AS bookings_count 
                  FROM bookings
                  LEFT JOIN categories ON bookings.CategoryID = categories.CategoryID 
                  JOIN categories_text ON categories.CategoryID = categories_text.CategoryID AND categories_text.SystemLanguageID = 1
                  GROUP BY bookings.CategoryID 
                  ORDER BY bookings_count DESC LIMIT 10";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getTotalOrders()
    {
        $Date = date('Y-m-d');
        $sql = "SELECT COUNT(orders_requests.OrderRequestID) as ordersCount FROM orders_requests";
        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['ordersCount'] > 0 ? number_format($data['ordersCount']) : 0);
    }

    public function getTodaysOrders()
    {
        $Date = date('Y-m-d');
        $sql = "SELECT COUNT(orders_requests.OrderRequestID) as ordersCount FROM orders_requests JOIN orders ON orders_requests.OrderID = orders.OrderID WHERE DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d') = '$Date'";
        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['ordersCount'] > 0 ? number_format($data['ordersCount']) : 0);
    }

    public function getWeeklyOrders()
    {
        $sql = "SELECT COUNT(orders_requests.OrderRequestID) as ordersCount FROM orders_requests JOIN orders ON orders_requests.OrderID = orders.OrderID WHERE YEARWEEK(FROM_UNIXTIME(orders.CreatedAt), 1) = YEARWEEK(CURDATE(), 1)";
        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['ordersCount'] > 0 ? number_format($data['ordersCount']) : 0);
    }

    public function getMonthlyOrders()
    {
        $Date = date('Y-m-d');
        $sql = "SELECT COUNT(orders_requests.OrderRequestID) as ordersCount FROM orders_requests JOIN orders ON orders_requests.OrderID = orders.OrderID WHERE MONTH(FROM_UNIXTIME(orders.CreatedAt)) = MONTH(CURRENT_DATE()) AND YEAR(FROM_UNIXTIME(orders.CreatedAt)) = YEAR(CURRENT_DATE())";
        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['ordersCount'] > 0 ? number_format($data['ordersCount']) : 0);
    }


    public function getTotalSales()
    {
        $sql = "SELECT SUM(orders_requests.GrandTotal) as sales
                  FROM orders_requests
                  WHERE (OrderStatusID = 3 OR OrderStatusID = 4 OR OrderStatusID = 7)";

        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['sales'] > 0 ? number_format($data['sales'], 2) : 0.00);
    }

    public function getMonthlySales()
    {
        $sql = "SELECT SUM(orders_requests.GrandTotal) as sales
                  FROM orders_requests
                  JOIN orders ON orders_requests.OrderID = orders.OrderID
                  WHERE (orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 4 OR orders_requests.OrderStatusID = 7) AND MONTH(FROM_UNIXTIME(orders.CreatedAt)) = MONTH(CURRENT_DATE())
                  AND YEAR(FROM_UNIXTIME(orders.CreatedAt)) = YEAR(CURRENT_DATE())";

        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['sales'] > 0 ? number_format($data['sales'], 2) : 0.00);
    }

    public function getWeeklySales()
    {
        $sql = "SELECT SUM(orders_requests.GrandTotal) as sales
                  FROM orders_requests
                  JOIN orders ON orders_requests.OrderID = orders.OrderID
                  WHERE (orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 4 OR orders_requests.OrderStatusID = 7) AND YEARWEEK(FROM_UNIXTIME(orders.CreatedAt), 1) = YEARWEEK(CURDATE(), 1)";

        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['sales'] > 0 ? number_format($data['sales'], 2) : 0.00);
    }

    public function getTodaysSales()
    {
        $Date_timestamp = time();
        $Date = date('Y-m-d', $Date_timestamp);
        $sql = "SELECT SUM(orders_requests.GrandTotal) as sales
                  FROM orders_requests
                  JOIN orders ON orders_requests.OrderID = orders.OrderID
                  WHERE (orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 4 OR orders_requests.OrderStatusID = 7) AND DATE_FORMAT(FROM_UNIXTIME(orders.CreatedAt), '%Y-%m-%d') = '$Date'";

        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['sales'] > 0 ? number_format($data['sales'], 2) : 0.00);
    }

}
