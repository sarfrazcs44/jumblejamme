<?php

Class Booking_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("bookings");

    }


    public function getBookings($where = false, $order_by_time = false, $limit = false, $start = 0, $system_language_code = false)
    {

        $this->db->select('bookings.*,booking_invoice.*,booking_statuses.*,users.Email,users.Mobile,users_text.FullName,categories_text.Title as CategoryTitle,categories.Image as CategoryImage, cities_text.Title as UserCity, tut.FullName as TechnicianFullName, tu.Email as TechnicianEmail, tu.Mobile as TechnicianMobile, v.VehicleNumber as VehicleNumber, tct.Title as TechnicianCity, vct.Title as VehicleCity, tu.OnlineStatus as TechnicianOnlineStatus');
        $this->db->from('bookings');

        $this->db->join('booking_invoice', 'bookings.BookingID = booking_invoice.BookingID', 'LEFT');
        $this->db->join('booking_statuses', 'bookings.Status = booking_statuses.BookingStatusID', 'LEFT');

        $this->db->join('users', 'bookings.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');

        // technician details
        $this->db->join('users as tu', 'bookings.TechnicianID = tu.UserID', 'LEFT');
        $this->db->join('users_text as tut', 'tu.UserID = tut.UserID', 'LEFT');

        // vehicle details
        $this->db->join('vehicles as v', 'bookings.VehicleID = v.VehicleID', 'LEFT');

        $this->db->join('cities', 'users.CityID = cities.CityID', 'left');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID', 'left');

        // technician city
        $this->db->join('cities as tc', 'tu.CityID = tc.CityID', 'left');
        $this->db->join('cities_text as tct', 'tc.CityID = tct.CityID AND tct.SystemLanguageID = 1', 'left');

        // vehicle city
        $this->db->join('cities as vc', 'v.CityID = vc.CityID', 'left');
        $this->db->join('cities_text as vct', 'vc.CityID = vct.CityID AND vct.SystemLanguageID = 1', 'left');

        $this->db->join('categories', 'bookings.CategoryID = categories.CategoryID', 'left');
        $this->db->join('categories_text', 'categories.CategoryID = categories_text.CategoryID', 'left');

        $this->db->join('system_languages as slct', 'slct.SystemLanguageID = cities_text.SystemLanguageID', 'Left');
        $this->db->join('system_languages as slcat', 'slcat.SystemLanguageID = categories_text.SystemLanguageID', 'Left');

        // $this->db->where('tct.SystemLanguageID', 1);
        // $this->db->where('vct.SystemLanguageID', 1);

        if ($system_language_code) {
            $this->db->where('slct.ShortCode', $system_language_code);
            $this->db->where('slcat.ShortCode', $system_language_code);
        } else {
            $this->db->where('slct.IsDefault', '1');
            $this->db->where('slcat.IsDefault', '1');
        }

        if ($where) {
            $this->db->where($where);
        }

        $this->db->group_by('bookings.BookingID');
        if ($order_by_time) {
            $this->db->order_by('bookings.BookingDate', 'DESC');
        } else {
            $this->db->order_by('bookings.BookingDate', 'DESC');
        }

        if ($limit) {
            $this->db->limit($limit, $start);
        }


        $result = $this->db->get();

        // echo $this->db->last_query();exit();
        return $result->result_array();


    }


}