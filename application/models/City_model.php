<?php
Class City_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("cities");
    }

    public function getDataByCity($city_name,$system_language_code = 'EN'){

    	$this->db->select('cities.CityID,cities_text.Title as CityTitle,states.StateID,states_text.Title as StateTitle,countries.CountryID,countries_text.Title as CountryTitle');

    	$this->db->from('cities');

    	$this->db->join('cities_text','cities_text.CityID = cities.CityID');
    	$this->db->join('system_languages','system_languages.SystemLanguageID = cities_text.SystemLanguageID');

    	$this->db->join('states','states.StateID = cities.StateID');
    	$this->db->join('states_text','states_text.StateID = states.StateID');

    	$this->db->join('countries','countries.CountryID = states.CountryID');
    	$this->db->join('countries_text','countries_text.CountryID = countries.CountryID');

    	if($system_language_code) {
                $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }

        $this->db->like('LOWER(cities_text.Title)',strtolower($city_name),'both');

        return  $this->db->get()->row_array();
    }

    public function getDataByLocation($CountryCode, $StateTitle, $CityTitle, $system_language_code = 'EN')
    {
        $this->db->select('cities.CityID,cities_text.Title as CityTitle,states.StateID,states_text.Title as StateTitle,countries.CountryID,countries_text.Title as CountryTitle');

        $this->db->join('cities_text','cities_text.CityID = cities.CityID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = cities_text.SystemLanguageID');

        $this->db->join('states','states.StateID = cities.StateID');
        $this->db->join('states_text','states_text.StateID = states.StateID');

        $this->db->join('countries','countries.CountryID = states.CountryID');
        $this->db->join('countries_text','countries_text.CountryID = countries.CountryID');

        if ( $system_language_code ) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault','1');
        }

        $this->db->where([
            'LOWER(countries.CountryCode)' => strtolower($CountryCode),
            'LOWER(states_text.Title)' => strtolower($StateTitle),
            'LOWER(cities_text.Title)' => strtolower($CityTitle)
        ]);

        return  $this->db->get('cities')->row_array();
    }

    public function insertNewLocation($ipDetail, $system_language_code = 'EN')
    {
        $this->load->Model([
            'Country_model',
            'Country_text_model',
            'State_model',
            'State_text_model',
            'City_text_model'
        ]);

        $countries = $this->Country_model->getAllJoinedData(true, 'CountryID', $system_language_code, 'countries.CountryCode = "' . $ipDetail->country . '"');

        if ($countries) {

            $CountryID = $countries[0]['CountryID'];

        } else {

            $CountryID = $this->Country_model->save([
                'CountryCode' => $ipDetail->country,
                'PhoneCode' => '',
                'SortOrder' => 0,
                'Hide' => 0,
                'IsActive' => 1,
                'CreatedAt' => date('Y-m-d H:i:s'),
                'UpdatedAt' => date('Y-m-d H:i:s'),
                'CreatedBy' => 0,
                'UpdatedBy' => 0
            ]);

            $names = json_decode(file_get_contents("http://country.io/names.json"), true);

            $country_name = isset($names[$ipDetail->country]) ? $names[$ipDetail->country] : '';

            $this->Country_text_model->save([
                'CountryID' => $CountryID,
                'Title' => $country_name,
                'SystemLanguageID' => 1,
                'CreatedAt' => date('Y-m-d H:i:s'),
                'UpdatedAt' => date('Y-m-d H:i:s'),
                'CreatedBy' => 0,
                'UpdatedBy' => 0
            ]);
        }

        if ($ipDetail->region) {

            $states = $this->State_model->getAllJoinedData(true, 'StateID', $system_language_code, 'states.CountryID = "' . $CountryID . '" AND states_text.Title = "' . $ipDetail->region . '"');

            if ($states) {

                $StateID = $states[0]['StateID'];

            } else {

                $StateID = $this->State_model->save([
                    'CountryID' => $CountryID,
                    'SortOrder' => 0,
                    'Hide' => 0,
                    'IsActive' => 1,
                    'CreatedAt' => date('Y-m-d H:i:s'),
                    'UpdatedAt' => date('Y-m-d H:i:s'),
                    'CreatedBy' => 0,
                    'UpdatedBy' => 0
                ]);

                $this->State_text_model->save([
                    'StateID' => $StateID,
                    'Title' => $ipDetail->region,
                    'SystemLanguageID' => 1,
                    'CreatedAt' => date('Y-m-d H:i:s'),
                    'UpdatedAt' => date('Y-m-d H:i:s'),
                    'CreatedBy' => 0,
                    'UpdatedBy' => 0
                ]);
            }

            if ($ipDetail->city) {

                $cities = $this->City_model->getAllJoinedData(true, 'CityID', $system_language_code, 'cities.StateID = "' . $StateID . '" AND cities_text.Title = "' . $ipDetail->city . '"');

                if ($cities) {

                    $CityID = $cities[0]['CityID'];

                } else {

                    $CityID = $this->City_model->save([
                        'StateID' => $StateID,
                        'SortOrder' => 0,
                        'Hide' => 0,
                        'IsActive' => 1,
                        'CreatedAt' => date('Y-m-d H:i:s'),
                        'UpdatedAt' => date('Y-m-d H:i:s'),
                        'CreatedBy' => 0,
                        'UpdatedBy' => 0
                    ]);

                    $this->City_text_model->save([
                        'CityID' => $CityID,
                        'Title' => $ipDetail->city,
                        'SystemLanguageID' => 1,
                        'CreatedAt' => date('Y-m-d H:i:s'),
                        'UpdatedAt' => date('Y-m-d H:i:s'),
                        'CreatedBy' => 0,
                        'UpdatedBy' => 0
                    ]);
                }
            }
        }
    }

    public function getCitiesByCountryOrState($locations)
    {
        $this->db->select('cities.CityID, states.StateID, states.CountryID');
        $this->db->join('states', 'states.StateID = cities.StateID');
        
        if (isset($locations['states'])) {
            $this->db->where_in('cities.StateID', $locations['states']);
        }
        if (isset($locations['countries'])) {
            $this->db->or_where_in('states.CountryID', $locations['countries']);
        }

        return $this->db->get('cities')->result_array();
    }
}