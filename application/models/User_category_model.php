<?php

Class User_category_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_categories");

    }

    public function getCategoryBooths($CategoryID, $UserID)
    {
        $this->db->select('users.UserID, users_text.BoothName, users.BoothImage');
        $this->db->from('user_categories');
        $this->db->join('users', 'user_categories.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->where('user_categories.CategoryID', $CategoryID);
        $this->db->where('users.UserID !=', $UserID);
        $this->db->where('users_text.BoothName !=', '');
        $this->db->group_by('users.UserID');
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();
    }

    public function getSelectedCategories($UserID, $Type, $system_language_code = 'EN')
    {
        $this->db->select('user_categories.*, categories.*, categories_text.Title');
        $this->db->from('user_categories');
        $this->db->join('categories', 'user_categories.CategoryID = categories.CategoryID', 'LEFT');
        $this->db->join('categories_text', 'categories.CategoryID = categories_text.CategoryID');
        $this->db->join('system_languages', 'categories_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('user_categories.UserID', $UserID);
        $this->db->where('user_categories.Type', $Type);
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return array();
        }
    }

    /*public function getSuggestedBooths($UserID, $CityID)
    {
        $sql = "SELECT users_text.BoothName, users.*, cities_text.Title as CityTitle, countries.Currency, countries.CurrencySymbol 
                FROM user_categories
                LEFT JOIN users ON user_categories.UserID = users.UserID
                JOIN users_text ON users.UserID = users_text.UserID
                LEFT JOIN cities ON users.CityID = cities.CityID
                JOIN cities_text ON cities.CityID = cities_text.CityID
                LEFT JOIN countries ON cities.CountryID = countries.CountryID
                JOIN countries_text ON countries.CountryID = countries_text.CountryID
                WHERE users_text.BoothName != ''
                AND (user_categories.CategoryID IN (SELECT CategoryID FROM user_categories WHERE user_categories.UserID = $UserID) OR users.CityID = $CityID)
                AND users.UserID != $UserID
                GROUP BY users.UserID LIMIT 10";
        $result = $this->db->query($sql);
        return $result->result();
    }*/

    public function getSuggestedBooths($UserID, $system_language_code = 'EN',$type = 'booth')
    {
        $sql = "SELECT users_text.FullName,users_text.BoothName, users.*, cities_text.Title as CityTitle, countries.Currency, countries.CurrencySymbol 
                FROM user_categories
                LEFT JOIN users ON user_categories.UserID = users.UserID
                JOIN users_text ON users.UserID = users_text.UserID
                LEFT JOIN cities ON users.CityID = cities.CityID
                JOIN cities_text ON cities.CityID = cities_text.CityID
                LEFT JOIN countries ON cities.CountryID = countries.CountryID
                JOIN countries_text ON countries.CountryID = countries_text.CountryID
                JOIN system_languages slct ON cities_text.SystemLanguageID = slct.SystemLanguageID
                JOIN system_languages slctt ON countries_text.SystemLanguageID = slctt.SystemLanguageID
                WHERE slct.ShortCode = '$system_language_code' AND slctt.ShortCode = '$system_language_code'
                AND users_text.BoothName != ''
                AND user_categories.CategoryID IN (SELECT CategoryID FROM user_categories WHERE user_categories.UserID = $UserID)
                AND users.UserID != $UserID
                AND user_categories.Type = '".$type."'
                GROUP BY users.UserID LIMIT 10";
        $result = $this->db->query($sql);
        return $result->result();
    }

}