<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


require_once 'google-translate/vendor/autoload.php';
use Stichoza\GoogleTranslate\GoogleTranslate;



class GoogleTranslateLib {

    function __construct() {

    }

    public function getTranslation($lang = 'en', $text) {
            
        $tr = new GoogleTranslate(); // Translates to 'en' from auto-detected language by default
        $tr->setSource('en'); // Translate from English
        $tr->setSource(); // Detect language automatically
        $tr->setTarget($lang); // Translate to Georgian
        return $tr->translate($text);
    }
    
}
