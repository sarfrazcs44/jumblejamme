<?php include 'header.php'; ?>
<div class="main-wrapper">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="search-filter">
               <form action="#">
                  <div class="col-md-3">
                     <select class="form-control">
                        <option value="">Search by Category</option>
                        <option value="">Elecronics</option>
                        <option value="">Furniture</option>
                        <option value="">Automobile</option>
                        <option value="">Home Appliance</option>
                     </select>
                  </div>
                  <div class="col-md-5">
                     <input class="form-control" type="search" placeholder="Search By Keyword">
                  </div>
                  <div class="col-md-3">
                     <label class="check1">1
                     <input type="checkbox" checked="checked">
                     <span class="checkmark"></span>
                     </label>
                     or
                     <label class="check1">More
                     <input type="checkbox">
                     <span class="checkmark"></span>
                     </label>         
                  </div>
                  <div class="col-md-1">
                     <button type="submit" class="btn btn-default btn-search">Search</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p1.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="product-detail.php" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p2.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="product-detail.php" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p3.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="product-detail.php" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p4.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="product-detail.php" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p5.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p1.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p2.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p3.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p4.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p5.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="pagi001">
               <ul class="pagination">
                  <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item active"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">Next</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include 'footer.php'; ?>