<?php include 'header.php'; ?>
<link rel="stylesheet" type="text/css" href="css/chat.css">

<div class="wrapper">

<section class="mobile-view">
<div class="container-fluid">
   <div class="row">
      <div class="col-md-12">
         <a href="demands.php"><img src="images/home3.jpg" class="img-responsive"></a>
      </div>
       <div class="col-md-12">
         <a href="product-Grid.php"><img src="images/home1.jpg" class="img-responsive"></a>
      </div>
       <div class="col-md-12">
         <a href="bidding.php"><img src="images/home2.jpg" class="img-responsive"></a>
      </div>
   </div>
</div>
   
</section>


   <section class="main-home">
      <div class="container-fluid">
         <div class="col-md-4 col-sm-4">
            <div class="slide-box003">
               <div class="row">
                  <div class="col-md-12">
                     <div class="slider-heading">
                        Demands
                     </div>
                  </div>
               </div>
               <div class="ads-box">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="topbar001">
                           <div class="col-md-2">
                              <div class="ad-img">
                                 <img src="images/p1.jpg">
                              </div>
                           </div>
                           <div class="col-md-10">
                              <h3 class="title01">
                              Seller name
                              </h3>
                           </div>
                        </div>
                        <div class="pro001">
                           <div class="col-md-4">
                              <img src="images/p1.jpg">
                           </div>
                           <div class="col-md-8">
                              <p class="desc01">
                                 lorem ipsum es to dolor porque lorem ipsum es to dolor lorem ipsum es to dolor
                              </p>
                              <div class="location001">
                                 <i class="fa fa-map-marker"></i> DHA, Lahore
                              </div>
                           </div>
                        </div>
                        <div class="sharing-icons">
                            <div class="row">
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-commenting-o"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-share-alt"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#" value='Show' id="showPanel"><i class="fa fa-comments-o"></i></a></div></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="ads-box">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="topbar001">
                           <div class="col-md-2">
                              <div class="ad-img">
                                 <img src="images/p1.jpg">
                              </div>
                           </div>
                           <div class="col-md-10">
                              <h3 class="title01">
                              Seller name
                              </h3>
                           </div>
                        </div>
                        <div class="pro001">
                           <div class="col-md-4">
                              <img src="images/p1.jpg">
                           </div>
                           <div class="col-md-8">
                              <p class="desc01">
                                 lorem ipsum es to dolor porque lorem ipsum es to dolor lorem ipsum es to dolor
                              </p>
                              <div class="location001">
                                 <i class="fa fa-map-marker"></i> DHA, Lahore
                              </div>
                           </div>
                        </div>
                         <div class="sharing-icons">
                            <div class="row">
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-commenting-o"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-share-alt"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#" value='Show' id="showPanel"><i class="fa fa-comments-o"></i></a></div></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="ads-box">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="topbar001">
                           <div class="col-md-2">
                              <div class="ad-img">
                                 <img src="images/p1.jpg">
                              </div>
                           </div>
                           <div class="col-md-10">
                              <h3 class="title01">
                              Seller name
                              </h3>
                           </div>
                        </div>
                        <div class="pro001">
                           <div class="col-md-4">
                              <img src="images/p1.jpg">
                           </div>
                           <div class="col-md-8">
                              <p class="desc01">
                                 lorem ipsum es to dolor porque lorem ipsum es to dolor lorem ipsum es to dolor
                              </p>
                              <div class="location001">
                                 <i class="fa fa-map-marker"></i> DHA, Lahore
                              </div>
                           </div>
                        </div>
                         <div class="sharing-icons">
                            <div class="row">
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-commenting-o"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#"><i class="fa fa-share-alt"></i></a></div></div>
                           <div class="col-md-4 col-sm-4 col-xs-4"><div class="icon1"><a href="#" value='Show' id="showPanel"><i class="fa fa-comments-o"></i></a></div></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4">
            <div class="slide-box001">
               <div class="row">
                  <div class="col-md-12">
                     <div class="slider-heading">
                        Latest Products
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <figure class="card card-product">
                        <div class="img-wrap"><img src="images/p1.jpg"> </div>
                        <h4 class="title">Good product</h4>
                        <div class="bottom-wrap">
                           <div class="price-wrap h5">
                              <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                           </div>
                           <a href="" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                           <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                     </figure>
                  </div>
                  <!-- col // -->
                  <div class="col-md-6 col-sm-12">
                     <figure class="card card-product">
                        <div class="img-wrap"><img src="images/p1.jpg"> </div>
                        <h4 class="title">Good product</h4>
                        <div class="bottom-wrap">
                           <div class="price-wrap h5">
                              <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                           </div>
                           <a href="" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                           <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                     </figure>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <figure class="card card-product">
                        <div class="img-wrap"><img src="images/p1.jpg"> </div>
                        <h4 class="title">Good product</h4>
                        <div class="bottom-wrap">
                           <div class="price-wrap h5">
                              <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                           </div>
                           <a href="" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                           <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                     </figure>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <figure class="card card-product">
                        <div class="img-wrap"><img src="images/p1.jpg"> </div>
                        <h4 class="title">Good product</h4>
                        <div class="bottom-wrap">
                           <div class="price-wrap h5">
                              <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                           </div>
                           <a href="" class="btn btn-sm btn-primary float-right cart-btn">Add to Cart</a> 
                           <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                     </figure>
                  </div>
                  <!-- col // --><!-- col // -->
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4">
            <div class="slide-box002">
               <div class="row">
                  <div class="col-md-12">
                     <div class="slider-heading">
                        Bidding
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <figure class="card card-product">
                        <div class="img-wrap"><img src="images/p1.jpg"> </div>
                        <h4 class="title">Good product</h4>
                        <div class="bottom-wrap">
                           <div class="price-wrap h5">
                              <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                           </div>
                           <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                           <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                     </figure>
                  </div>
                  <!-- col // -->
                  <div class="col-md-6 col-sm-12">
                     <figure class="card card-product">
                        <div class="img-wrap"><img src="images/p1.jpg"> </div>
                        <h4 class="title">Good product</h4>
                        <div class="bottom-wrap">
                           <div class="price-wrap h5">
                              <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                           </div>
                           <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                           <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                     </figure>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <figure class="card card-product">
                        <div class="img-wrap"><img src="images/p1.jpg"> </div>
                        <h4 class="title">Good product</h4>
                        <div class="bottom-wrap">
                           <div class="price-wrap h5">
                              <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                           </div>
                           <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                           <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                     </figure>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <figure class="card card-product">
                        <div class="img-wrap"><img src="images/p1.jpg"> </div>
                        <h4 class="title">Good product</h4>
                        <div class="bottom-wrap">
                           <div class="price-wrap h5">
                              <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                           </div>
                           <a href="#" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a>
                           <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                     </figure>
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>
</section>
</div>
<?php include 'footer.php'; ?>