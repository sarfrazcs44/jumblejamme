<?php include 'header.php'; ?>
      <link rel="stylesheet" type="text/css" href="css/chat.css">


<div class="main-wrapper">

   <div class="container">

   <div class="row">
   <div class="col-md-12">

   <div class="profile-header">

   <div class="col-md-2">
      <div class="profile-image">
         <div class="row">
   <div class="small-12 medium-2 large-2 columns">
     <div class="circle">
       <!-- User Profile Image -->
       <img class="profile-pic" src="images/128.jpg">

       <!-- Default Image -->
       <!-- <i class="fa fa-user fa-5x"></i> -->
          <div class="p-image">
       <i class="fa fa-camera upload-button"></i>
        <input class="file-upload" type="file" accept="image/*"/>
     </div>
     </div>
  
  </div>
</div>
      </div>
   </div>

   <div class="col-md-2">
      <div class="profile-name">
         <h3>Bilal Hassan</h3>
         <p>@bilalhassan161</p>
      </div>
   </div>

    <div class="col-md-8">

    <div class="profile-info">
<button class="btn btn-default follow">
  <span class="msg-follow">Follow</span>
  <span class="msg-following">Following</span>
  <span class="msg-unfollow">Unfollow</span>
</button>
        <a href="chat.php" class="btn btn-default">message</a>

         <div class="dropdown account-drop">
                
                  <a href="#" class="dropdown-toggle btn btn-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">... </a>
                  <ul class="dropdown-menu">
                    <li><a href="edit-account.php">Edit Account</a></li>
                    <li><a href="#">Payments</a></li>
                    <li><a href="#">Block</a></li>
                    <li><a href="#">Report an Issue</a></li>                   
                  </ul>
                </div>



       
    </div>
       
    </div>
      
   </div>
      
      
    
   </div>


</div>

<div class="row" style="margin-top:20px;">
         <div class="col-md-4">
            <div class="col-md-12">
               <div class="search-filter">
                  <form action="#">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Search by Category</label>
                           <select class="form-control">
                              <option value="">Select Category</option>
                              <option value="">Elecronics</option>
                              <option value="">Furniture</option>
                              <option value="">Automobile</option>
                              <option value="">Home Appliance</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Search by Keyword</label>
                           <input class="form-control" type="search" placeholder="Enter keyword">
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Search by Location</label>
                           <input class="form-control" type="search" placeholder="Enter Location">
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group">
                           <div class="col-md-12">
                              <div class="col-md-6 col-sm-6">
                                 <label>Min Price</label>
                                 <input class="form-control" type="search" placeholder="eg.100">
                              </div>
                              <div class="col-md-6 col-sm-6">
                                 <label>Max Price</label>
                                 <input class="form-control" type="search" placeholder="eg.100">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <button type="submit" class="btn btn-default btn-search">Search</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="col-md-8">
            <div class="ads-box">
               <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="topbar001">
                        <div class="col-md-1 col-sm-1 col-xs-1">
                           <div class="ad-img">
                              <img src="images/p1.jpg">
                           </div>
                        </div>
                        <div class="col-md-11 col-sm-11 col-xs-11">
                           <h3 class="title01">
                              Product title here
                           </h3>
                        </div>
                     </div>
                     <div class="pro001">
                        <div class="col-md-4">
                           <img src="images/p1.jpg">
                        </div>
                        <div class="col-md-8">
                           <p class="desc01">
                              lorem ipsum es to dolor porque lorem ipsum es to dolor lorem ipsum es to dolor lorem ipsum es to dolor porque lorem ipsum es to dolor lorem ipsum es to dolor lorem ipsum es to dolor porque lorem ipsum es to dolor lorem ipsum es to dolor
                           </p>
                           <div class="location001">
                              <i class="fa fa-map-marker"></i> DHA, Lahore
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <div class="sharing-icons">
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                 <div class="icon1"><a href="#"><i class="fa fa-commenting-o"></i> Comment
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                 <div class="icon1">
                                    <a href="#share" class="dg-share-button js-share-button" data-title="Sharing Title">
                                       <div class="dg-share-button-text"> <i class="fa fa-share"></i>  Share</div>
                                       <div class="dg-share-button-popup">
                                          <ul>
                                             <li class="js-social-share">
                                                <span ><i class="fa fa-facebook-official" aria-hidden="true"></i></span>
                                             </li>
                                             <li class="js-social-share">
                                                <span ><i class="fa fa-twitter-square" aria-hidden="true"></i></span>
                                             </li>
                                             <li class="js-social-share">
                                                <span ><i class="fa fa-instagram" aria-hidden="true"></i></span>
                                             </li>
                                          </ul>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                 <div class="icon1"><a href="#" value='Show' id="showPanel"><i class="fa fa-comments-o"></i> Chat</a> </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div class="add-comment">
                        <div class="row">
                           <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
                              <div class="user-img003">
                                 <img src="images/128.jpg">
                              </div>
                           </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">
                              <div class="user-text003">
                                 <div class="comment-text">
                                    <input type="text" name="" class="type-reply" placeholder="Enter you comment here">
                                    <span class="reply-button"><button type="submit" class="send-btn" name=""><i class="fa fa-paper-plane" aria-hidden="true"></i></button></span>
                                 </div>
                                 <span>Press enter to post</span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="comment001">
                        <ul class="comment-wrap">
                           <li class="comments-list">
                              <div class="row">
                                 <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
                                    <div class="user-img003">
                                       <img src="images/128.jpg">
                                    </div>
                                 </div>
                                 <div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">
                                    <div class="user-text003">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                       <div><span id="show"><b>Reply</b></span> <span>12-10-2019</span></div>
                                       <ul class="sub-comment">
                                          <li id="showcomment" class="subcomment"><input type="text" class="type-reply" placeholder="Enter your Reply here"/> <span class="reply-button"><button type="submit" class="send-btn" name=""><i class="fa fa-paper-plane" aria-hidden="true"></i></button></span></li>
                                          <li><input type="input" class="subcomment" placeholder="Enter your Reply here"/></li>
                                          <span class="subcomment" >Press enter to post</span>
                                          <li class="comments-list">
                                             <div class="row">
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
                                                   <div class="user-img003">
                                                      <img src="images/128.jpg">
                                                   </div>
                                                </div>
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">
                                                   <div class="user-text003">
                                                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                                      <div><span id="show"><b>Reply</b></span> <span>12-10-2019</span></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

</div>
</div>


<?php include 'footer.php'; ?>

