<?php include 'header.php'; ?>
<div class="wrapper">
<div class="container">
   <section>
      <div class="col-md-12">
         <form action="#">
            <div class="col-md-6">
               <h2 class="create-account">Change Avatar</h2>
               <div class="profile-image001">
                  <img src="images/128.jpg">
               </div>
               <label for="change" class="chng-btn"> Change image </label>
               <input type="file" name="" value="Change image" id="change" class="up01">
            </div>
            <div class="col-md-6">
               <h2 class="create-account">Edit your Information</h2>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">    
                        <label>First Name</label>     
                        <input type="text" name="" required="" class="form1" value="bilal">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">    
                        <label>Last Name</label>     
                        <input type="text" name="" required="" class="form1" value="hassan">
                     </div>
                  </div>
                
                  <div class="col-md-6">
                     <div class="form-group">    
                        <label>Phone Number</label>     
                        <input type="text" name="" required="" class="form1" value="123 456 789 0">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">    
                        <label>Email Address</label>     
                        <input type="text" name="" required="" class="form1" value="bilal@example.com">
                     </div>
                  </div>

               </div>
              <h2 class="create-account">Edit your Shipping Details</h2>


                <div class="row">
                 
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Country</label>     
                        <select class="form1">
                           <option>Pakistan</option>
                           <option>China</option>
                           <option>USA</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>State</label>     
                        <select class="form1">
                           <option selected="">Sindh</option>
                           <option>Punjab</option>
                           <option>KPK</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>City</label>     
                        <select class="form1">
                           <option selected="">karachi</option>
                           <option>lahore</option>
                           <option>Hyderbad</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">    
                        <label>Zip Code</label>     
                        <input type="text" name="" class="form1" value="25632">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">    
                        <label>Enter your Complete Address</label>     
                        <input type="text" name="" class="form1" >
                     </div>
                  </div>
              

               </div>
                 <h2 class="create-account">Edit your Payment Details</h2>
<div class="row">

                  
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label>Name on Card</label>
                                       <input type="text" class="form-control" stripe-data="name"
                                          id="name-on-card" placeholder="Card Holder's Name">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Card Number</label>
                                       <input type="text" class="form-control" stripe-data="name"
                                          id="name-on-card" placeholder="Debit/Credit card number">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Expiration Date</label>
                                       <input type="text" class="form-control" stripe-data="name"
                                          id="name-on-card" placeholder="mm/yyyy">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Card CVC</label>
                                       <input type="text" class="form-control" stripe-data="cvc"
                                          id="card-cvc" placeholder="Security Code">
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <img class=" payment-images" src="images/cc.png">
                                 </div>
                               
                                 </div>
                               
               <div class="row">

                  <div class="col-md-12">
                          
                     By creating an account, you agree to Jublejam's Conditions of Use and Privacy Notice.
                  </div>

                  <div class="col-md-6">
                     <div class="form-group">    
                        <input type="submit" name="" required="" value="Update" class="regis-btn">
                     </div>
                  </div>
                  </div>
         </form>
         </div>
      </div>
   </section>
</div>
<?php include 'footer.php'; ?>