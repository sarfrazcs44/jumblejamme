<?php include 'header.php'; ?>
<div class="wrapper">
<div class='container'>
   <div class='row'>
      <div class='col-md-12'>
         <div id='mainContentWrapper'>
            <div class="col-md-12">
               <h3 class="main-heading005">
                  Review Your Order & Complete Checkout
               </h3>
               <div class="shopping_cart">
                  <form  role="form" action="" method="post" id="payment-form">
                     <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <h4 class="panel-title">
                                 <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Review
                                 Your Order</a>
                              </h4>
                           </div>
                           <div id="collapseOne" class="panel-collapse collapse in">
                              <div class="panel-body">
                                 <div class="cart-listing">
                                    <div class="row">
                                       <div class="col-md-1 col-sm-1"><img src="images/p3.jpg" class="cart-list-img" /> </div>
                                       <div class="col-md-4 col-sm-4 cart005">Product Name Dada</div>
                                       <div class="col-md-2 col-sm-2 cart005">In stock</div>
                                       <div class="col-md-2 col-sm-2"><input type="number" class="form1 qty" name=""></div>
                                       <div class="col-md-2 col-sm-2 cart005">124,90 €</div>
                                       <div class="col-md-1 col-sm-1 cart005"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> </button> </div>
                                    </div>
                                 </div>
                                 <div class="cart-listing">
                                    <div class="row">
                                       <div class="col-md-1 col-sm-1"><img src="images/p3.jpg" class="cart-list-img" /> </div>
                                       <div class="col-md-4 col-sm-4 cart005">Product Name Dada</div>
                                       <div class="col-md-2 col-sm-2 cart005">In stock</div>
                                       <div class="col-md-2 col-sm-2"><input type="number" class="form1 qty" name=""></div>
                                       <div class="col-md-2 col-sm-2 cart005">124,90 €</div>
                                       <div class="col-md-1 col-sm-1 cart005"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> </button> </div>
                                    </div>
                                 </div>
                                 <div class="cart-list-payment">
                                    <div class="row">
                                       <div class="col-md-10">
                                          <div class="pull-right"> Subtotal:</div>
                                       </div>
                                       <div class="col-md-2">
                                          <div class="pull-right">$300</div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-10">
                                          <div class="pull-right"> Shipping Charges:</div>
                                       </div>
                                       <div class="col-md-2">
                                          <div class="pull-right">$300</div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-10">
                                          <div class="pull-right total-prc"> Total:</div>
                                       </div>
                                       <div class="col-md-2">
                                          <div class="pull-right total-prc">$300</div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="shippingbtn"><a style="width:auto;" data-toggle="collapse"
                        data-parent="#accordion" href="#collapseTwo" class=" btn btn-success" onclick="$(this).fadeOut(); $('#payInfo').fadeIn();">Continue
                        to Billing Information»</a>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Contact
                              and Billing Information</a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                           <div class="panel-body">
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label>First Name</label>
                                       <input type="text" name="" class="form-control">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label>Last Name</label>
                                       <input type="text" name="" class="form-control">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label>Phone no</label>
                                       <input type="text" name="" class="form-control">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label>Country</label>
                                       <select class="form-control">
                                          <option>Pakistan</option>
                                          <option>India</option>
                                          <option>Iran</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label>State</label>
                                       <select  class="form-control">
                                          <option>Sindh</option>
                                          <option>Punjab</option>
                                          <option>Kpk</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label>City</label>
                                       <select class="form-control">
                                          <option>Karachi</option>
                                          <option>Lahore</option>
                                          <option>Hyderabad</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-9">
                                    <div class="form-group">
                                       <div class="form-group">
                                          <label>Shipping Address</label>
                                          <input type="text" name="" class="form-control">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                       <div class="form-group">
                                          <label>Zip Code</label>
                                          <input type="text" name="" class="form-control">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div style="text-align: right; margin-bottom:20px;"><a data-toggle="collapse"
                        data-parent="#accordion"
                        href="#collapseThree"
                        class=" btn   btn-success" id="payInfo"
                        style="width:auto;display: none;" onclick="$(this).fadeOut();  
                        document.getElementById('collapseThree').scrollIntoView()">Enter Payment Information »</a>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                              <b>Payment Information</b>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                           <div class="panel-body">
                              <span class='payment-errors'></span>
                              <div class="col-md-8 col-sm-6">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label>Name on Card</label>
                                       <input type="text" class="form-control" stripe-data="name"
                                          id="name-on-card" placeholder="Card Holder's Name">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Card Number</label>
                                       <input type="text" class="form-control" stripe-data="name"
                                          id="name-on-card" placeholder="Debit/Credit card number">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Expiration Date</label>
                                       <input type="text" class="form-control" stripe-data="name"
                                          id="name-on-card" placeholder="mm/yyyy">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Card CVC</label>
                                       <input type="text" class="form-control" stripe-data="cvc"
                                          id="card-cvc" placeholder="Security Code">
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <img class=" payment-images" src="images/cc.png">
                                 </div>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <div class="save-card"><input type="checkbox" name="" id="savecard"> <label for="savecard"> Save Card Details</label> </div>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <button type="submit" class="btn btn-success btn-lg" style="width:auto;">Pay
                                    Now
                                    </button>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <p style="margin-top:20px;">
                                    By submiting this order you are agreeing to our <a href="#">universal
                                    billing agreement</a>, and <a href="#">terms of service</a>.
                                    If you have any questions about our products or services please contact us
                                    before placing this order.
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
</div>
<?php include 'footer.php'; ?>