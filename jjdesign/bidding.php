<?php include 'header.php'; ?>
<div class="main-wrapper">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="search-filter">
               <form action="#">
                  <div class="col-md-3">
                     <select class="form-control">
                        <option value="">Search by Category</option>
                        <option value="">Elecronics</option>
                        <option value="">Furniture</option>
                        <option value="">Automobile</option>
                        <option value="">Home Appliance</option>
                     </select>
                  </div>
              
                  <div class="col-md-5">
                     <input class="form-control" type="search" placeholder="Search By Keyword">
                  </div>
                  <div class="col-md-3 no-padding">
                     <div class="col-md-6 col-sm-6">
                        <input class="form-control" type="search" placeholder="Min-price">
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <input class="form-control" type="search" placeholder="Max-price">
                     </div>
                  </div>
                  <div class="col-md-1">
                     <button type="submit" class="btn btn-default btn-search">Search</button>
                  </div>
               </form>
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p1.jpg"> </div>
               <h4 class="title">Good product</h4>
               <p id="counter" class="counter-clr"></p>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p2.jpg"> </div>
               <h4 class="title">Good product</h4>
               <p id="counter1" class="counter-clr"></p>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="bidding-detail.php" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p3.jpg"> </div>
               <h4 class="title">Good product</h4>
               <p id="counter2" class="counter-clr"></p>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="bidding-detail.php" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p4.jpg"> </div>
               <h4 class="title">Good product</h4>
               <p id="counter3" class="counter-clr"></p>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="bidding-detail.php" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p5.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="bidding-detail.php" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p1.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p2.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p3.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p4.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
         <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="card card-product">
               <div class="img-wrap"><img src="images/p5.jpg"> </div>
               <h4 class="title">Good product</h4>
               <div class="bottom-wrap">
                  <div class="price-wrap h5">
                     <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                  </div>
                  <div class="stock-1">
                     <span>In Stock</span>
                  </div>
                  <a href="" class="btn btn-sm btn-primary float-right bid-btn">Bid Now</a> 
                  <!-- price-wrap.// -->
               </div>
               <!-- bottom-wrap.// -->
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="pagi001">
               <ul class="pagination">
                  <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item active"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">Next</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   // Set the date we're counting down to
   var countDownDate = new Date("Nov 9, 2019 12:37:25").getTime();
   
   // Update the count down every 1 second
   var x = setInterval(function() {
   
     // Get today's date and time
     var now = new Date().getTime();
       
     // Find the distance between now and the count down date
     var distance = countDownDate - now;
       
     // Time calculations for days, hours, minutes and seconds
     var days = Math.floor(distance / (1000 * 60 * 60 * 24));
     var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
     var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
     var seconds = Math.floor((distance % (1000 * 60)) / 1000);
     var milliseconds = Math.floor(((distance % (16.5 * 60))));
       
     // Output the result in an element with id="demo"
     document.getElementById("counter").innerHTML = hours + "h "
     + minutes + "m " + seconds + "s ";
   
       document.getElementById("counter1").innerHTML = hours + "h "
     + minutes + "m " + seconds + "s ";
   
       document.getElementById("counter2").innerHTML = hours + "h "
     + minutes + "m " + seconds + "s ";
   
       document.getElementById("counter3").innerHTML = hours + "h "
     + minutes + "m " + seconds + "s ";
       
     // If the count down is over, write some text 
     if (distance < 0) {
       clearInterval(x);
       document.getElementById("counter").innerHTML = "EXPIRED";
     }
   }, 1);
</script>
<?php include 'footer.php'; ?>