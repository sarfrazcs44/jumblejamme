<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Jumble Jam</title>
      <!-- Bootstrap core CSS -->
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="css/font-awesome.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/themify-icons.css">
      <link rel="icon" type="image/png" href="img/favicon.png">
      <link rel="stylesheet" type="text/css" href="css/owl.css">
      <link rel="stylesheet" type="text/css" href="css/chat.css">

      <link rel="stylesheet" type="text/css" href="css/style.css">
   </head>
   <body>
      <header>
         <!--end shopping-cart -->
         <!-- Static navbar -->
         <div class="container-fluid">
            <div class="shopping-cart">
               <!-- <div class="shopping-cart-header">
                  <div class="shopping-cart-total">
                    <span class="lighter-text">Total:</span>
                    <span class="main-color-text total">$461.15</span>
                  </div>
                  </div>  --><!--end shopping-cart-header -->
               <ul class="shopping-cart-items">
                  <li class="clearfix">
                     <img src="images/p1.jpg" alt="item1" />
                     <span class="item-name">XMREDTREE</span>
                     <span class="item-detail">Pack 100</span>
                     <span class="item-price">$49.50</span>
                     <span class="item-quantity">Quantity: 01</span>
                  </li>
                  <li class="clearfix">
                     <img src="images/p1.jpg" alt="item1" />
                     <span class="item-name">XMWHREIN</span>
                     <span class="item-detail">Pack 100</span>
                     <span class="item-price">$34.06</span>
                     <span class="item-quantity">Quantity: 10</span>
                  </li>
                  <li class="clearfix">
                     <img src="images/p1.jpg" alt="item1" />
                     <span class="item-name">XMJBRR</span>
                     <span class="item-detail">Pack 25</span>
                     <span class="item-price">$14.21</span>
                     <span class="item-quantity">Quantity: 5</span>
                  </li>
               </ul>
               <a href="checkout.php" class="checkout-btn">Checkout <i class="fa fa-chevron-right"></i></a>
            </div>
            <div class="top-header">
               <div class="row">
                  <div class="col-md-2 col-sm-2 col-xs-12">
                     <a href="index.php"><img src="images/logo.png" class="img-responsive logo"></a>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                     <div class="input-group filter001">
                        <div class="input-group-btn search-panel">
                           <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                           <span id="search_concept">Filter by</span> <span class="caret"></span>
                           </button>
                           <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Mobile</a></li>
                              <li><a href="#">Furniture</a></li>
                              <li><a href="#">Electronics</a></li>
                           </ul>
                        </div>
                        <input type="hidden" name="search_param" value="all" id="search_param">         
                        <input type="text" class="form-control" name="x" placeholder="Search Anything Here">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-4  col-xs-12 cart-icon001">
                     <ul>
                        <li class="support">
                           <a href="#" class="btn btn-default add-new" data-toggle="modal" data-target="#addpro">Add New Post</a>
                        </li>
                        <li>  <a href="#" id="cart"><i class="fa fa-shopping-cart"></i> <span class="badge">6</span></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="menu001">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                     <a class="navbar-brand" href="#"></a>
                  </div>
                  <div id="navbar" class="navbar-collapse collapse">
                     <ul class="nav navbar-nav">
                        <li class="active"></li>
                        <li></li>
                        <li></li>
                        <li class="dropdown">
                           <ul class="dropdown-menu">
                              <li></li>
                              <li></li>
                              <li><a href="#">Something else here</a></li>
                              <li></li>
                              <li></li>
                           </ul>
                        </li>
                     </ul>
                     <ul class="nav navbar-nav navbar-left">
                        <ul class="nav navbar-nav">
                           <li class="active"><a href="index.php">Home</a></li>
                           <li><a href="demands.php">Demands</a></li>
                           <li><a href="bidding.php">Bidding</a></li>
                           <li><a href="product-Grid.php">Products</a></li>
                           </li>
                        </ul>
                     </ul>
                     <ul class=" nav navbar-nav navbar-right">
                        <li><a href="#" data-toggle="modal" data-target="#myModal">Login</a></li>
                        <li><a href="Registration.php">Register</a></li>
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><b>Hello</b> <span>Bilal Hassan!</span> <span class="caret"></span></a>
                           <ul class="dropdown-menu">
                              <li><a href="profile.php">My Profile</a></li>
                              <li><a href="all-bidding-list.php">My Biddings</a></li>
                              <li><a href="#">My Ads</a></li>
                              <li role="separator" class="divider"></li>
                              <li class="dropdown-header">Log Out</li>
                           </ul>
                        </li>
                     </ul>
                  </div>
                  <!--/.nav-collapse -->
               </div>
               <!--/.container-fluid -->
            </nav>
         </div>
      </header>