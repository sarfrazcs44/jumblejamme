<?php include 'header.php'; ?>
<div class="wrapper">
   <div class="container-fluid">
      <section class="main02">
         <div class="col-md-12">
            <div class="col-md-6">
               <div class="side-heading">
                  <h2>Welcome to Jumblejam</h2>
                  <p>You are 30 seconds away from earning your own money!</p>
               </div>
            </div>
            <div class="col-md-6">
               <h2 class="create-account">Create a Account</h2>
               <form action="#">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>First Name</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Last Name</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <label>Gender</label>     
                        <div class="form-group">    
                           <input type="radio" name="gender" value="Male"> <span>Male</span>
                           <input type="radio" name="gender" value="Female"> <span>Female</span>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Date of birth</label>     
                           <input type="date" name="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Country</label>     
                           <select class="form1">
                              <option>Pakistan</option>
                              <option>China</option>
                              <option>USA</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>State</label>     
                           <select class="form1">
                              <option>Sindh</option>
                              <option>Punjab</option>
                              <option>KPK</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>City</label>     
                           <select class="form1">
                              <option>karachi</option>
                              <option>lahore</option>
                              <option>Hyderbad</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Zip Code</label>     
                           <input type="text" name="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Phone Number</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Email Address</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-12">
                        By creating an account, you agree to Jublejam's Conditions of Use and Privacy Notice.
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <input type="submit" name="" required="" value="Register" class="regis-btn">
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
   </div>
   </section>
</div>
<?php include 'footer.php'; ?>