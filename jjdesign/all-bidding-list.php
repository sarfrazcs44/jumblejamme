<?php include 'header.php'; ?>
<div class="main-wrapper">
   <div class="container">
      <div class="col-md-12">
         <h3 class="main-heading005">
            Recent Biddings
         </h3>
      </div>
      <div class="col-md-12 col-sm-6">
         <div class="bidding-listing">
            <div class="col-md-1">
               <img src="images/bg2.jpg" class="img-responsive bidding-list-image">
            </div>
            <div class="col-md-4">
               <div class="bidding-desc">
                  <p>lorem ipsum es to dolor lorem ipsum es to dolor lorem ipsum es to dolorlorem ipsum es to dolorloremlorem ipsum es....</p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="bidding-time">
                  <span> (3h-25m-25s) </span> <span>25-11-2018</span>
               </div>
            </div>
            <div class="col-md-2">
               <div class="bidding-price">
                  Last bids <span>$290</span>
               </div>
            </div>
            <div class="col-md-2">
               <div class="bidding-update">
                  <a href="#" class="btn btn-success">Update Bid</a>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-12 col-sm-6">
         <div class="bidding-listing">
            <div class="col-md-1">
               <img src="images/bg2.jpg" class="img-responsive bidding-list-image">
            </div>
            <div class="col-md-4">
               <div class="bidding-desc">
                  <p>lorem ipsum es to dolor lorem ipsum es to dolor lorem ipsum es to dolorlorem ipsum es to dolorloremlorem ipsum es....</p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="bidding-time">
                  <span> (3h-25m-25s) </span> <span>25-11-2018</span>
               </div>
            </div>
            <div class="col-md-2">
               <div class="bidding-price">
                  Last bids <span>$290</span>
               </div>
            </div>
            <div class="col-md-2">
               <div class="bidding-update">
                  <a href="#" class="btn btn-success">Update Bid</a>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-12 col-sm-6">
         <div class="bidding-listing">
            <div class="col-md-1">
               <img src="images/bg2.jpg" class="img-responsive bidding-list-image">
            </div>
            <div class="col-md-4">
               <div class="bidding-desc">
                  <p>lorem ipsum es to dolor lorem ipsum es to dolor lorem ipsum es to dolorlorem ipsum es to dolorloremlorem ipsum es....</p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="bidding-time">
                  <span> (3h-25m-25s) </span> <span>25-11-2018</span>
               </div>
            </div>
            <div class="col-md-2">
               <div class="bidding-price">
                  Last bids <span>$290</span>
               </div>
            </div>
            <div class="col-md-2">
               <div class="bidding-update">
                  <a href="#" class="btn btn-success">Update Bid</a>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-12 col-sm-6">
         <div class="bidding-listing">
            <div class="col-md-1">
               <img src="images/bg2.jpg" class="img-responsive bidding-list-image">
            </div>
            <div class="col-md-4">
               <div class="bidding-desc">
                  <p>lorem ipsum es to dolor lorem ipsum es to dolor lorem ipsum es to dolorlorem ipsum es to dolorloremlorem ipsum es....</p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="bidding-time">
                  <span> (3h-25m-25s) </span> <span>25-11-2018</span>
               </div>
            </div>
            <div class="col-md-2">
               <div class="bidding-price">
                  Last bids <span>$290</span>
               </div>
            </div>
            <div class="col-md-2">
               <div class="bidding-update">
                  <a href="#" class="btn btn-success">Update Bid</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include 'footer.php'; ?>