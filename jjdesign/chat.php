<?php include 'header.php'; ?>
<link rel="stylesheet" type="text/css" href="css/chat.css">
<div class="wrapper">
   <div class="container">
      <div id="frame">
         <div id="sidepanel">
            <div id="profile">
               <div class="wrap">
                  <img id="profile-img" src="http://emilcarlsson.se/assets/mikeross.png" class="online" alt="" />
                  <p>Mike Ross</p>
               </div>
            </div>
            <div id="search">
               <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
               <input type="text" placeholder="Search contacts..." />
            </div>
            <div id="contacts">
               <ul>
                  <li class="contact">
                     <div class="wrap">
                        <span class="contact-status online"></span>
                        <img src="http://emilcarlsson.se/assets/louislitt.png" alt="" />
                        <div class="meta">
                           <p class="name">Idrees</p>
                           <p class="preview">You just got LITT up, Mike.</p>
                        </div>
                     </div>
                  </li>
                  <li class="contact active">
                     <div class="wrap">
                        <span class="contact-status online"></span>
                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                        <div class="meta">
                           <p class="name">Bilal Hassan</p>
                           <p class="preview">Wrong. You take the gun, or you pull out a bigger one. Or, you call their bluff. Or, you do any one of a hundred and forty six other things.</p>
                        </div>
                     </div>
                  </li>
                  <li class="contact">
                     <div class="wrap">
                        <span class="contact-status online"></span>
                        <img src="http://emilcarlsson.se/assets/rachelzane.png" alt="" />
                        <div class="meta">
                           <p class="name">Usman ahmad</p>
                           <p class="preview">I was thinking that we could have chicken tonight, sounds good?</p>
                        </div>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
         <div class="content">
            <div class="contact-profile">
               <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
               <p>Harvey Specter</p>
            </div>
            <div class="messages">
               <ul>
                  <li class="sent">
                     <img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
                     <p>How the hell am I supposed to get a jury to believe you when I am not even sure that I do?!</p>
                  </li>
                  <li class="replies">
                     <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                     <p>When you're backed against the wall, break the god damn thing down.</p>
                  </li>
                  <li class="replies">
                     <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                     <p>Excuses don't win championships.</p>
                  </li>
                  <li class="sent">
                     <img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
                     <p>Oh yeah, did Michael Jordan tell you that?</p>
                  </li>
                  <li class="replies">
                     <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                     <p>No, I told him that.</p>
                  </li>
                  <li class="replies">
                     <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                     <p>What are your choices when someone puts a gun to your head?</p>
                  </li>
                  <li class="sent">
                     <img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
                     <p>What are you talking about? You do what they say or they shoot you.</p>
                  </li>
                  <li class="replies">
                     <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                     <p>Wrong. You take the gun, or you pull out a bigger one. Or, you call their bluff. Or, you do any one of a hundred and forty six other things.</p>
                  </li>
               </ul>
            </div>
            <div class="message-input">
               <div class="wrap">
                  <input type="text" placeholder="Write your message..." />
                  <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                  <button class="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include 'footer.php'; ?>