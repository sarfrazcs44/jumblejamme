<?php include 'header.php'; ?>
<div class="wrapper">
   <div class="container">
      <section class="">
         <div class="col-md-12">
            <div class="col-md-6">
               <h2 class="create-account">Add New Demand</h2>
               <form action="#">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">    
                           <label>Title</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Select Category</label>     
                           <select class="form1">
                              <option>Electronics</option>
                              <option>Automobile</option>
                              <option>Cloths</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Price</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <label>Location</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-12">
                        <label>Description</label>     
                        <textarea class="form1 pro-desc"></textarea>			
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">    
                           <input type="submit" name="" required="" value="Update" class="regis-btn">
                        </div>
                     </div>
                  </div>
            </div>
            <div class="col-md-6">
            <div class="field" align="left">
            <h3>Upload your Product images</h3>
            <label class="upload-pro" for="files">Click here to upload images<br>(Max no of image is 10)</label>
            <input type="file" id="files" name="files[]" multiple />
            </div>
            </form>
            <div class="row" id="image_preview"></div>
            </div>
         </div>
   </div>
   </section>
   <!-- <script type="text/javascript">
      function preview_images() 
      {
       var total_file=document.getElementById("images").files.length;
       for(var i=0;i<total_file;i++)
       {
        $('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
       }
      }
      
      </script> -->
</div>
<script type="text/javascript"></script>
<?php include 'footer.php'; ?>0