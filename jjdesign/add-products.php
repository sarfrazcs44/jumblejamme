<?php include 'header.php'; ?>
<div class="wrapper">
   <div class="container">
      <section class="">
         <div class="col-md-12 col-sm-12">
            <div class="col-md-6 col-sm-12">
               <h2 class="create-account">Add New Product</h2>
               <form action="#">
                  <div class="row">
                     <div class="col-md-12 col-sm-6">
                        <div class="form-group">    
                           <label>Title</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                           <label>Select Category</label>     
                           <select class="form1">
                              <option>Electronics</option>
                              <option>Automobile</option>
                              <option>Cloths</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                           <label>Price</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                           <label>Discount Price (optional)</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                           <label>Stock (Quantity)</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                      <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                           <label>Add Shipping Price</label>     
                           <input type="text" name="" required="" class="form1">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                            
                            <label class="top001"><input type="checkbox" name="" required=""> Included Shipping Price</label>   
                        </div>
                     </div>
                     <div class="col-md-12 col-sm-12">
                        <div class="multi-field-wrapper">
                           <div class="multi-fields">
                              <div class="row multi-field">
                                 <div class="col-md-5 col-sm-5">
                                    <label> Add Variation</label>
                                    <input type="text" name="" class="form1" placeholder="eg:1 to 10">
                                 </div>
                                 <div class="col-md-5 col-sm-5">
                                    <label> Add Price</label>
                                    <input type="text" name="" class="form1" placeholder="eg:$100">
                                 </div>
                                 <div class="col-md-2 col-sm-2 remove-field">
                                    <button type="button" class="btn btn-danger">X</button>
                                 </div>
                              </div>
                           </div>
                           <button type="button" class="add-field  btn btn-success">Add more</button>
                        </div>
                     </div>
                     <div class="col-md-12 col-sm-12">
                        <label>Description</label>     
                        <textarea class="form1 pro-desc"></textarea>			
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">    
                           <input type="submit" name="" required="" value="Update" class="regis-btn">
                        </div>
                     </div>
                  </div>
            </div>
            <div class="col-md-6 col-sm-12">
            <div class="field" align="left">
            <h3>Upload your Product images</h3>
            <label class="upload-pro" for="files">Click here to upload images<br>(Max no of image is 10)</label>
            <input type="file" id="files" name="files[]" multiple />
            </div>
            </form>
            <div class="row" id="image_preview"></div>
            </div>
         </div>
   </div>
   </section>
   <!-- <script type="text/javascript">
      function preview_images() 
      {
       var total_file=document.getElementById("images").files.length;
       for(var i=0;i<total_file;i++)
       {
        $('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
       }
      }
      
      </script> -->
</div>
<script type="text/javascript"></script>
<?php include 'footer.php'; ?>