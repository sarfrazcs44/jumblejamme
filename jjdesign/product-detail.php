<?php include 'header.php'; ?>
<div class="main-wrapper">
   <div class="container">
      <div class="row">
         <div class="preview01 col-md-6">
            <div id="carousel" class="carousel slide" data-ride="carousel">
               <div class="carousel-inner">
                  <div class="item active" data-thumb="0">
                     <img src="images/p3.jpg">        
                  </div>
                  <div class="item" data-thumb="0">
                     <img src="images/p4.jpg">
                  </div>
                  <div class="item" data-thumb="0">
                     <img src="images/p5.jpg">
                  </div>
                  <div class="item" data-thumb="0">
                     <img src="images/p3.jpg">
                  </div>
                  <div class="item" data-thumb="1">
                     <img src="images/p4.jpg">
                  </div>
                  <div class="item" data-thumb="1">
                     <img src="images/p5.jpg">
                  </div>
                  <div class="item" data-thumb="1">
                     <img src="images/p3.jpg">
                  </div>
               </div>
            </div>
            <div class="clearfix">
               <div id="thumbcarousel" class="carousel slide" data-interval="false">
                  <div class="carousel-inner">
                     <div class="item active">
                        <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="images/p3.jpg"></div>
                        <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="images/p4.jpg"></div>
                        <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="images/p5.jpg"></div>
                        <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="images/p3.jpg"></div>
                     </div>
                     <!-- /item -->
                     <div class="item">
                        <div data-target="#carousel" data-slide-to="4" class="thumb"><img src="images/p3.jpg"></div>
                        <div data-target="#carousel" data-slide-to="5" class="thumb"><img src="images/p4.jpg"></div>
                        <div data-target="#carousel" data-slide-to="6" class="thumb"><img src="images/p5.jpg"></div>
                        <div data-target="#carousel" data-slide-to="7" class="thumb"><img src="images/p3.jpg"></div>
                     </div>
                     <!-- /item -->
                  </div>
                  <!-- /carousel-inner -->
                  <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  </a>
                  <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  </a>
               </div>
               <!-- /thumbcarousel -->
            </div>
            <!-- /clearfix -->
         </div>
         <div class="details col-md-6">
            <h3 class="product-title01">Product Title Here</h3>
            <div class="rating">
               <div class="stars">
                  <span class="fa fa-star checked"></span>
                  <span class="fa fa-star checked"></span>
                  <span class="fa fa-star checked"></span>
                  <span class="fa fa-star"></span>
                  <span class="fa fa-star"></span>
               </div>
               <span class="review-no">41 reviews</span>
            </div>
            <p class="product-description01">Suspendisse quos? Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere.</p>
            <h4 class="price"> price: <span>$180</span></h4>
            <p class="vote"><strong>91%</strong> of buyers enjoyed this product!</p>
            <div class="quantity" style="padding-bottom:20px;">
               <h4> Quantity </h4>
               <div>
                  <div class="btn-minus"><span class="glyphicon glyphicon-minus"></span></div>
                  <input value="1" />
                  <div class="btn-plus"><span class="glyphicon glyphicon-plus"></span></div>
               </div>
            </div>
            <div class="variation">
               <select class="form1">
                  <option>1 for $20</option>
                  <option>10 for $80</option>
                  <option>50 for $400</option>
                  <option>100 for $400</option>
               </select>
            </div>
            <div class="action">
               <button class="add-to-cart01 btn btn-default" type="button">add to cart</button>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="desc005">
               <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#home">Product Description</a></li>
                  <li><a data-toggle="tab" href="#menu1">Specification</a></li>
               </ul>
               <div class="tab-content">
                  <div id="home" class="tab-pane fade in active">
                     <h3>ADVANTAGES</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                     <h3>HOW TO USE?</h3>
                     <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                  </div>
                  <div id="menu1" class="tab-pane fade">
                     <h3>Specification</h3>
                     <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <h2 class="review-heading">Reviews & Rating</h2>
         </div>
         <!--          <div class="col-md-8">
            -->         <!--    <div class="form-group" id="rating-ability-wrapper">
            <form>
               <label class="control-label" for="rating">
               <span class="field-label-header">How would you rate Our Product*</span><br>
               <span class="field-label-info"></span>
               <input type="hidden" id="selected_rating" name="selected_rating" value="" required="required">
               </label>
               <div class="bold rating-header" style="">
                  <span class="selected-rating">0</span><small> / 5</small>
               </div>
               <span class="btnrating btn btn-default btn-md" data-attr="1" id="rating-star-1">
               <i class="fa fa-star" aria-hidden="true"></i>
               </span>
               <span class="btnrating btn btn-default btn-md" data-attr="2" id="rating-star-2">
               <i class="fa fa-star" aria-hidden="true"></i>
               </span>
               <span class="btnrating btn btn-default btn-md" data-attr="3" id="rating-star-3">
               <i class="fa fa-star" aria-hidden="true"></i>
               </span>
               <span class="btnrating btn btn-default btn-md" data-attr="4" id="rating-star-4">
               <i class="fa fa-star" aria-hidden="true"></i>
               </span>
               <span class="btnrating btn btn-default btn-md" data-attr="5" id="rating-star-5">
               <i class="fa fa-star" aria-hidden="true"></i>
               </span>
            </div>
            <div class="comment-box">
            <b>Ask Question/Comment</b>
            <textarea size="3" class="form1"></textarea>
            <input type="submit" name="" class="btn btn-default comment-btn" value="Post">
            </div>
            </form>
            </div> -->
         <!-- </div> -->
         <div class="col-md-8">
            <div class="comment-1">
               <div class="comment-user-img">
                  <div class="comment-img"><img src="images/128.jpg" class="comment-user-img"></div>
                  <div class="name-date">
                     <div class="comment-name">John Wick</div>
                     <div class="comment-date">Date: <span>10 jan 2019</span></div>
                     <div class="rating">
                        <div class="stars">
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star"></span>
                           <span class="fa fa-star"></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="message">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
               </div>
            </div>
            <div class="comment-1">
               <div class="comment-user-img">
                  <div class="comment-img"><img src="images/128.jpg" class="comment-user-img"></div>
                  <div class="name-date">
                     <div class="comment-name">John Wick</div>
                     <div class="comment-date">Date: <span>10 jan 2019</span></div>
                     <div class="rating">
                        <div class="stars">
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star"></span>
                           <span class="fa fa-star"></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="message">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
               </div>
            </div>
            <div class="comment-1">
               <div class="comment-user-img">
                  <div class="comment-img"><img src="images/128.jpg" class="comment-user-img"></div>
                  <div class="name-date">
                     <div class="comment-name">John Wick</div>
                     <div class="comment-date">Date: <span>10 jan 2019</span></div>
                     <div class="rating">
                        <div class="stars">
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star"></span>
                           <span class="fa fa-star"></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="message">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-md-4">
            <div class="well well-sm">
               <div class="row">
                  <div class="col-xs-12 col-md-6 text-center">
                     <h1 class="rating-num">
                        4.0
                     </h1>
                     <div class="rating">
                        <span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star">
                        </span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star">
                        </span><span class="glyphicon glyphicon-star-empty"></span>
                     </div>
                     <div>
                        <span class="glyphicon glyphicon-user"></span>1,050,008 total
                     </div>
                  </div>
                  <div class="col-xs-12 col-md-6">
                     <div class="row rating-desc">
                        <div class="col-xs-3 col-md-3 text-right">
                           <span class="glyphicon glyphicon-star"></span>5
                        </div>
                        <div class="col-xs-8 col-md-9">
                           <div class="progress progress-striped">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                 <span class="sr-only">80%</span>
                              </div>
                           </div>
                        </div>
                        <!-- end 5 -->
                        <div class="col-xs-3 col-md-3 text-right">
                           <span class="glyphicon glyphicon-star"></span>4
                        </div>
                        <div class="col-xs-8 col-md-9">
                           <div class="progress">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                 <span class="sr-only">60%</span>
                              </div>
                           </div>
                        </div>
                        <!-- end 4 -->
                        <div class="col-xs-3 col-md-3 text-right">
                           <span class="glyphicon glyphicon-star"></span>3
                        </div>
                        <div class="col-xs-8 col-md-9">
                           <div class="progress">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                 <span class="sr-only">40%</span>
                              </div>
                           </div>
                        </div>
                        <!-- end 3 -->
                        <div class="col-xs-3 col-md-3 text-right">
                           <span class="glyphicon glyphicon-star"></span>2
                        </div>
                        <div class="col-xs-8 col-md-9">
                           <div class="progress">
                              <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                 <span class="sr-only">20%</span>
                              </div>
                           </div>
                        </div>
                        <!-- end 2 -->
                        <div class="col-xs-3 col-md-3 text-right">
                           <span class="glyphicon glyphicon-star"></span>1
                        </div>
                        <div class="col-xs-8 col-md-9">
                           <div class="progress">
                              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                                 <span class="sr-only">15%</span>
                              </div>
                           </div>
                        </div>
                        <!-- end 1 -->
                     </div>
                     <!-- end row -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
           //-- Click on detail
       
   
           //-- Click on QUANTITY
           $(".btn-minus").on("click",function(){
               var now = $(".quantity > div > input").val();
               if ($.isNumeric(now)){
                   if (parseInt(now) -1 > 0){ now--;}
                   $(".quantity > div > input").val(now);
               }else{
                   $(".quantity > div > input").val("1");
               }
           })            
           $(".btn-plus").on("click",function(){
               var now = $(".quantity > div > input").val();
               if ($.isNumeric(now)){
                   $(".quantity > div > input").val(parseInt(now)+1);
               }else{
                   $(".quantity > div > input").val("1");
               }
           })                        
       }) 
</script>
<?php include 'footer.php'; ?>